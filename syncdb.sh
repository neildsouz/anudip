#!/bin/bash

mysql -u root -proot <<!!
drop database if exists openuniversity;
create database openuniversity character set utf8;
exit
!!

python manage.py syncdb
python manage.py loaddata initial_data/auth/initial_data.json
python manage.py loaddata initial_data/translation/initial_data.json
python manage.py loaddata initial_data/badge/initial_data.json
python manage.py loaddata initial_data/core/initial_data.json
python manage.py loaddata initial_data/common/initial_data.json
python manage.py loaddata initial_data/contents/contents_core/initial_data.json
python manage.py loaddata initial_data/contents/practical/initial_data.json
python manage.py loaddata initial_data/contents/courses/initial_data.json
python manage.py loaddata initial_data/contents/stats/initial_data.json
python manage.py loaddata initial_data/contents/files/initial_data.json
