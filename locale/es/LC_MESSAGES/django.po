# Spanish translation for django-registration.
# Copyright (C) 2007, James Bennet
# This file is distributed under the same license as the registration package.
# Ernesto Rico Schmidt <e.rico.schmidt@gmail.com>, 2008.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: django-registration 0.3 \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-08-26 15:48+0800\n"
"PO-Revision-Date: 2008-03-11 00:19-0400\n"
"Last-Translator: Ernesto Rico Schmidt <e.rico.schmidt@gmail.com>\n"
"Language-Team: Español <de@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: settings.py:84
msgid "Mongolia"
msgstr ""

#: settings.py:85
msgid "English"
msgstr ""

#: settings.py:86
msgid "Russia"
msgstr ""

#: apps/core/models.py:9
msgid "female"
msgstr ""

#: apps/core/models.py:10
msgid "male"
msgstr ""

#: apps/core/models.py:15 apps/registration/models.py:169
msgid "user"
msgstr "usuario"

#: apps/core/models.py:16
msgid "birthdate"
msgstr ""

#: apps/core/models.py:17
msgid "teacher"
msgstr ""

#: apps/core/models.py:18
msgid "location"
msgstr ""

#: apps/core/models.py:19
msgid "mobilenumber"
msgstr ""

#: apps/core/models.py:20
msgid "gender"
msgstr ""

#: apps/courses/forms.py:415
msgid "Publish this question?"
msgstr ""

#: apps/courses/models.py:36
msgid "created on"
msgstr ""

#: apps/courses/models.py:114
msgid "Related document"
msgstr ""

#: apps/courses/models.py:116
msgid "Created date"
msgstr ""

#: apps/courses/models.py:119
msgid "Team members"
msgstr ""

#: apps/courses/models.py:126
msgid "answer type"
msgstr ""

#: apps/courses/models.py:127 apps/courses/models.py:175
#: templates/moderators/list-exercises.html:21
#: templates/moderators/view-exercise.html:18
#: templates/moderators/view-topic.html:18
msgid "created date"
msgstr ""

#: apps/courses/models.py:129
msgid "is required"
msgstr ""

#: apps/courses/models.py:131
msgid "votes"
msgstr ""

#: apps/courses/models.py:134
msgid "accounts voted"
msgstr ""

#: apps/courses/models.py:135 templates/moderators/list-question.html:19
#: templates/moderators/student_question_list.html:17
msgid "points"
msgstr ""

#: apps/courses/models.py:136
msgid "submitted by"
msgstr ""

#: apps/courses/models.py:139
msgid "Publish this question"
msgstr ""

#: apps/courses/models.py:140
msgid "suggest status"
msgstr ""

#: apps/courses/models.py:143
msgid "answered accounts"
msgstr ""

#: apps/courses/models.py:157
msgid "Account topics"
msgstr ""

#: apps/courses/models.py:177 templates/moderators/list-exercises.html:22
#: templates/moderators/view-exercise.html:19
msgid "lesson"
msgstr ""

#: apps/courses/models.py:179 templates/moderators/list-exercises.html:23
#: templates/moderators/view-exercise.html:20
msgid "bonus points"
msgstr ""

#: apps/courses/models.py:182
msgid "questions"
msgstr ""

#: apps/courses/models.py:188
msgid "account topics"
msgstr ""

#: apps/courses/models.py:251
msgid "file with a 'flv' format"
msgstr ""

#: apps/courses/views.py:158
msgid "please try again"
msgstr ""

#: apps/moderators/views.py:39
msgid "add subject"
msgstr ""

#: apps/moderators/views.py:57
msgid "edit subject"
msgstr ""

#: apps/moderators/views.py:78 apps/moderators/views.py:184
#: apps/moderators/views.py:395 apps/moderators/views.py:449
#: templates/moderators/list-answers.html:24
#: templates/moderators/list-exercises.html:42
#: templates/moderators/list-lessons.html:34
#: templates/moderators/list-question-answers.html:23
#: templates/moderators/list-question.html:25
#: templates/moderators/list-question.html:40
#: templates/moderators/list-subject.html:30
#: templates/moderators/list-topic.html:36
msgid "translate"
msgstr ""

#: apps/moderators/views.py:128
msgid "add topic"
msgstr ""

#: apps/moderators/views.py:161
msgid "edit topic"
msgstr ""

#: apps/moderators/views.py:233
msgid "add lesson"
msgstr ""

#: apps/moderators/views.py:249
msgid "edit lesson"
msgstr ""

#: apps/moderators/views.py:279 apps/moderators/views.py:302
msgid "translate lesson"
msgstr ""

#: apps/moderators/views.py:325
msgid "add exercise"
msgstr ""

#: apps/moderators/views.py:361
msgid "edit exercise"
msgstr ""

#: apps/moderators/views.py:483
msgid "add question"
msgstr ""

#: apps/moderators/views.py:515 apps/moderators/views.py:535
msgid "edit question"
msgstr ""

#: apps/moderators/views.py:565
msgid "add answer"
msgstr ""

#: apps/moderators/views.py:584
msgid "edit answer"
msgstr ""

#: apps/registration/admin.py:23
msgid "Activate users"
msgstr ""

#: apps/registration/admin.py:43
#, fuzzy
msgid "Re-send activation emails"
msgstr "clave de activación"

#: apps/registration/forms.py:35
#, fuzzy
msgid "Username"
msgstr "nombre de usuario"

#: apps/registration/forms.py:36
#, fuzzy
msgid "This value must contain only letters, numbers and underscores."
msgstr ""
"Los nombres de usuarios sólo pueden contener letras, números y guiones bajos"

#: apps/registration/forms.py:39
msgid "E-mail"
msgstr ""

#: apps/registration/forms.py:41
#, fuzzy
msgid "Password"
msgstr "contraseña"

#: apps/registration/forms.py:43
#, fuzzy
msgid "Password (again)"
msgstr "contraseña (otra vez)"

#: apps/registration/forms.py:44
#: templates/registration/registration_form.html:17
msgid "Birth date"
msgstr ""

#: apps/registration/forms.py:56
msgid "A user with that username already exists."
msgstr ""

#: apps/registration/forms.py:68
msgid "The two password fields didn't match."
msgstr ""

#: apps/registration/forms.py:79
msgid "I have read and agree to the Terms of Service"
msgstr "He leído y acepto los términos de servicio"

#: apps/registration/forms.py:80
msgid "You must agree to the terms to register"
msgstr "Tienes que aceptar los términos para registrarte"

#: apps/registration/forms.py:96
msgid ""
"This email address is already in use. Please supply a different email "
"address."
msgstr ""
"La dirección de correo electrónico ya está siendo usada. Por "
"favorproporciona otra dirección."

#: apps/registration/forms.py:123
msgid ""
"Registration using free email addresses is prohibited. Please supply a "
"different email address."
msgstr ""
"El registro usando una dirección de correo electrónico gratis está prohibido."
"Por favor proporciona otra dirección."

#: apps/registration/models.py:170
msgid "activation key"
msgstr "clave de activación"

#: apps/registration/models.py:175
msgid "registration profile"
msgstr "perfil de registro"

#: apps/registration/models.py:176
msgid "registration profiles"
msgstr "perfiles de registro"

#: apps/students/views.py:20
msgid "success"
msgstr ""

#: apps/students/views.py:21
msgid "you have already voted for this question"
msgstr ""

#: apps/students/views.py:22
msgid "please sign in with your account"
msgstr ""

#: templates/base.html:91 templates/registration/base.html:12
msgid "Home"
msgstr ""

#: templates/base.html:92
msgid "Moderate"
msgstr ""

#: templates/base.html:109 templates/registration/base.html:15
msgid "Logged in"
msgstr ""

#: templates/base.html:111
msgid "Account"
msgstr ""

#: templates/base.html:112 templates/registration/base.html:16
msgid "Log out"
msgstr ""

#: templates/base.html:113 templates/registration/base.html:17
#, fuzzy
msgid "Change password"
msgstr "contraseña"

#: templates/base.html:116 templates/base.html.py:118
#: templates/registration/activate.html:10 templates/registration/base.html:19
#: templates/registration/login.html:8
#: templates/registration/password_reset_complete.html:8
msgid "Log in"
msgstr ""

#: templates/base.html:127
msgid "About"
msgstr ""

#: templates/base.html:128 templates/courses/index.html:76
msgid "Courses"
msgstr ""

#: templates/base.html:129
msgid "Students"
msgstr ""

#: templates/base.html:130
msgid "Teachers"
msgstr ""

#: templates/base.html:131
msgid "Blog"
msgstr ""

#: templates/base.html:132
msgid "Challenge"
msgstr ""

#: templates/base.html:154
msgid "The New Media Marketing Agency"
msgstr ""

#: templates/challenge/add.html:6 templates/profiles/create_profile.html:18
#: templates/profiles/edit_profile.html:18
#: templates/profiles/profile_detail.html:8
#: templates/profiles/profile_list.html:8
#: templates/registration/password_change_form.html:8
#: templates/registration/password_reset_confirm.html:11
#: templates/registration/password_reset_form.html:8
#: templates/registration/registration_form.html:19
msgid "Submit"
msgstr ""

#: templates/challenge/index.html:14 templates/students/index.html:5
msgid "Open University Challenge"
msgstr ""

#: templates/challenge/index.html:31 templates/students/index.html:22
msgid "How to Learn"
msgstr ""

#: templates/challenge/index.html:35 templates/challenge/index.html.py:43
#: templates/registration/login.html:13
msgid "Register"
msgstr ""

#: templates/challenge/index.html:36
msgid "Share"
msgstr ""

#: templates/challenge/success.html:4
msgid "Successfully registered"
msgstr ""

#: templates/courses/topic.html:37
msgid "no video available for this topic"
msgstr ""

#: templates/courses/topic.html:61
msgid "Max Points"
msgstr ""

#: templates/courses/topic.html:64
#: templates/moderators/topic-exercise-question.html:15
msgid "Exercises"
msgstr ""

#: templates/courses/topic.html:72
msgid "completed"
msgstr ""

#: templates/courses/students/ask-start-practice.html:16
msgid ""
"Taking these exercises without signing in your progress won't be saved, so "
"please sign in or register if you have not done so!"
msgstr ""

#: templates/courses/students/ask-start-practice.html:19
msgid "You are going to start exercise"
msgstr ""

#: templates/courses/students/ask-start-practice.html:19
msgid "from lesson"
msgstr ""

#: templates/courses/students/ask-start-practice.html:20
msgid "of topic"
msgstr ""

#: templates/courses/students/practice-list.html:6
msgid "Exercises to practice"
msgstr ""

#: templates/courses/students/practice-list.html:8
msgid "You have completed this exercise"
msgstr ""

#: templates/courses/students/practice-list.html:8
msgid "You haven't completed this exercise"
msgstr ""

#: templates/courses/students/practice-result.html:6
msgid "you've answered all the questions in the current topic's exercise"
msgstr ""

#: templates/courses/students/practice-result.html:7
#: templates/moderators/list-lessons.html:21
#: templates/moderators/view-lesson.html:18
msgid "topic"
msgstr ""

#: templates/courses/students/practice-result.html:8
msgid "exercise"
msgstr ""

#: templates/courses/students/practice-result.html:9
msgid "questions total"
msgstr ""

#: templates/courses/students/practice-result.html:10
msgid "questions answered total"
msgstr ""

#: templates/courses/students/practice-result.html:12
msgid "go"
msgstr ""

#: templates/courses/students/practice-result.html:12
#: templates/moderators/list-question.html:10
#: templates/moderators/student_question_list.html:10
msgid "back"
msgstr ""

#: templates/courses/students/practice-result.html:12
msgid "to topic page"
msgstr ""

#: templates/courses/students/practice.html:29
msgid "You have answered incorrectly"
msgstr ""

#: templates/courses/students/practice.html:38
#: templates/home/home_after_login.html:69
#: templates/moderators/exercise_list_questions.html:16
#: templates/moderators/list-exercise-questions.html:23
msgid "Question"
msgstr ""

#: templates/courses/students/practice.html:53
msgid "total votes"
msgstr ""

#: templates/home/home_after_login.html:12
msgid "Suggested Topics"
msgstr ""

#: templates/home/home_after_login.html:43
msgid "Track Progress"
msgstr ""

#: templates/home/home_after_login.html:45
msgid "Points"
msgstr ""

#: templates/home/home_after_login.html:47
msgid "Badges"
msgstr ""

#: templates/home/home_after_login.html:55
msgid "Lessons Completed"
msgstr ""

#: templates/home/home_after_login.html:64
msgid "Questions Submitted"
msgstr ""

#: templates/home/home_after_login.html:70
msgid "Is Published"
msgstr ""

#: templates/home/home_after_login.html:71
msgid "Suggest status"
msgstr ""

#: templates/moderators/add-answer.html:59
#: templates/moderators/add_answer.html:13
msgid "Answers"
msgstr ""

#: templates/moderators/add-exercise-question.html:25
#: templates/moderators/add-question.html:45
msgid "add another answer"
msgstr ""

#: templates/moderators/add-exercise-question.html:26
#: templates/moderators/add-question.html:26
msgid "Answers for this question"
msgstr ""

#: templates/moderators/add-exercise.html:52
msgid "add another question"
msgstr ""

#: templates/moderators/add-exercise.html:53
msgid "Questions for this exercise"
msgstr ""

#: templates/moderators/add-exercise.html:74
#: templates/moderators/add-lesson.html:26
#: templates/moderators/add-subject.html:26
#: templates/moderators/add-topic.html:49
#: templates/moderators/add_question_answer.html:26
#: templates/moderators/exercise_list_questions.html:8
#: templates/moderators/list-answers.html:15
#: templates/moderators/list-exercise-questions.html:16
#: templates/moderators/list-exercises.html:15
#: templates/moderators/list-lessons.html:15
#: templates/moderators/list-question-answers.html:15
#: templates/moderators/list-subject.html:15
#: templates/moderators/list-topic.html:15
msgid "Back"
msgstr ""

#: templates/moderators/add-subject.html:8
#: templates/moderators/add-subject.html:12
msgid "Add Subject"
msgstr ""

#: templates/moderators/exercise_list_questions.html:15
msgid "Id"
msgstr ""

#: templates/moderators/exercise_list_questions.html:17
#: templates/moderators/exercise_list_questions.html:26
#: templates/moderators/list-exercise-questions.html:35
#: templates/moderators/list-question-answers.html:30
msgid "Edit"
msgstr ""

#: templates/moderators/exercise_list_questions.html:18
#: templates/moderators/list-exercise-questions.html:36
#: templates/moderators/list-question-answers.html:31
msgid "Delete"
msgstr ""

#: templates/moderators/exercise_list_questions.html:19
#: templates/moderators/exercise_list_questions.html:28
#: templates/moderators/list-answers.html:14
#: templates/moderators/list-question-answers.html:14
msgid "Add answer"
msgstr ""

#: templates/moderators/exercise_list_questions.html:27
msgid "Remove"
msgstr ""

#: templates/moderators/index.html:6
msgid "Moderator"
msgstr ""

#: templates/moderators/index.html:10
msgid "Moderator page"
msgstr ""

#: templates/moderators/index.html:23
msgid "subjects"
msgstr ""

#: templates/moderators/index.html:24
msgid "topics"
msgstr ""

#: templates/moderators/index.html:25
msgid "lessons"
msgstr ""

#: templates/moderators/index.html:26
msgid "exercises"
msgstr ""

#: templates/moderators/index.html:27
msgid "suggested question"
msgstr ""

#: templates/moderators/list-answers.html:6
#: templates/moderators/list-answers.html:10
#: templates/moderators/list-question-answers.html:6
#: templates/moderators/list-question-answers.html:10
msgid "Answer list"
msgstr ""

#: templates/moderators/list-answers.html:19
#: templates/moderators/list-question-answers.html:19
msgid "answer"
msgstr ""

#: templates/moderators/list-answers.html:20
msgid "is_correct"
msgstr ""

#: templates/moderators/list-answers.html:21
#: templates/moderators/list-exercises.html:39
#: templates/moderators/list-lessons.html:31
#: templates/moderators/list-question.html:22
#: templates/moderators/list-subject.html:27
#: templates/moderators/list-topic.html:33
#: templates/moderators/student_question_list.html:20
#: templates/moderators/view-exercise.html:27
#: templates/moderators/view-lesson.html:21
#: templates/moderators/view-subject.html:20
#: templates/moderators/view-topic.html:23
msgid "edit"
msgstr ""

#: templates/moderators/list-answers.html:22
#: templates/moderators/list-exercises.html:40
#: templates/moderators/list-lessons.html:32
#: templates/moderators/list-question.html:23
#: templates/moderators/list-subject.html:28
#: templates/moderators/list-topic.html:34
#: templates/moderators/student_question_list.html:21
msgid "delete"
msgstr ""

#: templates/moderators/list-answers.html:23
#: templates/moderators/list-exercises.html:27
#: templates/moderators/list-lessons.html:23
#: templates/moderators/list-question-answers.html:22
#: templates/moderators/list-question.html:24
#: templates/moderators/list-topic.html:24
msgid "available_translations"
msgstr ""

#: templates/moderators/list-exercise-questions.html:6
msgid "Question List"
msgstr ""

#: templates/moderators/list-exercise-questions.html:10
msgid "Question list"
msgstr ""

#: templates/moderators/list-exercise-questions.html:15
msgid "Add question"
msgstr ""

#: templates/moderators/list-exercise-questions.html:24
msgid "Answer Type"
msgstr ""

#: templates/moderators/list-exercise-questions.html:25
#: templates/moderators/list-exercises.html:26
#: templates/moderators/list-lessons.html:22
#: templates/moderators/list-question-answers.html:21
#: templates/moderators/list-subject.html:20
#: templates/moderators/list-topic.html:23
msgid "Functions"
msgstr ""

#: templates/moderators/list-exercise-questions.html:26
#: templates/moderators/list-subject.html:21
msgid "available translations"
msgstr ""

#: templates/moderators/list-exercise-questions.html:27
msgid "Number of answers"
msgstr ""

#: templates/moderators/list-exercise-questions.html:28
msgid "view answers "
msgstr ""

#: templates/moderators/list-exercise-questions.html:37
msgid "Translate"
msgstr ""

#: templates/moderators/list-exercise-questions.html:41
msgid "view answers"
msgstr ""

#: templates/moderators/list-exercises.html:6
#: templates/moderators/list-exercises.html:10
msgid "Exercises list"
msgstr ""

#: templates/moderators/list-exercises.html:14
msgid "Add exercise"
msgstr ""

#: templates/moderators/list-exercises.html:19
#: templates/moderators/list-lessons.html:19
#: templates/moderators/list-subject.html:19
#: templates/moderators/list-topic.html:19
#: templates/moderators/view-exercise.html:16
#: templates/moderators/view-lesson.html:16
#: templates/moderators/view-subject.html:17
#: templates/moderators/view-topic.html:16
#, fuzzy
msgid "name"
msgstr "nombre de usuario"

#: templates/moderators/list-exercises.html:20
#: templates/moderators/list-lessons.html:20
#: templates/moderators/list-topic.html:20
msgid "desc"
msgstr ""

#: templates/moderators/list-exercises.html:24
msgid "number of questions"
msgstr ""

#: templates/moderators/list-exercises.html:25
msgid "view questions"
msgstr ""

#: templates/moderators/list-exercises.html:37
msgid "View Questions"
msgstr ""

#: templates/moderators/list-exercises.html:41
#: templates/moderators/list-lessons.html:33
#: templates/moderators/list-subject.html:29
#: templates/moderators/list-topic.html:35
msgid "view"
msgstr ""

#: templates/moderators/list-lessons.html:6
#: templates/moderators/list-lessons.html:10
msgid "Lesson list"
msgstr ""

#: templates/moderators/list-lessons.html:14
msgid "Add lesson"
msgstr ""

#: templates/moderators/list-question-answers.html:20
msgid "is correct"
msgstr ""

#: templates/moderators/list-question.html:14
#: templates/moderators/student_question_list.html:15
msgid "question"
msgstr ""

#: templates/moderators/list-question.html:15
#: templates/moderators/student_question_list.html:16
msgid "answer_type"
msgstr ""

#: templates/moderators/list-question.html:16
msgid "hint"
msgstr ""

#: templates/moderators/list-question.html:17
msgid "is_required"
msgstr ""

#: templates/moderators/list-question.html:18
msgid "language"
msgstr ""

#: templates/moderators/list-question.html:20
#: templates/moderators/student_question_list.html:18
msgid "submitted_by"
msgstr ""

#: templates/moderators/list-question.html:21
#: templates/moderators/student_question_list.html:19
msgid "is_published"
msgstr ""

#: templates/moderators/list-subject.html:6
#: templates/moderators/list-subject.html:10
msgid "Subject list"
msgstr ""

#: templates/moderators/list-subject.html:14
msgid "Add subject"
msgstr ""

#: templates/moderators/list-topic.html:6
#: templates/moderators/list-topic.html:10
msgid "Topic list"
msgstr ""

#: templates/moderators/list-topic.html:14
msgid "Add topic"
msgstr ""

#: templates/moderators/list-topic.html:21
#: templates/moderators/view-topic.html:19
msgid "age group"
msgstr ""

#: templates/moderators/list-topic.html:22
#: templates/moderators/view-topic.html:20
msgid "subject"
msgstr ""

#: templates/moderators/student_question_list.html:14
msgid "id"
msgstr ""

#: templates/moderators/suggest_questions.html:4
msgid "Create suggest question"
msgstr ""

#: templates/moderators/suggest_questions.html:5
msgid "Back to List"
msgstr ""

#: templates/moderators/topic-exercise-question.html:14
msgid "Topics"
msgstr ""

#: templates/moderators/topic-exercise-question.html:16
msgid "Submitted questions"
msgstr ""

#: templates/moderators/topic-exercise-question.html:17
msgid "Suggest question"
msgstr ""

#: templates/moderators/topic-exercise-question.html:20
msgid "Student questions"
msgstr ""

#: templates/moderators/translate.html:29
#: templates/moderators/translate_test.html:42
msgid "Available translations"
msgstr ""

#: templates/moderators/view-exercise.html:6
#: templates/moderators/view-exercise.html:10
msgid "Exercise"
msgstr ""

#: templates/moderators/view-exercise.html:14
#: templates/moderators/view-lesson.html:14
#: templates/moderators/view-subject.html:15
#: templates/moderators/view-topic.html:14
msgid "Fields"
msgstr ""

#: templates/moderators/view-exercise.html:17
#: templates/moderators/view-lesson.html:17
#: templates/moderators/view-topic.html:17
msgid "description"
msgstr ""

#: templates/moderators/view-exercise.html:28
#: templates/moderators/view-lesson.html:22
#: templates/moderators/view-subject.html:21
#: templates/moderators/view-topic.html:24
msgid "to subject list"
msgstr ""

#: templates/moderators/view-lesson.html:6
#: templates/moderators/view-lesson.html:10
msgid "Lesson"
msgstr ""

#: templates/moderators/view-subject.html:6
#: templates/moderators/view-subject.html:10
msgid "Subject"
msgstr ""

#: templates/moderators/view-topic.html:6
#: templates/moderators/view-topic.html:10
msgid "Topic"
msgstr ""

#: templates/registration/activate.html:8
msgid "Account successfully activated"
msgstr ""

#: templates/registration/activate.html:14
#, fuzzy
msgid "Account activation failed"
msgstr "clave de activación"

#: templates/registration/login.html:12
#, fuzzy
msgid "Forgot password"
msgstr "contraseña"

#: templates/registration/login.html:12
msgid "Reset it"
msgstr ""

#: templates/registration/login.html:13
msgid "Not member"
msgstr ""

#: templates/registration/logout.html:5
msgid "Logged out"
msgstr ""

#: templates/registration/password_change_done.html:5
#, fuzzy
msgid "Password changed"
msgstr "contraseña (otra vez)"

#: templates/registration/password_reset_complete.html:6
msgid "Password reset successfully"
msgstr ""

#: templates/registration/password_reset_confirm.html:16
msgid "Password reset failed"
msgstr ""

#: templates/registration/password_reset_done.html:5
msgid "Email with password reset instructions has been sent."
msgstr ""

#: templates/registration/password_reset_email.html:2
#, python-format
msgid "Reset password at %(site_name)s"
msgstr ""

#: templates/registration/registration_complete.html:5
msgid "You are now registered. Activation email sent."
msgstr ""

#: templates/students/index.html:19
msgid "More"
msgstr ""

#~ msgid "email address"
#~ msgstr "dirección de coreo electrónico"

#~ msgid "This username is already taken. Please choose another."
#~ msgstr "Este nombre de usuario ya está ocupado. Por favor escoge otro"

#~ msgid "You must type the same password each time"
#~ msgstr "Tienes que introducir la misma contraseña cada vez"
