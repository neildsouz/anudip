��          �      ,      �  )   �     �  -   �     �     �     
  _   $  %   �  N   �  >   �     8  '   A     i     x     �     �  Q  �  )   �     $  ;   8     t     }  !   �  t   �  ,   +  W   X  D   �     �  G        J     a     v     �               
                             	                                  A user with that username already exists. Activate users I have read and agree to the Terms of Service Password Password (again) Re-send activation emails Registration using free email addresses is prohibited. Please supply a different email address. The two password fields didn't match. This email address is already in use. Please supply a different email address. This value must contain only letters, numbers and underscores. Username You must agree to the terms to register activation key registration profile registration profiles user Project-Id-Version: django-registration 0.8 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-08-26 15:48+0800
PO-Revision-Date: 2007-09-29 16:50+0200
Last-Translator: Jannis Leidel <jannis@leidel.info>
Language-Team: Deutsch <de@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Dieser Benutzername ist bereits vergeben. Benutzer aktivieren Ich habe die Nutzungsvereinbarung gelesen und stimme ihr zu Passwort Passwort (wiederholen) Aktivierungs-E-Mail erneut senden Die Registrierung mit einer kostenlosen E-Mail-Adresse ist untersagt. Bitte geben Sie eine andere E-Mail-Adresse an. Die beiden Passwörter sind nicht identisch. Diese E-Mail-Adresse wird schon genutzt. Bitte geben Sie eine andere E-Mail-Adresse an. Dieser Wert darf nur Buchstaben, Ziffern und Unterstriche enthalten. Benutzername Sie müssen der Nutzungsvereinbarung zustimmen, um sich zu registrieren Aktivierungsschlüssel Registrierungsprofil Registrierungsprofile Benutzer 