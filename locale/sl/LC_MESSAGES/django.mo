��          �            x  )   y     �  -   �     �     �     �  _     %   t  N   �  >   �  '   (     P     _     t     �  z  �  1   
     <     Q     o     u  #   �  Y   �       0      7   Q  4   �     �     �     �  	   �               
                              	                                  A user with that username already exists. Activate users I have read and agree to the Terms of Service Password Password (again) Re-send activation emails Registration using free email addresses is prohibited. Please supply a different email address. The two password fields didn't match. This email address is already in use. Please supply a different email address. This value must contain only letters, numbers and underscores. You must agree to the terms to register activation key registration profile registration profiles user Project-Id-Version: 0.8.1beta
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-08-26 15:48+0800
PO-Revision-Date: 2009-10-23 15:49+0100
Last-Translator: Domen Kožar <domen@dev.si>
Language-Team: Slovenian <domen@dev.si>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Slovenian
X-Poedit-Country: SLOVENIA
 Uporabnik z tem uporabniškim imenom že obstaja. Aktiviraj uporabnike Strinjam se z pogoji uporable Geslo Geslo (ponovno) Ponovno pošlju aktivacijske emaile Registracija ni mogoča z brezplačnimi email naslovi. Prosimo vnesite drug email naslov. Polji z gesli se ne ujemata. Email je že v uporabi, prosimo vnesite drugega. Vrednost lahko vsebuje samo črke, cifre in podčrtaje. Za registracijo se morate strinjati z pogoji uporabe Aktivacijski ključ Registracijski profil Registracijski profili Uporabnik 