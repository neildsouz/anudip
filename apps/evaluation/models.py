from django import forms 
from django.db import models
from django.forms.fields import DateField, ChoiceField, MultipleChoiceField
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.auth.models import User
from django.template.context import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.forms import ModelForm, Textarea
from django.contrib.auth.models import User
from django.template.context import RequestContext

g1_CHOICES = (
    (1,_('0 minutes')),
    (2,_('1 - 30 minutes')),
    (3,_('30 minutes - 1 hour')),
    (4,_('1 hour - 3 hours')),
    (5,_('More than 5 hours')),
)

g2_CHOICES = (
    (1,_('At home')),
    (2,_('At school')),
    (3,_('Friends house ')),
    (4,_('At community center')),
    (5,_('No access to computer')),
    (6,_('Others')),
)

g3_CHOICES = (
    (1,_('Very important')),
    (2,_('Fairly important')),
    (3,_('Somewhat important')),
    (4,_('Not very important')),
    (5,_('Not important at all')),
)

g4_CHOICES = (
    (1,_('A Whole Lot')),
    (2,_('A lot')),
    (3,_('Somewhat')),
    (4,_('A little bit')),
    (5,_('Not At All')),
)

g5_CHOICES = (
    (1,_('Listening to songs')),
    (2,_('Playing Games')),
    (3,_('Drawing')),
    (4,_('Learning for school')),
    (5,_('Watching Movies')),
)

g6_CHOICES = (
    (1,_('Turn to the internet ')),
    (2,_('Ask my teacher or turn to my textbook for help')),
    (3,_('Ask a friend in class')),
    (4,_('Ask my parents or a family member for help')),
    (5,_('Pretend i understand')),
)

g7_CHOICES = (
    (1,_('A Whole Lot')),
    (2,_('A lot')),
    (3,_('Somewhat')),
    (4,_('A little bit')),
    (5,_('Not At All')),
)

g8_CHOICES = (
    (1,_('A Whole Lot')),
    (2,_('A lot')),
    (3,_('Somewhat')),
    (4,_('A little bit')),
    (5,_('Not At All')),
)

g9_CHOICES = (
    (1,_('A Whole Lot')),
    (2,_('A lot')),
    (3,_('Somewhat')),
    (4,_('A little bit')),
    (5,_('Not At All')),
)

g10_CHOICES = (
    (1,_('A Whole Lot')),
    (2,_('A lot')),
    (3,_('Somewhat')),
    (4,_('A little bit')),
    (5,_('Not At All')),
)

g13_CHOICES = (
    (1,_('Yes')),
    (2,_('No')),
    (3,_('Maybe')),
    (4,_('I dont know, but i want to find out')),
    (5,_('I dont know and i dont care')),
)

g14_CHOICES = (
    (1,_('Private tuition')),
    (2,_('After School Extra Classes')),
    (3,_('Music-Drama-Dance-Arts')),
    (4,_('None')),
)

class Eval1(models.Model):
    user_id = models.CharField(max_length=10)
    first_name = models.CharField(max_length=110)
    last_name = models.CharField(max_length=110)
    q1 = models.CharField( blank=False, max_length=110)
    q2 = models.CharField( blank=False, max_length=110)
    q3 = models.CharField( blank=False, max_length=110)
    q4 = models.CharField( blank=False, max_length=110)
    q5 = models.CharField( blank=False, max_length=110)
    q6 = models.CharField( blank=False, max_length=110)
    q7 = models.CharField( blank=False, max_length=110)
    q8 = models.CharField( blank=False, max_length=110)
    q9 = models.CharField( blank=False, max_length=110)
    q10 = models.CharField( blank=False, max_length=110)
    q11 = models.CharField(max_length=400, blank=False)
    q12 = models.CharField(max_length=400, blank=False)
    q13 = models.CharField( blank=False, max_length=110)
    q14 = models.CharField( blank=False, max_length=110)
    q15 = models.CharField(max_length=400, blank=False)


class EvalForm(ModelForm):
    class Meta:
        model = Eval1
        fields = ('user_id', 'first_name', 'last_name', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'q11', 'q12', 'q13', 'q14', 'q15')
        widgets = {
            'q1': CheckboxSelectMultiple(choices=g1_CHOICES),
            'q2': CheckboxSelectMultiple(choices=g2_CHOICES),
            'q3': CheckboxSelectMultiple(choices=g3_CHOICES),
            'q4': CheckboxSelectMultiple(choices=g4_CHOICES),
            'q5': CheckboxSelectMultiple(choices=g5_CHOICES),
            'q6': CheckboxSelectMultiple(choices=g6_CHOICES),
            'q7': CheckboxSelectMultiple(choices=g7_CHOICES),
            'q8': CheckboxSelectMultiple(choices=g8_CHOICES),
            'q9': CheckboxSelectMultiple(choices=g9_CHOICES),   
            'q10': CheckboxSelectMultiple(choices=g10_CHOICES),
            'q13': CheckboxSelectMultiple(choices=g13_CHOICES),
            'q14': CheckboxSelectMultiple(choices=g14_CHOICES),                                                                      
        }
