from django.db.models.query_utils import Q
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template.context import RequestContext
from django.utils.translation import gettext
from django.contrib.auth.models import User
from openuniversity.apps.evaluation.models import EvalForm
from django.forms.fields import DateField, ChoiceField, MultipleChoiceField
from django.contrib.auth.decorators import login_required

@login_required()
def index(request): 
    user = request.user
    if request.method == 'POST':
    	if user.is_authenticated():
    	    form = EvalForm(request.POST)
            if form.is_valid():
                form.save()
                return render_to_response('evaluation/evaluationthanks.html')
        else:
            form = EvalForm(request.POST)
            if form.is_valid():
                form.save()
                return render_to_response('evaluation/evaluationthanks.html')
    else:
        if user.is_authenticated():
            form = EvalForm(initial={'user_id': user.id,
                                     'first_name':user.first_name,
                                     'last_name':user.last_name})
        else:
            form = EvalForm()
    return render_to_response('evaluation/evaluation.html', {'form': form})
