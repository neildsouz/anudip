#coding: utf-8
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from openuniversity.apps.contents.contents_core.models import Subject, Topic, \
    ContentAlias
from openuniversity.apps.contents.practical.models import Lesson
from openuniversity.apps.contents.stats.models import Account_Lesson, \
    CompletedExercise
from openuniversity.apps.core.models import AgeGroup
from openuniversity.apps.common.models import SubType

def home(request):
    subjects = Subject.objects.all()
    age_groups = AgeGroup.objects.all()
    return render_to_response("contents/topic/home.html", locals(),
                              context_instance=RequestContext(request))

def ajax_topics(request):
    subjects = [int(s) for s in request.GET['s'].strip().split(",")[1:]]
    age_groups = [int(ag) for ag in request.GET['ag'].strip().split(",")[1:]]
    topics = Topic.objects.all()
    if age_groups:
        topics = topics.filter(age_group__id__in=age_groups)
    if subjects:
        topics = topics.filter(subject__id__in=subjects)
    print topics
    return render_to_response("contents/topic/ajax_topics.html", locals(), 
                              context_instance=RequestContext(request))
    
def topic_lessons(request, t_alias):

    try:
        topic = Topic.objects.get(pk=int(t_alias))
    except:
        topic = ContentAlias.objects.get(alias=t_alias).content_object    
    lessons = topic.lessons.all()
    if request.user.is_superuser:
        return HttpResponseRedirect("/courses/")
    
    return render_to_response("contents/topic/lesson/list.html", locals(),
                              context_instance=RequestContext(request))

def show_lesson(request, t_alias, l_alias):

    try:
        lesson = ContentAlias.objects.get(alias=l_alias).content_object    
    except:
        lesson = Lesson.objects.get(pk=int(l_alias))
    exercises = lesson.exercises.all()

    a_l = None
    try:
        l_id = int(l_alias)
    except:
        l_id = ContentAlias.objects.get(alias=l_alias).content_object.id

    # no admins allowed!
    if request.user.is_superuser:
        return HttpResponseRedirect("/courses/")
    
    if not request.user.is_anonymous():
        if Account_Lesson.objects.filter(
               Q(account=request.user.get_profile())&
               Q(lesson__id=l_id)
               ).count() == 1:
            a_l = Account_Lesson.objects.get(
                      Q(account=request.user.get_profile())&
                      Q(lesson__id=l_id)
                  )
    if not a_l:
        for e in exercises:
            e.is_finished = False
    else:
        for e in exercises:
            if (e.id,) in CompletedExercise.objects.filter(account_lesson=a_l).values_list("exercise__id"):
                e.is_finished = True
            else:
                e.is_finished = False
                
    q_types = SubType.objects.all()
                
    return render_to_response("contents/topic/lesson/view.html", locals(),
                              context_instance=RequestContext(request))