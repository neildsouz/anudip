from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.contents.topics.views',
    url(r'^$', 'home'),
    url(r'^get_topics/$', 'ajax_topics'),
    url(r'^(?P<t_alias>[\w\-]+)/lessons/$', 'topic_lessons'),
    url(r'^(?P<t_alias>[\w\-]+)/(?P<l_alias>[\w\-]+)/$', 'show_lesson'),
)