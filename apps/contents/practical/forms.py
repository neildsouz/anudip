#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms
from django.contrib.contenttypes.generic import generic_inlineformset_factory
from django.forms.models import inlineformset_factory, BaseInlineFormSet, \
    ModelForm
from django.utils.translation import ugettext_lazy as _
from openuniversity.apps.common.models import SubType, Difficulty
from openuniversity.apps.contents.contents_core.forms import \
    ContentAliasHiddenForm
from openuniversity.apps.contents.contents_core.models import ContentAlias
from openuniversity.apps.contents.files.forms import LessonVideoForm, \
    LessonImageForm, LessonPDFForm, ExerciseVideoFormSet, ExerciseVideoForm
from openuniversity.apps.contents.files.models import PDFFile, Video, Image
from openuniversity.apps.contents.practical.models import Lesson, Answer, \
    Question, Exercise_Question, Exercise
from openuniversity.apps.core.models import Points
from openuniversity.apps.translation.forms import \
    AutoCurrentLanguageTranslateAbleForm
from openuniversity.apps.translation.models import TranslateAble

class TextAnswerPracticeForm(forms.Form):
    answer = forms.CharField(max_length=50)
    
    def is_answer_correct(self, question=None):
        if question == None:
            return False
        else:
            return question.answer_set.all()[0].answer.strip().lower() == self.cleaned_data['answer'].strip().lower()

class SingleChoiceAnswerPracticeForm(forms.Form):
    answer = forms.ChoiceField(required=True, widget=forms.RadioSelect)
    
    def is_answer_correct(self, question=None):
        if question == None:
            return False
        else:
            return question.answer_set.get(is_correct=True).id == int(self.cleaned_data['answer'].strip())
        
class MultipleChoiceAnswerPracticeForm(forms.Form):
    answer = forms.MultipleChoiceField(required=True, widget=forms.CheckboxSelectMultiple)
    
    def is_answer_correct(self, question=None):
        if question == None:
            return False
        else:
            answers_to_check = [ int(a) for a in self.cleaned_data['answer'] ]
            correct_answers = [ int(c[0]) for c in question.answer_set.filter(is_correct=True).values_list("id") ]
            return answers_to_check == correct_answers
        
class LessonForm(AutoCurrentLanguageTranslateAbleForm):
    name = forms.CharField(max_length=50)
    description = forms.CharField(widget=forms.Textarea, max_length=12500)
    alias_nested = generic_inlineformset_factory(ContentAlias, 
                 max_num=1, can_delete=False)
    alias_nested_hidden = generic_inlineformset_factory(ContentAlias, 
                 max_num=1, form=ContentAliasHiddenForm, can_delete=False)
    pdf_nested = inlineformset_factory(TranslateAble, PDFFile,
        form=LessonPDFForm, formset=BaseInlineFormSet,
        extra=1, can_delete=True)
    video_nested = inlineformset_factory(TranslateAble, Video, 
        form=LessonVideoForm, formset=BaseInlineFormSet,
        extra=1, can_delete=True)
    image_nested = inlineformset_factory(Lesson, Image, 
        formset=BaseInlineFormSet, form=LessonImageForm, extra=1,
        can_delete=True)
    
    class Meta:
        model = Lesson
        fields = ("name", "description")

    def save(self, **kwargs):
        return super(LessonForm, self).save(commit=True)
    
    def save_m2m(self, *args, **kwargs):
        return super(LessonForm, self).save_m2m(*args, **kwargs)

    def make_translatable(self):
        for field_key in self.fields.keys():
            if field_key not in self.instance.get_trans_field_names():
                self.fields[field_key].widget=forms.HiddenInput()      

class AnswerBaseFormSet(BaseInlineFormSet):
                
    def save(self, *args, **kwargs):
        super(AnswerBaseFormSet, self).save(*args, **kwargs)

class QuestionAnswerForm(forms.ModelForm):
    
    class Meta:
        model = Answer 
        
class QuestionForm(AutoCurrentLanguageTranslateAbleForm):
    answer_type = forms.ModelChoiceField(
             queryset=SubType.objects.filter(table_name=Answer._meta.db_table))
    question = forms.CharField(widget=forms.TextInput, max_length=2500)
    difficulty = forms.ModelChoiceField(queryset=Difficulty.objects.all())
    hint = forms.CharField(widget=forms.TextInput, required=False, max_length=500)
    video_nested = inlineformset_factory(TranslateAble, Video,
                    extra=1, 
                    form=LessonVideoForm,
                    formset=BaseInlineFormSet,
                    can_delete=True)
    formset = inlineformset_factory(Question, Answer, formset=
                    AnswerBaseFormSet, 
                    extra=1,
                    form=QuestionAnswerForm, 
                    exclude=("question"),
                    can_delete=True)

    class Meta:
        model = Question
        exclude = ("exercises", "answered_accounts","votes", 
                   "suggest_status", "voted_accounts",
                   "submitted_by", "is_required")
        
    def save_m2m(self, **kwargs):
        pass
    
    def make_translatable(self):
        for field_key in self.fields.keys():
            if field_key not in self.instance.get_trans_field_names():
                self.fields[field_key].widget=forms.HiddenInput()
        

#-------------------------------Question Answer: END

class Exercise_QuestionModelForm(ModelForm):
    class Meta:
        model = Exercise_Question
        verbose_name_plural = "Exercise Questions"
        verbose_name = "Exercise Question"
        
#--------------------------------------Exercise: START
class Exercise_QuestionForm(AutoCurrentLanguageTranslateAbleForm):
    question = forms.CharField(widget=forms.TextInput, max_length=2500)
    answer_type = forms.ModelChoiceField(queryset=SubType.objects.filter(
                      table_name=Answer._meta.db_table))
    hint = forms.CharField(widget=forms.TextInput, required=False, max_length=500)
    points = forms.ModelChoiceField(queryset=Points.objects.all())
    is_published = forms.BooleanField(required=False, label=_("Publish this question?"))
    
    class Meta:
        model = Exercise_Question
        exclude = ("exercise", "created_date", "votes", 'is_required')
        fields = ('difficulty', 'question', 'hint', 'answer_type', 'points', 'is_published')
    
    def save(self, *args, **kwargs):
#        print self.cleaned_data
        
        if self.cleaned_data.has_key("id") and self.cleaned_data['id'] != None:
            eq = Exercise_Question.objects.get(pk=self.cleaned_data['id'].id)
            eq.difficulty = self.cleaned_data['difficulty'] 
            eq.save()
            
            q = Question.objects.get(id=eq.m2m_question.id)
            q.answer_type = self.cleaned_data['answer_type']
            q.points = self.cleaned_data['points']
            q.submitted_by = self.question_submitted_by
            q.is_published = self.cleaned_data['is_published']
            q.save()
            
            self.instance = q
        else:
            q = Question()
            q.answer_type = self.cleaned_data['answer_type']
            q.points = self.cleaned_data['points']
            q.submitted_by = self.question_submitted_by
            q.is_published = self.cleaned_data['is_published']
            q.save()

            eq = Exercise_Question()
            eq.m2m_question = q
            eq.m2m_exercise = self.exercise
            eq.difficulty = self.cleaned_data['difficulty'] 
            eq.save()
            self.instance = q
        return super(Exercise_QuestionForm, self).save(*args, **kwargs)
    
class QuestionBaseFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        if kwargs.has_key('question_submitted_by'): # has only when adding questions
            self.question_submitted_by = kwargs['question_submitted_by']
            del(kwargs['question_submitted_by'])
        super(QuestionBaseFormSet, self).__init__(*args, **kwargs)
    
    def add_fields(self, form, index):
        from django.utils import translation
        if self.instance.id:
            count = self.instance.questions.count()
            if index < count:
                question = Exercise_Question.objects.filter(m2m_exercise=self.instance).order_by("pk")[index].m2m_question
                form.fields['question'].initial = question.t9n(u"question", 
                                   language_code=translation.get_language(),
                                   allow_blank=True)
                form.fields['hint'].initial = question.t9n("hint", 
                                   language_code=translation.get_language(),
                                   allow_blank=True)
                form.fields['answer_type'].initial = question.answer_type
                form.fields['points'].initial = question.points
                form.fields['is_published'].initial = question.is_published
        super(BaseInlineFormSet, self).add_fields(form, index)
                
    def save(self, *args, **kwargs):
        print "QuestionBaseFormSet.save"
        for form in self.forms:
            form.exercise = self.instance
            form.question_submitted_by = self.question_submitted_by
        return super(QuestionBaseFormSet, self).save(*args, **kwargs)

class ExerciseForm(AutoCurrentLanguageTranslateAbleForm):
    from django.forms.models import inlineformset_factory
    
    name = forms.CharField(max_length=50)
    description = forms.CharField(widget=forms.Textarea)
    alias_nested_hidden = generic_inlineformset_factory(ContentAlias, 
                 max_num=1, form=ContentAliasHiddenForm, can_delete=False)

    alias_nested = generic_inlineformset_factory(ContentAlias, 
                 max_num=1, can_delete=False)
    
    formset = inlineformset_factory(Exercise, Exercise_Question, formset=
                    QuestionBaseFormSet, form=Exercise_QuestionForm, extra=1,
                    can_delete=True)
    video_formset = inlineformset_factory(Exercise, Video, formset=
                    ExerciseVideoFormSet, form=ExerciseVideoForm, extra=1,
                    can_delete=False)
    
    class Meta:
        model = Exercise
        exclude = ("questions", "account_topics")
        fields = ("name", "description", "bonus_points", )
    
    def save(self, **kwargs):
        return super(ExerciseForm, self).save(commit=True)
    
    def save_m2m(self, **kwargs):
        pass
    
    def make_translatable(self):
        for field_key in self.fields.keys():
            if field_key not in self.instance.get_trans_field_names():
                self.fields[field_key].widget=forms.HiddenInput()

class AnswerForm(ModelForm):
    
    class Meta:
        model = Answer

class QuestionAnswerModelForm(ModelForm):
    class Meta:
        model = Answer
        exclude = ("question")