#coding: utf-8
'''
Created on Sep 27, 2011

@author: javkhlan
'''
from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.contents.practical.views',
    url(r'^(?P<e_alias>[\w\-]+)/$', 'practice'),
    url(r'^(?P<e_alias>[\w\-]+)/(?P<q_id>\d+)/(?P<reset>\w+)$', 'practice'),
    url(r'^(?P<e_alias>[\w\-]+)/(?P<q_id>\d+)/$', 'practice'),
)