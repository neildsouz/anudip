from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _
from openuniversity.apps.common.models import SubType, Difficulty
from openuniversity.apps.content_installer.core.models import ContentOnOffMarker
from openuniversity.apps.contents.contents_core.models import Topic, \
    ContentAlias
from openuniversity.apps.contents.files.models import get_img_upload_path
from openuniversity.apps.core.models import Points, Account
from openuniversity.apps.translation.models import TranslateAble
from openuniversity.apps.core.models import Language

class Lesson(TranslateAble, ContentOnOffMarker):
    topic = models.ForeignKey(Topic, related_name="lessons")
    
    def save(self, *args, **kwargs):
        super(Lesson, self).save(*args, **kwargs) # Call the "real" save() method.
        self.create_default_exercise()
        
    def __unicode__(self):
        from django.utils import translation
        return self.t9n("name", translation.get_language(), show_language=True)

    def alias(self):
        lesson_type = ContentType.objects.get_for_model(self)
        try:
            return ContentAlias.objects.get(content_type__pk=lesson_type.id,
                                  object_id=self.id).__unicode__()
        except:
            return self.id
    def video(self):
        if self.video_set.count():
            from django.utils import translation
            try:
                return self.video_set.get(language__code=translation.get_language())
            except:
                return self.video_set.all()[0]
        else:
            return None
            
        
    def get_possible_total_points(self):
        points = 0
        for e in self.exercises.all():
            if e.bonus_points:
                points += e.bonus_points.points
            for q in e.questions.filter(is_published=True):
                points += q.points.points
        return points
    
    def get_question_points(self):
        points_num = 0
        for e in self.exercises.all():
            for q in e.questions.all():
                points_num += q.points.points
        return points_num
    
    def get_trans_field_names(self):    
        return ("name", "description")
    
    def create_thumbnails(self):
        import Image
        for image_item in self.image_set.all():
            print dir(image_item)
            im = Image.open(image_item.image_file.path)
            im.thumbnail((64, 64), Image.ANTIALIAS)
            im.save(image_item.image_file.path+"_" + "thumb", "JPEG")
    def delete(self, *args, **kwargs):
        retVal = models.Model.delete(self, *args, **kwargs)
        try:
            ContentAlias.objects.get(object_id=self.id).delete()
            for ca in ContentAlias.objects.all():
                if ca.content_object == None:
                    ca.delete()
        except:
            pass
        return retVal
    
    def videos_has_thumbs(self):
        return any(self.video_set.values_list("is_localized"))
    
    def create_default_exercise(self):
        exercises = Exercise.objects.filter(lesson=self)
        for exercise in exercises:
            if exercise.t9n(field_name="name") == "Student suggested practice":
                return
        exercise = Exercise()
        exercise.lesson = self
        exercise.save()
        kwargs = {"language": Language.objects.get(code="en-us"),
                  "name": "Student suggested practice",
                  "description":"A set of questions submitted by students themselves to help you learn this lesson",}
        exercise.saveTranslations(**kwargs)
  

class Question(TranslateAble):
    # 0 - text, 1 - single choice, 2 - multi choice.
    answer_type = models.ForeignKey(SubType, verbose_name=_("answer type")) 
    created_date = models.DateTimeField(verbose_name=_("created date"),
                                        auto_now=True)
    is_required  = models.BooleanField(verbose_name=_("is required"),
                                       default=False)       
    votes = models.IntegerField(verbose_name=_("votes"),
                                    default=0)
    voted_accounts = models.ManyToManyField(Account, 
            verbose_name=_("accounts voted"), related_name="voted_questions")
    points = models.ForeignKey(Points, verbose_name=_("points"))
    submitted_by = models.ForeignKey(Account, verbose_name=_("submitted by"), 
                                     null=True, blank=True)
    is_published = models.BooleanField(default=True, 
                                       verbose_name=_("Publish this question"))
    suggest_status = models.IntegerField(verbose_name=_("suggest status"), 
                                         default=0) # 0. moderator added 1. suggested 2. denied 3. approved

    def __unicode__(self):
        return self.t9n(field_name="question", current_language=True, 
                        show_language=True)
    def get_trans_field_names(self):
        return ("question", "hint")
    
    def video(self):
        if self.video_set.count():
            from django.utils import translation
            try:
                return self.video_set.get(language__code=translation.get_language())
            except:
                return self.video_set.all()[0]
        else:
            return None

    def has_video(self):
        return bool(self.video_set.count())
    
    def answer_image_value_list(self):
        answers = []
        for item in self.answer_set.all():
            if item.image_file:
                answers.append(
                   (item.id, item.answer + " <img width=\"400px\" src='/media/"+\
                    ((item.image_file and item.image_file.__unicode__()) or "")+\
                    "' />")
                )
            else:
                answers.append(
                   (item.id, item.answer)
                )
        return tuple(answers)

class Exercise(TranslateAble, ContentOnOffMarker):

    created_date = models.DateTimeField(verbose_name=_("created date"),
                                        auto_now_add=True)
    lesson = models.ForeignKey(Lesson, verbose_name=_("lesson"), 
                                    related_name="exercises")
    bonus_points = models.ForeignKey(Points, verbose_name=_("bonus points"), 
                                     null=True, blank=True)
    questions = models.ManyToManyField(Question, 
                                       verbose_name=_("questions"), 
                                       through="Exercise_Question",
                                       related_name="exercises", 
                                       null=True, 
                                       blank=True)
    
    def __unicode__(self):
        return self.t9n(field_name="name", current_language=True, 
                        show_language=True)
    def alias(self):
        exercise_type = ContentType.objects.get_for_model(self)
        try:
            return ContentAlias.objects.get(content_type__pk=exercise_type.id,
                                  object_id=self.id).__unicode__()
        except:
            return self.id

    def get_trans_field_names(self):
        return ("name", "description")
    
    def delete(self, *args, **kwargs):
        retVal = models.Model.delete(self, *args, **kwargs)
        try:
            ContentAlias.objects.get(object_id=self.id).delete()
            for ca in ContentAlias.objects.all():
                if ca.content_object == None:
                    ca.delete()
        except:
            pass
        return retVal
    
class Exercise_Question(models.Model):
    m2m_question = models.ForeignKey(Question)
    m2m_exercise = models.ForeignKey(Exercise) 
    difficulty = models.ForeignKey(Difficulty)  
    
    def __unicode__(self):
        return u"Exercise_Question: " + self.m2m_question.__unicode__() +\
             " -- " + self.m2m_exercise.__unicode__()

class Answer(models.Model):
    answer = models.CharField(max_length=200, null=True, blank=True)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question)
    image_file = models.ImageField(verbose_name=u"image", null=True,
                               blank=True, upload_to=get_img_upload_path)
    
    class Meta:
        pass
    
    def __unicode__(self):
        return self.question.__unicode__() + u" -- " + self.answer