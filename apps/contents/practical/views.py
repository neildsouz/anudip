#coding: utf-8
from datetime import datetime
from django import forms
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from openuniversity.apps.common.models import SubType
from openuniversity.apps.contents.contents_core.models import ContentAlias
from openuniversity.apps.contents.practical.forms import TextAnswerPracticeForm, \
    SingleChoiceAnswerPracticeForm, MultipleChoiceAnswerPracticeForm
from openuniversity.apps.contents.practical.models import Exercise, Question, \
    Answer
from openuniversity.apps.contents.stats.models import Account_Lesson_Question, \
    CompletedExercise
from openuniversity.apps.core.models import Account

# Create your views here.
def practice(request, e_alias, q_id=None, reset=None):
    if not request.user.is_anonymous():
        logged_account = Account.objects.get(user__id=request.user.id)
    else:
        logged_account = None
        
    try:
        practice_exercise = ContentAlias.objects.get(alias=e_alias).content_object
    except:
        practice_exercise = Exercise.objects.get(pk=int(e_alias))
    p_e_q = practice_exercise.questions.filter(is_published=True)
    p_e_q_count = p_e_q.count()
    if (q_id==None):
        form_action = 1
        if request.user.is_anonymous(): # added for anonymous users to be able to practice
            sign_in_reminder = True 
            no_resume=True
        else :
            q_id_int = 1
            q_id_int_i = 1
            if logged_account.account_lesson_set.filter(lesson=practice_exercise.lesson).count() == 0:
                a_l = logged_account.account_lesson_set.create(lesson=practice_exercise.lesson)
            else:
                a_l = logged_account.account_lesson_set.get(lesson=practice_exercise.lesson)
            a_l_q = Account_Lesson_Question.objects.filter(account_lesson=a_l, exercise=practice_exercise)
            
            if a_l_q.filter(is_answered=True).count()==0:
                no_resume=True
            elif CompletedExercise.objects.filter(account_lesson=a_l, exercise=practice_exercise).count() == 0 and p_e_q_count>0:
                for q in p_e_q[q_id_int%p_e_q.count():]:
                    q_id_int_i = ((q_id_int_i) % p_e_q_count) + 1
                    if logged_account:
                        if (q.id,) in a_l_q.filter(is_answered=False).values_list("question"):
                            form_action = q_id_int_i
                            break
            else:
                #Already completed no resume or no quesions
                no_resume=True
        if not p_e_q_count:
            no_questions = True
        
        return render_to_response("contents/practice/ask-start-practice.html", 
                          locals(), context_instance=RequestContext(request))
    
    
    
    if request.POST:
        if request.POST.has_key("question_id") and request.POST['question_id'] != "None":
            if request.POST['submit'] == u"answer":
                if request.POST['answer_type'].strip() == u"text":
#                    print "text"
                    form = TextAnswerPracticeForm(request.POST)
                elif request.POST['answer_type'].strip() == u"single":
#                    print "single"
                    form = SingleChoiceAnswerPracticeForm(request.POST)
                    form.fields['answer']._set_choices(tuple(Question.objects.get(pk=int(request.POST['question_id'])).answer_set.values_list("id", "answer")))
                elif request.POST['answer_type'].strip() == u"multiple":
#                    print "multiple"
                    form = MultipleChoiceAnswerPracticeForm(request.POST)
                    form.fields['answer']._set_choices(tuple(Question.objects.get(pk=int(request.POST['question_id'])).answer_set.values_list("id", "answer")))
                else:
#                    print "unknown answer type"
                    pass
                if logged_account:
                    a_l = logged_account.account_lesson_set.get(lesson=practice_exercise.lesson)
                    a_l_q_item = a_l.account_lesson_question_set.get(question__id=int(request.POST['question_id']))
                else:
                    sess_a_l = request.session['account_lesson_set'][practice_exercise.lesson.id]
                    sess_a_l_q = sess_a_l["exercises"][practice_exercise.id]
                    sess_a_l_q_item = [ sess_a_l_qi for sess_a_l_qi in sess_a_l_q if sess_a_l_qi.question.id==int(request.POST['question_id']) ][0]
                    
                if form.is_valid():
                    if form.is_answer_correct(question=Question.objects.get(pk=int(request.POST['question_id']))):
                        is_incorrect = False
                        if logged_account:
                            a_l_q_item.attempts += 1
                            a_l_q_item.is_answered = True
                            a_l_q_item.finish_time = datetime.now()
                            a_l_q_item.save()
                        else:
                            # hoping that it has a hard link to its containing list sess_a_t_q. 
                            # after an hour >> now worries it does have a one memory address
                            sess_a_l_q_item.is_answered = True 
                    else:
                        is_incorrect = True
                else:
                    is_incorrect = True
                if is_incorrect:
                    if logged_account:
                        a_l_q_item.attempts += 1
                        a_l_q_item.save()
                        request.user.message_set.create(message="Incorrect, please try again")
                    else:
                        sess_a_l_q_item.attempts += 1
                    return HttpResponseRedirect(
                            redirect_to=reverse(
                                practice, 
                                args=[
                                  e_alias, request.session['save_question']
                                      ]
                                )
                            )
            elif request.POST['submit'] == u"skip":
#                print "next"
                pass 
                
            
##################################################################################################################################
    q_id_int = int(q_id)
    #used when user starts an exercise:
    #account_topic, making new one or resetting the old one if there abnormally more than 1 account topics for given topic and user
    if(q_id_int==1 and not reset==None and not request.POST.has_key("question_id")):
        if logged_account:
            if logged_account.account_lesson_set.all().filter(
                    lesson=practice_exercise.lesson).all().count() > 1: # TODO
                for al in logged_account.account_lesson_set.filter(lesson=practice_exercise.lesson).all():
                    al.delete()
            
            if logged_account.account_lesson_set.filter(lesson=practice_exercise.lesson).count() == 0:
                a_l = logged_account.account_lesson_set.create(lesson=practice_exercise.lesson)
            else:
                a_l = logged_account.account_lesson_set.get(lesson=practice_exercise.lesson)
            
            if CompletedExercise.objects.filter(
                                Q(account_lesson=a_l)&
                                Q(exercise=practice_exercise)).count() > 0:
                CompletedExercise.objects.get(
                                  Q(account_lesson = a_l)&
                                  Q(exercise=practice_exercise)).delete()
            
            a_l.is_completed = False
            a_l.save()
                
            [ a_li.delete() for a_li in a_l.account_lesson_question_set.filter(exercise=practice_exercise) ] # deleting old account topic questions
            [ a_l.account_lesson_question_set.create(question=q, exercise=practice_exercise) for q in p_e_q ] # creating account topic questions
        else: # added for anonymous users to be able to practice
            if not request.session.has_key("account_lesson_set"):
                request.session['account_lesson_set'] = {}
            
            if not practice_exercise.lesson.id in request.session['account_lesson_set'].keys():
                request.session['account_lesson_set'][practice_exercise.lesson.id] = {}
            sess_a_l = request.session['account_lesson_set'][practice_exercise.lesson.id]
            
            if not sess_a_l.has_key('completed_exercises'):
                sess_a_l['completed_exercises'] = []
            if practice_exercise.id in sess_a_l['completed_exercises']:
                sess_a_l['completed_exercises'].remove(practice.exercise.id)
                sess_a_l['is_completed'] = False
            
            if not sess_a_l.has_key("exercises"):
                sess_a_l["exercises"] = {}
            sess_a_l["exercises"][practice_exercise.id] = [ Account_Lesson_Question(question=q, exercise=practice_exercise) for q in p_e_q ] # TODO
            request.session['account_lesson_set'][practice_exercise.lesson.id] = sess_a_l
    else:
        if logged_account:
            a_l = logged_account.account_lesson_set.get(lesson=practice_exercise.lesson)
        else:
            sess_a_l = request.session['account_lesson_set'][practice_exercise.lesson.id] 

    question = p_e_q[q_id_int-1]
    hint = question.t9n(field_name="hint", current_language=True, 
                        show_language=True)
    #############
    form_action = None
    q_id_int_i = q_id_int # q_id_int_i -- question id integer index
    if logged_account:
        a_l_q = Account_Lesson_Question.objects.filter(account_lesson=a_l, exercise=practice_exercise) # a_t_q account_topic questions
        a_l_q_i = a_l_q.get(question=question)
        a_l_q_i.start_time = datetime.now()
        a_l_q_i.save()
    else:
        sess_a_l_q = sess_a_l["exercises"][practice_exercise.id]
    
#    print "------------------------------------------------"
#    print "q_id_int: ", q_id_int
    if logged_account: 
        print "a_l: ", a_l
        print "a_l_q", a_l_q
    
#    print "available questions-1: ", p_e_q[q_id_int%p_e_q.count():]
#    print "available questions-2: ", p_e_q[:q_id_int]
#    print "start, end"
    
    for q in p_e_q[q_id_int%p_e_q.count():]:
        q_id_int_i = ((q_id_int_i) % p_e_q.count()) + 1
#        print "top loop: ", q_id_int_i
        if logged_account:
            if (q.id,) in a_l_q.filter(is_answered=False).values_list("question"):
#                print "top loop if: ", q_id_int_i
                form_action = q_id_int_i
                break
        else:
            print [ sess_a_l_qi.question.id for sess_a_l_qi in sess_a_l_q if not sess_a_l_qi.is_answered ]
            if q.id in [ sess_a_l_qi.question.id for sess_a_l_qi in sess_a_l_q if not sess_a_l_qi.is_answered ]:
#                print "sess top loop if: ", q_id_int_i
                form_action = q_id_int_i
                break

    if form_action == None:    
        q_id_int_i = 0
        for q in p_e_q[:q_id_int%p_e_q.count()]:
            q_id_int_i = ((q_id_int_i) % p_e_q.count()) + 1
#            print "bot loop: ", q_id_int_i
            if logged_account:
                if (q.id,) in a_l_q.filter(is_answered=False).values_list("question"):
    #                print "bot loop if: ", q_id_int_i
                    form_action = q_id_int_i
                    break
            else:
                if q.id in [ sess_a_l_qi.question.id for sess_a_l_qi in sess_a_l_q if not sess_a_l_qi.is_answered ]:
    #                print "bot loop if: ", q_id_int_i
                    form_action = q_id_int_i
                    break
#    print "form_action: ", form_action
    if form_action == None:
        if logged_account:
            if CompletedExercise.objects.filter(account_lesson=a_l, exercise=practice_exercise).count() == 0:
                #dont create more than one
                ce = CompletedExercise(account_lesson=a_l, exercise=practice_exercise).save()
        else:
            while practice_exercise.id in sess_a_l['completed_exercises']:
                sess_a_l['completed_exercises'].pop(practice_exercise.id)
            sess_a_l['completed_exercises'].append(practice_exercise.id)

        if logged_account:
#            if a_l.are_all_exercises_completed_in_the_lesson():
##                a_t.is_completed = True
#                a_l.completed_lessons.add(practice_exercise.lesson)
#                a_l.calculate_points()
#                a_l.save()
#    #            print "completed topic"
#    #            print "total: %d points" % a_t.points
#            else:
#    #            print "not completed topic"
#                pass
            return HttpResponseRedirect(reverse('report_view', kwargs={'account_lesson_id':a_l.id,
                                                                       'user_id':request.user.id}))
#            render_to_response("contents/practice/practice-result.html", 
#              {'topic':practice_exercise.lesson.topic, 'exercise':practice_exercise,
#               'questions_total': p_e_q.count(), 
#               'questions_answered': a_l_q.filter(is_answered=True, exercise=practice_exercise).count()},
#                        context_instance=RequestContext(request))
        else:
            if [ sess_a_l_qi.question.id for sess_a_l_qi in sess_a_l_q if not sess_a_l_qi.is_answered ]:
                a_l.calculate_points()
                a_l.save()
            else:
                print "sess not completed topic"
                pass
            return render_to_response("contents/practice/practice-result.html", 
                  {'topic':practice_exercise.lesson.topic, 'exercise':practice_exercise,
                   'questions_total': p_e_q.count(), 
                   'questions_answered': len([ sess_a_t_qi.question.id for sess_a_t_qi in sess_a_l_q if sess_a_t_qi.is_answered ])},
                            context_instance=RequestContext(request))
            

#    print type(SubType.objects.filter(table_name=Answer._meta.db_table)[0])
    if question.answer_type == SubType.objects.filter(table_name=Answer._meta.db_table).get(type_int=1):
        form = TextAnswerPracticeForm()
#        print "text answer practice form: question id ", question.id
        form.fields['answer_type'] = forms.CharField(widget=forms.HiddenInput, initial="text")
    elif question.answer_type == SubType.objects.filter(table_name=Answer._meta.db_table).get(type_int=2):
        form = SingleChoiceAnswerPracticeForm()
#        print "single answer practice form: question id  ", question.id
        form.fields['answer'].choices = question.answer_image_value_list()
#        form.fields['answer'].choices = tuple(question.answer_set.values_list("id", "answer"))
        form.fields['answer_type'] = forms.CharField(widget=forms.HiddenInput, initial="single")
    elif question.answer_type == SubType.objects.filter(table_name=Answer._meta.db_table).get(type_int=3):
        form = MultipleChoiceAnswerPracticeForm()
#        print "multiple answer practice form: question id  ", question.id
        form.fields['answer'].choices = question.answer_image_value_list()
        form.fields['answer_type'] = forms.CharField(widget=forms.HiddenInput, initial="multiple")
        pass
    else:
#        print "unidentified answer type"
        pass
    form.action = form_action
    request.session['save_question'] = int(q_id)    
    return render_to_response("contents/practice/practice.html", locals(),
                          context_instance=RequestContext(request))