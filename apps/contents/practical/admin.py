'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.contents.practical.forms import QuestionForm, \
    AnswerForm, ExerciseForm, Exercise_QuestionModelForm, LessonForm
from openuniversity.apps.contents.practical.models import Question, Answer, \
    Exercise, Exercise_Question, Lesson

class QuestionAdmin(admin.ModelAdmin):
    form = QuestionForm
admin.site.register(Question, QuestionAdmin)

class AnswerAdmin(admin.ModelAdmin):
    form = AnswerForm
admin.site.register(Answer, AnswerAdmin)

class ExerciseAdmin(admin.ModelAdmin):
    form = ExerciseForm
admin.site.register(Exercise, ExerciseAdmin)

class Exercise_QuestionAdmin(admin.ModelAdmin):
    form = Exercise_QuestionModelForm
admin.site.register(Exercise_Question, Exercise_QuestionAdmin)
    
class LessonAdmin(admin.ModelAdmin):
    form = LessonForm
admin.site.register(Lesson, LessonAdmin)