#coding: utf-8
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _
from openuniversity.apps.content_installer.core.models import ContentOnOffMarker
from openuniversity.apps.core.models import AgeGroup, Account
from openuniversity.apps.translation.models import TranslateAble
import os
import uuid

# Create your models here.
class Subject(TranslateAble, ContentOnOffMarker): #TODO: course changed into subject
    def __unicode__(self):
        from django.utils import translation
        return self.t9n("name", translation.get_language(), show_language=True)

    def alias(self):
        subject_type = ContentType.objects.get_for_model(self)
        return ContentAlias.objects.get(content_type__pk=subject_type.id,
                                  object_id=self.id)
    
    def alias_or_id(self):
        subject_type = ContentType.objects.get_for_model(self)
        try:
            return ContentAlias.objects.get(content_type__pk=subject_type.id,
                                  object_id=self.id).__unicode__()
        except:
            return self.id
    def delete(self, *args, **kwargs):
        retVal = models.Model.delete(self, *args, **kwargs)
        try:
            ContentAlias.objects.get(object_id=self.id).delete()
            for ca in ContentAlias.objects.all():
                if ca.content_object == None:
                    ca.delete()
        except:
            pass
        return retVal

class Topic(TranslateAble, ContentOnOffMarker): # TODO exercise was removed
    created_date = models.DateTimeField(verbose_name=_("created on"), 
                    auto_now=True)
    age_group = models.ForeignKey(AgeGroup, null=True, blank=True)
    subject = models.ForeignKey(Subject)
    
    def __unicode__(self):
        from django.utils import translation
        return self.t9n("name", translation.get_language(), show_language=True)
    
    def alias(self):
        topic_type = ContentType.objects.get_for_model(self)
        try:
            return ContentAlias.objects.get(content_type__pk=topic_type.id,
                                  object_id=self.id).__unicode__()
        except:
            return self.id
    
    def get_trans_field_names(self):
        return ("name", "description")
    
    def get_possible_total_points(self):
        points = 0
        for l in self.lessons.all():
            for e in l.exercises.all():
                points += e.bonus_points.points
                for q in e.questions.filter(is_published=True):
                    points += q.points.points
                    
                    
        return points
    def delete(self, *args, **kwargs):
        retVal = models.Model.delete(self, *args, **kwargs)
        try:
            ContentAlias.objects.get(object_id=self.id).delete()
            for ca in ContentAlias.objects.all():
                if ca.content_object == None:
                    ca.delete()
        except:
            pass
        return retVal

    
class ContentAlias(models.Model):
    """
    Alias for contents
    """
    alias = models.SlugField(unique=True, max_length=100)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    
    def __unicode__(self):
        return self.alias or ""