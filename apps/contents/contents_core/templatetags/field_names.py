'''
Created on Aug 26, 2011

@author: javkhlan
'''
from django import template
register = template.Library()

@register.filter(name='field_name')
def field_name(value):
    field_names = value._meta.get_all_field_names()
    if "id" in field_names:
        field_names.remove("id")
    if "translateable_ptr" in field_names:
        field_names.remove("translateable_ptr")
    if "translations" in field_names:
        field_names.remove("translations")
    print field_names
    return field_names