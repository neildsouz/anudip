from django import template
register = template.Library()

class T9NNode(template.Node):
    def __init__(self, var, field_name):
        self.var = template.Variable(var)
#        print self.var
        self.field_name = field_name
    def render(self, context):
#        print "context: ", context['LANGUAGE_CODE']
        actual_var = self.var.resolve(context)
        return actual_var.t9n(field_name=self.field_name, 
                     language_code=context['LANGUAGE_CODE'], show_language=True)

def do_t9n(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, var, trans_field = token.split_contents()
#        print variable, trans_field
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
        pass
    if not (trans_field[0] == trans_field[-1] and trans_field[0] in ('"', "'")):
        raise template.TemplateSyntaxError("%r tag's argument should be in quotes" % tag_name)
    return T9NNode(var, trans_field[1:-1])

register.tag('t9n', do_t9n)
