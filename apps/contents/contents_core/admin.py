from django.contrib import admin
from openuniversity.apps.contents.contents_core.forms import SubjectForm, \
    TopicForm, ContentAliasForm
from openuniversity.apps.contents.contents_core.models import Subject, Topic, \
    ContentAlias

class SubjectAdmin(admin.ModelAdmin):
    form = SubjectForm
admin.site.register(Subject, SubjectAdmin)

class TopicAdmin(admin.ModelAdmin):
    form = TopicForm
admin.site.register(Topic, TopicAdmin)
    
class ContentAliasAdmin(admin.ModelAdmin):
    form = ContentAliasForm
admin.site.register(ContentAlias, ContentAliasAdmin)