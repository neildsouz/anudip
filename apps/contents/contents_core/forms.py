#coding: utf-8
from django import forms
from django.contrib.contenttypes.generic import generic_inlineformset_factory
from openuniversity.apps.contents.contents_core.models import ContentAlias, \
    Subject, Topic
from openuniversity.apps.translation.forms import \
    AutoCurrentLanguageTranslateAbleForm

class ContentAliasForm(forms.ModelForm):
#    def __init__(self, *args, **kwargs):
#        print "ContentAliasForm"
#        return super(ContentAliasForm, self).__init__(*args, **kwargs)
#    
#    def add_initial_prefix(self, field_name):
#        print "add_initial_prefix"
#        return forms.ModelForm.add_initial_prefix(self, field_name)
#    
#    def add_prefix(self, field_name):
#        print "add prefix"
#        return forms.ModelForm.add_prefix(self, field_name)
#    
#    def clean(self):
#        print "clean:s"
#        a = forms.ModelForm.clean(self)
#        print "clean:f"
#        return a
#    
#    def full_clean(self):
#        print "full clean:s"
#        a = forms.ModelForm.full_clean(self)
#        print "full clean:f"
#        return a
#    
#    def has_changed(self):
#        print "has changed"
#        return forms.ModelForm.has_changed(self)
#    
#    def hidden_fields(self):
#        print "hidden fields"
#        return forms.ModelForm.hidden_fields(self)
#    
#    def is_multipart(self):
#        print "is_multipart"
#        return forms.ModelForm.is_multipart(self)
#    
#    def non_field_errors(self):
#        print "non field errors"
#        return forms.ModelForm.non_field_errors(self)
#    
#    def save(self, commit=True):
#        print "save"
#        return forms.ModelForm.save(self, commit=commit)
#    
#    def validate_unique(self):
#        print "validate unique"
#        forms.ModelForm.validate_unique(self)
#        
#    def visible_fields(self):
#        print "visible fields"
#        return forms.ModelForm.visible_fields(self)
#    
#    def _clean_fields(self):
#        print "clean fields:s"
#        forms.ModelForm._clean_fields(self)
#        print "clean fields:f"
#        
#    def _clean_form(self):
#        print "clean form:s"
#        print self.cleaned_data
#        forms.ModelForm._clean_form(self)
#        print "clean form:f"
#        
#    def _get_changed_data(self):
#        print "get changed data"
#        return forms.ModelForm._get_changed_data(self)
#    
#    def _get_errors(self):
#        print "get errors"
#        return forms.ModelForm._get_errors(self)
#    
#    def _get_media(self):
#        print "get media"
#        return forms.ModelForm._get_media(self)
#    
#    def _get_validation_exclusions(self):
#        print "get validation exclusions"
#        return forms.ModelForm._get_validation_exclusions(self)
#    
#    def _html_output(self, normal_row, error_row, row_ender, help_text_html, errors_on_separate_row):
#        print "html output"
#        return forms.ModelForm._html_output(self, normal_row, error_row, row_ender, help_text_html, errors_on_separate_row)
#    
#    def _post_clean(self):
#        print "post clean:s"
#        forms.ModelForm._post_clean(self)
#        print "post clean:f"
#        
#    def _raw_value(self, fieldname):
#        print "raw value"
#        return forms.ModelForm._raw_value(self, fieldname)
#    
#    def _update_errors(self, message_dict):
#        print "update errors"
#        forms.ModelForm._update_errors(self, message_dict)
#        
#    def is_valid(self, *args, **kwargs):
#        print "is_valid"
#        if super(ContentAliasForm, self).is_valid(*args, **kwargs):
#            print "ContentAliasForm.model_class: ", self.instance.model_class()
#            return True
#        else:
#            print "ContentAliasForm.model_class: ", self.instance.model_class()
#            return False
    
    class Meta:
        model = ContentAlias

class ContentAliasHiddenForm(forms.ModelForm):
    alias = forms.SlugField(widget=forms.HiddenInput)
    class Meta:
        model = ContentAlias

class SubjectForm(AutoCurrentLanguageTranslateAbleForm):
    alias_nested = generic_inlineformset_factory(ContentAlias, 
                 form=ContentAliasForm,
                 max_num=1,
                 can_delete=False) # TODO: must be required to add at least one
    alias_nested_hidden = generic_inlineformset_factory(ContentAlias, 
                 form=ContentAliasHiddenForm,
                 max_num=1,
                 can_delete=False,
                 )
    name = forms.CharField(max_length=50)
    class Meta:
        model = Subject
        fields = ("name",)

    def save(self, **kwargs):
        return super(SubjectForm, self).save(commit=True)
    
    def save_m2m(self, **kwargs):
#        super(SubjectForm, self).save_m2m(**kwargs)
        pass
    
    def make_translatable(self):
        for field_key in self.fields.keys():
            if field_key not in self.instance.get_trans_field_names():
                self.fields[field_key].widget=forms.HiddenInput()

class TopicForm(AutoCurrentLanguageTranslateAbleForm):
    name = forms.CharField(max_length=50)
    description = forms.CharField(widget=forms.Textarea, max_length=12500)
    alias_nested = generic_inlineformset_factory(ContentAlias, 
                 max_num=1,
                 can_delete=False)
    alias_nested_hidden = generic_inlineformset_factory(ContentAlias, 
                 form=ContentAliasHiddenForm,
                 max_num=1,
                 can_delete=False,
                 )
    """
    video_formset = inlineformset_factory(TranslateAble, Video, 
        formset=BaseInlineFormSet,         
        form=TopicVideoForm, extra=1,
        can_delete=True)
    """
        
    class Meta:
        model = Topic
        exclude = ("accounts", "subject")
        fields = ("name", "description", "age_group")
    
    def save(self, **kwargs):
        return super(TopicForm, self).save(**kwargs)
    
    def save_m2m(self, *args, **kwargs):
        return super(TopicForm, self).save_m2m(*args, **kwargs)
    
    def make_translatable(self):
        for field_key in self.fields.keys():
            if field_key not in self.instance.get_trans_field_names():
                self.fields[field_key].widget=forms.HiddenInput()