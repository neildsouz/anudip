#coding: utf-8
'''
Created on Aug 11, 2011

@author: javkhlan
'''
from openuniversity.apps.contents.practical.models import Lesson
from openuniversity.apps.contents.stats.models import Account_Lesson

def get_suggested_lessons(user, total=3):
    account = user.get_profile()
    clessons = Lesson.objects.filter(id__in=Account_Lesson.objects.filter(
                    account=account, is_completed=True).values("lesson__id"))
    
    import datetime
    suggested_lessons = []
    """
    before = datetime.datetime(year=account.birthdate.year, month=account.birthdate.month,
                      day=account.birthdate.day)
    now = datetime.datetime.now()
    for t in Topic.objects.exclude(id__in=ctopics.values_list("id")):
        st = int(t.age_group.age.strip().split("-")[0])
        fn = int(t.age_group.age.strip().split("-")[1])
        if int((now-before).days/365) <= fn and int((now-before).days/365) >= st:
            suggested_lessons.append(t)
    """
    return suggested_lessons