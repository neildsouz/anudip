from django.db import models
from django.utils.translation import ugettext_lazy as _
from openuniversity.apps.core.models import Language, Points
from openuniversity.apps.translation.models import TranslateAble
import commands
import os
import random

def get_pdf_upload_path(instance, filename):
    if hasattr(instance.content, "lesson") and instance.content.lesson != None:
        return os.path.join(
            "content", 
            "%s" % instance.content.lesson.topic.subject.alias_or_id(),
            "%s" % instance.content.lesson.topic.alias(),
            "%s" % instance.content.lesson.alias(),
            "pdf",
            filename)
        
def get_img_upload_path(instance, filename):
    if hasattr(instance, "content"):
        c = instance.content
        if hasattr(c, "lesson") and c.lesson != None:
            return os.path.join(
                "content", 
                "%s" % c.lesson.topic.subject.alias_or_id(),
                "%s" % c.lesson.topic.alias(),
                "%s" % c.lesson.alias(),
                "img",
                filename)
    elif hasattr(instance, "question") and instance.question != None:
        return os.path.join(
           "content", 
            "%s" % instance.question.exercises.all()[0].lesson.topic.subject.alias_or_id(),
            "%s" % instance.question.exercises.all()[0].lesson.topic.alias(),
            "%s" % instance.question.exercises.all()[0].lesson.alias(),
            "exercises",
            "%s" % instance.question.exercises.all()[0].alias(),
            "%s" % instance.question.id,
            "answers",
            filename)

def get_video_upload_path(instance, filename):
    c = instance.content
    if hasattr(c, "lesson") and c.lesson != None:
        return os.path.join(
            "content", 
            "%s" % c.lesson.topic.subject.alias_or_id(),
            "%s" % c.lesson.topic.alias(),
            "%s" % c.lesson.alias(),
            "vdo",
            filename)
    elif hasattr(c, "question") and c.question != None:
        print "question"
        return os.path.join(
            "content", 
            "%s" % c.question.exercises.all()[0].lesson.topic.subject.alias_or_id(),
            "%s" % c.question.exercises.all()[0].lesson.topic.alias(),
            "%s" % c.question.exercises.all()[0].lesson.alias(),
            "exercises",
            "%s" % c.question.exercises.all()[0].alias(),
            "%s" % c.question.id,
            "vdo",
            filename)
def get_subtitle_upload_path(instance, filename):
    c = instance.content
    if hasattr(c, "lesson") and c.lesson != None:
        return os.path.join(
            "content", 
            "%s" % c.lesson.topic.subject.alias_or_id(),
            "%s" % c.lesson.topic.alias(),
            "%s" % c.lesson.alias(),
            "vdo",
            filename)
    elif hasattr(c, "question") and c.question != None:
        print "question"
        return os.path.join(
            "content", 
            "%s" % c.question.exercises.all()[0].lesson.topic.subject.alias_or_id(),
            "%s" % c.question.exercises.all()[0].lesson.topic.alias(),
            "%s" % c.question.exercises.all()[0].lesson.alias(),
            "exercises",
            "%s" % c.question.exercises.all()[0].alias(),
            "%s" % c.question.id,
            "vdo",
            filename)

def get_upload_path(instance, filename):
    return ""

# Create your models here.
class Video(models.Model):
    url_path = models.URLField(null=True, blank=True)
    url_path_embed = models.URLField(null=True, blank=True)
    file = models.FileField(upload_to=get_video_upload_path, null=True, 
            blank=True, verbose_name=_("file with a 'flv' format"))
    subtitle = models.FileField(upload_to=get_subtitle_upload_path, 
            null=True, blank=True, verbose_name=_("subtitle file"))
    is_localized = models.BooleanField()
    language = models.ForeignKey(Language)
    created_date = models.DateTimeField(auto_now=True)
    content = models.ForeignKey(TranslateAble, null=True, blank=True)
    points = models.ForeignKey(Points, null=True, blank=True)
    has_thumbnails = models.BooleanField(default=False)
    thumbnail_path = models.FilePathField(null=True, blank=True)
        
    def __unicode__(self):
        return (self.is_localized and self.file) or self.url_path
    
    def subtitle_file(self):
        return (self.is_localized and self.subtitle) or ""
    
    def create_video_thumbnails(self):
        print "------------"
        file_separator = "/"
        name = self.file.path.split(file_separator)[-1]
        pwd = file_separator.join(self.file.path.split(file_separator)[:-1])
        duration = int(commands.getoutput("""ffmpeg -i %s 2>&1 | grep totalduration | awk '{ print $3 }'""" % self.file.path))
        frame_rate = int(commands.getoutput("""ffmpeg -i %s 2>&1 | grep framerate | awk '{ print $3 }'""" % self.file.path))
        total_frames = int(duration) * int(frame_rate)
        for i in range(1, 6):
            multiplier = random.random() * total_frames / total_frames
            print float(duration*multiplier)
            print duration, multiplier
            print "ffmpeg  -ss %f -i %s -f image2 -vframes 1 %s.jpg" % (
                duration*multiplier, self.file.path, pwd+file_separator+name.split(".")[0]+"-"+str(i))
            commands.getoutput("ffmpeg  -ss %f -i %s -f image2 -vframes 1 %s.jpg" % (
                duration*multiplier, self.file.path, pwd+file_separator+name.split(".")[0]+"-"+str(i)))
    def get_thumbnails(self):
        if self.has_thumbnails:
            file_separator = "/"
            name = self.file.path.split(file_separator)[-1]
            return [ file_separator.join(self.file.__unicode__().split(file_separator)[:-1]) + file_separator + name.split(".")[0]+"-"+str(t)+".jpg" for t in range(1,6) ]

class PDFFile(models.Model):
    file = models.FileField(upload_to=get_pdf_upload_path, verbose_name=_("file"))
    is_localized = models.BooleanField(default=False)
    url_path = models.URLField(null=True, blank=True)
    content = models.ForeignKey(TranslateAble, null=True, blank=True)
    name = models.CharField(max_length=30, blank=True)
    
    def save(self, *args, **kwargs):
        if not self.name:
            file_separator = "/"
            self.name = self.file.path.split(file_separator)[-1]
        super(PDFFile, self).save(*args, **kwargs)
    
class Image(models.Model):  
    image_file = models.ImageField(upload_to=
                                get_img_upload_path, null=True,blank=True)
    content = models.ForeignKey(TranslateAble)
    is_localized = models.BooleanField()

    def __unicode__(self):
        return self.image_file
    
    def thumbnail(self):
        return self.image_file.file.__unicode__() + u"_thumb"