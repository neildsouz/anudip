#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.contents.files.forms import VideoForm
from openuniversity.apps.contents.files.models import Video

class VideoAdmin(admin.ModelAdmin):
    form = VideoForm
admin.site.register(Video, VideoAdmin)
