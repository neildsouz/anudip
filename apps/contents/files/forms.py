#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms
from django.forms.models import ModelForm, BaseInlineFormSet
from openuniversity.apps.contents.files.models import Video, Image, PDFFile

class LessonImageForm(ModelForm):
    
    class Meta:
        model = Image
        exclude = ("lesson", "is_localized")
    def save(self,*args, **kwargs):
        if len(self.files):
            self.instance.is_localized = True
        retVal = super(LessonImageForm,self).save(*args, **kwargs)
        return retVal
    
class LessonPDFForm(ModelForm):
    
    class Meta:
        model = PDFFile
        exclude = ("is_localized")
    def save(self,*args, **kwargs):
        if len(self.files):
            self.instance.is_localized = True
        retVal = super(LessonPDFForm,self).save(*args, **kwargs)
        return retVal
    
class LessonVideoForm(ModelForm):
    url_path = forms.CharField(required=False)

    class Meta:
        model = Video
        exclude = ("is_localized", "type", "has_thumbnails", "thumbnail_path")
        
    def save(self, *args, **kwargs):
        if len(self.files): # TODO: temporary
            self.instance.is_localized = True
        return super(LessonVideoForm, self).save(*args, **kwargs)
        #TODO: youtube-dl runs here

class ExerciseVideoBaseFormSet(BaseInlineFormSet):
    def add_fields(self, form, index):
        if self.instance.id:
            count = self.instance.questions.count()
            if index < count:
                question = self.instance.questions.all()[index]
                form.fields['question'].initial = question.question
                form.fields['answer_type'].initial = question.answer_type
                form.fields['created_date'].initial = question.created_date
                form.fields['hint'].initial = question.hint
                form.fields['is_required'].initial = question.is_required
                form.fields['votes'].initial = question.votes
                form.fields['points'].initial = question.points
                form.fields['language'].initial = question.language
                form.fields['submitted_by'].initial = question.submitted_by
                form.fields['is_published'].initial = question.is_published
        super(BaseInlineFormSet, self).add_fields(form, index)
        pass
    pass

class TopicVideoForm(ModelForm):
    url_path = forms.CharField(required=False)
#    type = forms.ModelChoiceField(queryset=SubType.objects.filter(
#                      table_name=Video._meta.db_table))
    class Meta:
        model = Video
        exclude = ("exercise", "is_localized", "type")
        
    def save(self, *args, **kwargs):
        if len(self.files): # TODO: temporary
            self.instance.is_localized = True
        return super(TopicVideoForm, self).save(*args, **kwargs)
    
class ExerciseVideoFormSet(BaseInlineFormSet):
    
#    def add_fields(self, form, index):
#        if self.instance.id:
#            count = self.instance.video_set.count()
#            if index < count:
#                video = self.instance.video_set.all()[index]
#                form.fields['type'].initial = video.type # used to fill initial data in overriden model form fields
#        super(BaseInlineFormSet, self).add_fields(form, index)
    def save(self, *args, **kwargs):
#        super(QuestionBaseFormSet, self).save(*args, **kwargs) 
        for form in self.forms:
            form.exercise = self.instance
            form.save()


class ExerciseVideoForm(ModelForm):
#    type = forms.ModelChoiceField(queryset=SubType.objects.filter(table_name=Video._meta.db_table))
    class Meta:
        model = Video
        exclude = ("exercise", "topic", "is_localized", "type")
        
    def save(self, *args, **kwargs):
        if len(self.files): # TODO: temporary
            self.instance.is_localized = True
        super(ModelForm, self).save(*args, **kwargs)
        #TODO: youtube-dl runs here

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video


