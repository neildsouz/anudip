'''
Created on Oct 4, 2011

@author: javkhlan
'''
from openuniversity.apps.contents.contents_core.models import Topic, Subject
from openuniversity.apps.contents.practical.models import Question, Exercise, \
    Lesson

def validate_course_content_ids(content_ids):
    """
    content ids must be integers
    """
    global subjects
    subjects = []
    find_upper(content_ids, None, None)
    return subjects
    
subjects = []

def find_upper(content_ids, content, ctype):
    global subjects
    
    if ctype == None:
        for c_id in content_ids:
            if Question.objects.filter(pk=c_id).count() == 1:
                question = Question.objects.get(pk=c_id)
                find_upper(content_ids, question, ctype=Question).append({"id":question.id})

    elif ctype == Question:
        return find_upper(content_ids, content.exercises.all()[0], Exercise)
    
    elif ctype == Exercise:
        exercise_list = find_upper(content_ids, content.lesson, Lesson)
        for eli in exercise_list:
            if eli['id'] == content.id:
                return eli['questions']
        e = {"id":content.id, "questions":[]}
        exercise_list.append(e)
        return e['questions']
        
    elif ctype == Lesson:
        lesson_list = find_upper(content_ids, content.topic, Topic)
        for lli in lesson_list:
            if lli['id'] == content.id:
                return lli['exercises']
        l = {"id":content.id, "exercises":[]}
        lesson_list.append(l)
        return l['exercises']
        
    elif ctype == Topic:
        topic_list = find_upper(content_ids, content.subject, Subject)
        for tli in topic_list:
            if tli['id'] == content.id:
                return tli['lessons']
        t = {"id":content.id, "lessons":[]}
        topic_list.append(t)
        return t['lessons']
        
    elif ctype == Subject:
        for s in subjects:
            if s['id'] == content.id:
                return s['topics']
        s = {"id":content.id, 'topics':[]}
        subjects.append(s)
        return s['topics']