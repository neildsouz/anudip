'''
Created on Sep 30, 2011

@author: javkhlan
'''
from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.contents.courses.views',
    url(r'^$', 'my_courses', name="my_courses"),
    url(r'^add', "course_add"),
#    url(r'^get_courses/$', 'ajax_courses'),
#    url(r'^(?P<c_alias>[\w\-]+)/lessons/$', 'course_lessons'),
#    url(r'^(?P<c_alias>[\w\-]+)/(?P<l_alias>[\w\-]+)/$', 'show_lesson'),
)