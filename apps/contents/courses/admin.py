'''
Created on Oct 6, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.contents.courses.forms import CourseForm
from openuniversity.apps.contents.courses.models import Course

class CourseAdmin(admin.ModelAdmin):
    form = CourseForm
admin.site.register(Course, CourseAdmin)