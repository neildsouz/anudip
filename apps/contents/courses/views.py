#coding: utf-8
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from openuniversity.apps.common.models import SubType
from openuniversity.apps.contents.contents_core.models import Subject, Topic
from openuniversity.apps.contents.courses.forms import CourseForm
from openuniversity.apps.contents.courses.models import CourseContents, Course
from openuniversity.apps.contents.courses.util import \
    validate_course_content_ids
from openuniversity.apps.contents.practical.models import Lesson, Exercise, \
    Question

def my_courses(request):
    return render_to_response("contents/courses/home.html", locals(),
                              context_instance=RequestContext(request))

#def ajax_courses(request):
#    from django.db.models.query_utils import Q
#    subjects = [int(s) for s in request.GET['s'].strip().split(",")[1:]]
#    age_groups = [int(ag) for ag in request.GET['ag'].strip().split(",")[1:]]
#    topics = Topic.objects.all()
#    if age_groups:
#        topics = topics.filter(age_group__id__in=age_groups)
#    if subjects:
#        topics = topics.filter(subject__id__in=subjects)
#    print topics
#    return render_to_response("contents/topic/ajax_topics.html", locals(), 
#                              context_instance=RequestContext(request))
#    
#def course_lessons(request, t_alias):
#
#    id = None
#    try:
#        id = int(t_alias)
#        topic = Topic.objects.get(pk=id)
#    except:
#        topic = ContentAlias.objects.get(alias=t_alias).content_object    
#    lessons = topic.lessons.all()
#    a_t = None
#
#    if request.user.is_superuser:
#        return HttpResponseRedirect("/courses/")
#    
#    if not request.user.is_anonymous():
#        if Account_Topic.objects.filter(Q(account=request.user.get_profile())
#                                        &Q(topic__id=id)).count() == 1:
#            a_t = Account_Topic.objects.get(
#                        Q(account=request.user.get_profile())&Q(topic__id=id))
#    
#    if not a_t:
#        for l in lessons:
#            for e in l.exercises.all():
#                e.is_finished = False
#    else:
#        for l in lessons:
#            for e in l.exercises.all():
#                if (e.id,) in a_t.completed_exercises.values_list("id"):
#                    e.is_finished = True
#                else:
#                    e.is_finished = False
#                
#    return render_to_response("contents/topic/lesson/list.html", locals(),
#                              context_instance=RequestContext(request))
#
#def show_lesson(request, t_alias, l_alias):
#
#    try:
#        lesson = ContentAlias.objects.get(alias=l_alias).content_object    
#    except:
#        lesson = Lesson.objects.get(pk=int(l_alias))
#    exercises = lesson.exercises.all()
#
#    a_t = None
#    try:
#        t_id = int(t_alias)
#    except:
#        t_id = ContentAlias.objects.get(alias=t_alias).content_object.id
#
#    # no admins allowed!
#    if request.user.is_superuser:
#        return HttpResponseRedirect("/courses/")
#    
#    if not request.user.is_anonymous():
#        if Account_Topic.objects.filter(
#               Q(account=request.user.get_profile())&
#               Q(topic__id=t_id)
#               ).count() == 1:
#            a_t = Account_Topic.objects.get(
#                      Q(account=request.user.get_profile())&
#                      Q(topic__id=t_id)
#                      )
#    
#    if not a_t:
#        for e in exercises:
#            e.is_finished = False
#    else:
#        for e in exercises:
#            if (e.id,) in a_t.completed_exercises.values_list("id"):
#                e.is_finished = True
#            else:
#                e.is_finished = False
#                
#    return render_to_response("contents/topic/lesson/view.html", locals(),
#                              context_instance=RequestContext(request))
#    
def course_add(request):
    
    form = CourseForm()
    course = Course()
    
    toplevel = []
    for topic in Topic.objects.all():
        t = {"lessons":[]}
        t['id'] = topic.id
        t['name'] = topic.t9n("name")
        toplevel.append(t)
        for lesson in topic.lessons.all():
            l = {"exercises":[]}
            l['id'] = lesson.id
            l['name'] = lesson.t9n("name")
            t['lessons'].append(l)
            for exercise in lesson.exercises.all():
                e = {"questions":[]}
                e['id'] = exercise.id
                e['name'] = exercise.t9n("name")
                l['exercises'].append(e)
                for question in exercise.questions.all():
                    q = {'id':question.id,
                         'question':question.t9n("question")}
                    e['questions'].append(q)

    if request.POST:
        print "courses_add::request.POST"
        if request.POST.has_key("content_id"):
            course.created_account = request.user.get_profile()
            form = CourseForm(request.POST, instance=course)
            print "name: ", request.POST['name']
            print "description: ", request.POST['description']
            if form.is_valid():
                course = form.save()
                unicode_id_list = dict(request.POST)['content_id']
                int_id_list = [ int(ci) for ci in set(unicode_id_list) ]
                course_content_ids = validate_course_content_ids(int_id_list)
                print course_content_ids
                for s in course_content_ids: # toplevel list, is subject list
                    m2m_course_content = CourseContents(
                       course=course,
                       content=Subject.objects.get(pk=s['id']),
                       added_account=request.user.get_profile(),
                       content_level=SubType.objects.get(type_int=10)
                    )
                    m2m_course_content.save()
                    for t in s['topics']:
                        m2m_course_content = CourseContents(
                           course=course,
                           content=Topic.objects.get(pk=t['id']),
                           added_account=request.user.get_profile(),
                           content_level=SubType.objects.get(type_int=11)
                        )
                        m2m_course_content.save()
                        for l in t['lessons']:
                            m2m_course_content = CourseContents(
                               course=course,
                               content=Lesson.objects.get(pk=l['id']),
                               added_account=request.user.get_profile(),
                               content_level=SubType.objects.get(type_int=12)
                            )
                            m2m_course_content.save()
                            for e in l['exercises']:
                                m2m_course_content = CourseContents(
                                   course=course,
                                   content=Exercise.objects.get(pk=e['id']),
                                   added_account=request.user.get_profile(),
                                   content_level=SubType.objects.get(
                                                             type_int=13)
                                )
                                m2m_course_content.save()
                                for q in e['questions']:
                                    m2m_course_content = CourseContents(
                                       course=course,
                                       content=Question.objects.get(pk=q['id']),
                                       added_account=request.user.get_profile(),
                                       content_level=SubType.objects.get(
                                                                 type_int=14)
                                    )
                                    m2m_course_content.save()
                                    
                return HttpResponseRedirect(reverse(my_courses))
            else:
                print form.errors
                print "errors"
                return render_to_response("contents/courses/add.html", locals(),
                              context_instance=RequestContext(request))
                    
    return render_to_response("contents/courses/add.html", locals(),
                              context_instance=RequestContext(request))