'''
Created on Oct 6, 2011

@author: javkhlan
'''
from django import forms
from openuniversity.apps.contents.courses.models import Course
from openuniversity.apps.translation.forms import \
    AutoCurrentLanguageTranslateAbleForm
from openuniversity.apps.common.models import SubType

class CourseForm(AutoCurrentLanguageTranslateAbleForm):
    
    name = forms.CharField(max_length=50)
    description = forms.CharField(max_length=12500, widget=forms.Textarea)
    course_type = forms.ModelChoiceField(
             queryset=SubType.objects.filter(table_name=Course._meta.db_table),
             widget=forms.RadioSelect)
    
    class Meta:
        model = Course
        exclude = ("contents", "created_account")
        