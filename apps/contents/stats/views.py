from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect
from openuniversity.apps.contents.stats.utils import Report
from openuniversity.apps.contents.stats.models import Account_Lesson
from openuniversity.apps.moderator.views import is_moderator as mod_check

@login_required
def view_report(request, user_id, account_lesson_id):
    try:
        profile_obj = request.user.get_profile()
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('profiles_create_profile'))
    
    user_id = long(user_id)
    is_moderator = mod_check(profile_obj.user)
    if is_moderator:
        teacher = True
        reported_fullname = get_object_or_404(User, id=user_id).get_full_name()
    elif user_id == profile_obj.user.id:
        teacher = False
        reported_fullname = profile_obj.user.get_full_name()
    else:
        #Cant let a student view someone else's report :)
        return HttpResponseRedirect(reverse('openuniversity.apps.home.views.index'))
    
    account_lesson = Account_Lesson.objects.get(id=account_lesson_id)
    report = Report(account_lesson, teacher)
    
    return render_to_response( "contents/stats/view-report.html", 
                               locals(), context_instance=RequestContext(request))