from openuniversity.apps.contents.stats.models import Account_Lesson, Account_Lesson_Question
from openuniversity.apps.contents.practical.forms import TextAnswerPracticeForm, \
    SingleChoiceAnswerPracticeForm, MultipleChoiceAnswerPracticeForm
from openuniversity.apps.contents.practical.models import Answer
from openuniversity.apps.common.models import SubType
from datetime import timedelta

class ReportQnA:
    def __init__(self, question, account_lesson, teacher):
        self.question = question
        
        #assuming there should be one a_l_q for a given a_l
        account_lesson_question = Account_Lesson_Question.objects.filter(account_lesson=account_lesson,question=question).values()[0]
        self.is_answered = account_lesson_question['is_answered']
        start_time = account_lesson_question['start_time']
        finish_time = account_lesson_question['finish_time']
        if start_time and finish_time and finish_time >= start_time:
            time_taken = (finish_time - start_time).seconds
            if time_taken == 0:
                self.time_taken = "1 second" #0 seconds looks odd on the ui
            elif time_taken < 60:
                self.time_taken = str(time_taken) + " seconds"
            else :
                self.time_taken = str(time_taken/60) + " minutes"
        else:
            self.time_taken = "Not available"
        self.attempts = account_lesson_question['attempts']
        self.hint_is_used = account_lesson_question['hint_is_used']
        
        self.teacher = teacher
        
        show_answer = self.get_show_answer()
        self.answerform = self.get_answer_form(show_answer)
        
    def get_show_answer(self):
        #always show answer to teacher
        if self.teacher:
            return True
        #show answer only if student has already answered it
        elif self.is_answered:
            return True
        #WTF?
        else:
            return False
        
    def get_answer_form(self, show_answer):
        # 0 - text, 1 - single choice, 2 - multi choice.
        if self.question.answer_type == SubType.objects.filter(table_name=Answer._meta.db_table).get(type_int=1):
            correct_answers = self.question.answer_set.get(is_correct=True)
            if(show_answer):
                data = {'answer':correct_answers}
            else:
                data = {}
            form = TextAnswerPracticeForm(data)
        elif self.question.answer_type == SubType.objects.filter(table_name=Answer._meta.db_table).get(type_int=2):
            correct_answers = self.question.answer_set.get(is_correct=True)
            if(show_answer):
                data = {'answer':correct_answers.id}
            else:
                data = {}
            form = SingleChoiceAnswerPracticeForm(data)
            form.fields['answer'].choices = self.question.answer_image_value_list()
        elif self.question.answer_type == SubType.objects.filter(table_name=Answer._meta.db_table).get(type_int=3):
            correct_answers = self.question.answer_set.filter(is_correct=True)
            if(show_answer):
                answers_to_check = [ int(c[0]) for c in correct_answers.values_list("id") ]
                data = {'answer':answers_to_check}
            else:
                data = {}
            form = MultipleChoiceAnswerPracticeForm(data)
            form.fields['answer'].choices = self.question.answer_image_value_list()
        else:
            #WTF
            form = None
        return form
        
    
class ReportExercise:
    def __init__(self, exercise, account_lesson, teacher):
        self.exercise = exercise
        self.reportqnas = []
        for question in exercise.questions.filter(is_published=True):
            self.reportqnas.append(ReportQnA(question, account_lesson, teacher))

class Report:
    def __init__(self, account_lesson, teacher):
        self.account_lesson = account_lesson
        self.reportexercises = []
        for exercise in account_lesson.lesson.exercises.all():
            self.reportexercises.append(ReportExercise(exercise, account_lesson, teacher))
    