# coding: utf-8
# Create your models here.
from django.db import models
from openuniversity.apps.contents.practical.models import Question, Exercise, \
    Lesson
from openuniversity.apps.core.models import Account

class Account_Lesson(models.Model):
    """
        Account_Topic is a many2many table containing data on the progress
        of a user on the given topic
    """
    account = models.ForeignKey(Account)
    lesson = models.ForeignKey(Lesson)
    video = models.BooleanField(default=False)
    textbook = models.BooleanField(default=False)
    submitted_questions_num = models.IntegerField(default=0)
    started_date = models.DateTimeField(null=True, blank=True)
    finished_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    points = models.IntegerField(default=0)
    
    def __unicode__(self):
        return u"Account_Lesson: " + unicode(self.id)
    
    def are_all_exercises_completed_in_the_lesson(self):
        return [ e.pk for e in self.lesson.exercises.all() ].sort() == \
            [ e.pk for e in self.completed_exercises.all() ].sort()
    
    def calculate_points(self, recalculate=True):
        """
            called once the topic is completed by a given user
        """
        if recalculate:
            points = 0
            for a_t_qi in self.account_topic_question_set.all():
                points += a_t_qi.question.points.points
        #            print [ a_t_qi.id, a_t_qi.question.points.points ]
            if self.video:
                points += self.video_set.all()[0].points.points
            self.points = points
        return self.points

class Account_Lesson_Question(models.Model):
    """
    Many to many key holder for AccountLesson and Question tables,
    in addition to the foreign keys it has fields related to the statistics of
    the question and account interaction
    """
    account_lesson = models.ForeignKey(Account_Lesson)
    question = models.ForeignKey(Question)
    exercise = models.ForeignKey(Exercise)
    is_answered = models.BooleanField(default=False)
    start_time = models.DateTimeField(null=True, blank=True) 
    finish_time = models.DateTimeField(null=True, blank=True)
    attempts = models.IntegerField(default=0)
    hint_is_used = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.account_lesson.__unicode__() + unicode(" -- ") +\
            unicode(self.is_answered)

class CompletedExercise(models.Model):
    """
    Many to many key holder for account lesson and exercise tables,
    in addition to foreign keys it has a completed data field
    """
    account_lesson = models.ForeignKey(Account_Lesson, related_name="completed_exercises")
    exercise = models.ForeignKey(Exercise)
    completed_date = models.DateTimeField(auto_now=True)