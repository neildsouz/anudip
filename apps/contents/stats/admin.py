#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.contents.stats.forms import Account_LessonForm, \
    Account_Topic_QuestionForm, CompletedLessonExerciseForm
from openuniversity.apps.contents.stats.models import Account_Lesson_Question, \
    Account_Lesson, CompletedExercise

class Account_Lesson_QuestionAdmin(admin.ModelAdmin):
    form = Account_Topic_QuestionForm
admin.site.register(Account_Lesson_Question, Account_Lesson_QuestionAdmin)

class Account_LessonAdmin(admin.ModelAdmin):
    form = Account_LessonForm
admin.site.register(Account_Lesson, Account_LessonAdmin)

class CompletedExerciseAdmin(admin.ModelAdmin):
    form = CompletedLessonExerciseForm
admin.site.register(CompletedExercise, CompletedExerciseAdmin)