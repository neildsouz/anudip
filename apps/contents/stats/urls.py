from django.conf.urls.defaults import *
from openuniversity.apps.contents.stats import views

urlpatterns = patterns('',
    url(r'(?P<user_id>\d+)/(?P<account_lesson_id>\d+)/$', views.view_report, name='report_view'),
)