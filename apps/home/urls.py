from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.home.views',
     url(r'^$', 'index', name="index"),
     url(r'^about/$', 'about'),
)