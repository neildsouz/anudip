from django.db.models.query_utils import Q
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from openuniversity.apps.contents.contents_core import util
from openuniversity.apps.contents.practical.models import Question
from openuniversity.apps.contents.stats.models import Account_Lesson

def index(request):
    if not request.user.is_superuser:
        if request.user.is_authenticated(): #and not request.user.is_superuser:
            account = request.user.get_profile()
            als = Account_Lesson.objects.filter(
                                       account=request.user.get_profile()) # TODO
            suggested_topics = util.get_suggested_lessons(request.user)
            user_account_suggest_question = Question.objects.filter(
                                                    submitted_by=account)
            completed_lesson_al = Account_Lesson.objects.filter(
                Q(account__user__id=request.user.id)&Q(is_completed=True))
            return render_to_response("home/home_after_login.html", 
                                      locals(), 
                                  context_instance=RequestContext(request))
        print "user_is_not_authenticated"
    return render_to_response("home/home.html",
                          context_instance=RequestContext(request))

def about(request):
    return render_to_response("home/about.html", 
                              context_instance=RequestContext(request))


