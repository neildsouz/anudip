#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.badge.forms import BadgeForm
from openuniversity.apps.badge.models import Badge

class BadgeAdmin(admin.ModelAdmin):
    form = BadgeForm

admin.site.register(Badge, BadgeAdmin)
