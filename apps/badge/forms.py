#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms
from openuniversity.apps.badge.models import Badge
from openuniversity.apps.core.models import Language
from openuniversity.apps.translation.forms import TranslateAbleForm

class BadgeForm(TranslateAbleForm):
    name = forms.CharField(max_length=50)
    description = forms.CharField(widget=forms.TextInput)
    language = forms.ModelChoiceField(queryset=Language.objects.all())
    
    class Meta:
        model = Badge
        exclude = ("accounts")

    def save(self, **kwargs):
        return super(BadgeForm, self).save(12,commit=True)
    
    def save_m2m(self, **kwargs):
#        super(SubjectForm, self).save_m2m(**kwargs)
        pass