#coding: utf-8
from django.db import models
from openuniversity.apps.contents.practical.models import Exercise
from openuniversity.apps.core.models import Account
from openuniversity.apps.translation.models import TranslateAble

# Create your models here.
class Badge(TranslateAble):
    points_num = models.IntegerField(default=0)
    exercise = models.ForeignKey(Exercise, null=True, 
                                    related_name="content_badge")
    is_special = models.BooleanField(default=False)
    accounts = models.ManyToManyField(Account, related_name="badges")
    file = models.ImageField(verbose_name=u"Зураг",max_length=300, 
                             upload_to= "badges/" )
    
    def get_file_name(self):
        return self.file.file.name.split("/").pop()
    
    def get_trans_field_names(self):
        return ("name", "description")