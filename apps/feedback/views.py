from django.db.models.query_utils import Q
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template.context import RequestContext
from django.utils.translation import gettext
from django.contrib.auth.models import User
from openuniversity.apps.feedback.models import ContactForm
from openuniversity.apps.feedback.models import ContactFormAnon

def index(request): 
    user = request.user
    if request.method == 'POST':
    	if user.is_authenticated():
    	    form = ContactForm(request.POST)
            if form.is_valid():
                form.save()
                return render_to_response('feedback/feedbackthanks.html')
        else:
            form = ContactFormAnon(request.POST)
            if form.is_valid():
                form.save()
                return render_to_response('feedback/feedbackthanks.html')
    else:
        if user.is_authenticated():
            form = ContactForm(initial={'name':user.get_full_name(),
                                'email':user.email,})
        else:
            form = ContactFormAnon()
    return render_to_response('feedback/feedback.html', {'form': form})
