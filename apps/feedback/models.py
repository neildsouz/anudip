from django import forms 
from django.forms import ModelForm
from django.db import models
from django.contrib.auth.models import User
from django.template.context import RequestContext
from django.utils.translation import ugettext_lazy as _

class ContactAnon(models.Model):
    TOPIC_CHOICES = (
        (u'general', u'General enquiry'),
        (u'bug', u'Bug report'),
        (u'suggestion', u'Suggestion'),
    )

    topic = models.CharField(_('Topic'),max_length=10, choices=TOPIC_CHOICES, blank=False)
    name = models.CharField(_('Name'), max_length=50, blank=False)
    phone = models.CharField(_('Phone'), max_length=15, blank=True)
    email = models.EmailField(_('E-mail'), max_length=75, blank=True)
    message = models.TextField(_('Message'), max_length=1000, blank=False)

class Contact(models.Model):
    TOPIC_CHOICES = (
        (u'general', u'General enquiry'),
        (u'bug', u'Bug report'),
        (u'suggestion', u'Suggestion'),
    )
    topic = models.CharField(_('Topic'),max_length=10, choices=TOPIC_CHOICES, blank=False)
    name = models.CharField(_('Name'), max_length=50, blank=False)
    phone = models.CharField(_('Phone'), max_length=15, blank=True)
    email = models.EmailField(_('E-mail'), max_length=75, blank=True)
    message = models.TextField(_('Message'), max_length=1000, blank=False)

class ContactFormAnon(ModelForm):
    class Meta:
        model = ContactAnon

class ContactForm(ModelForm):
    class Meta:
	model = Contact
