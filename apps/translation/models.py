#coding: utf-8
# Create your models here.
from django.db import models
from django.db.models.query_utils import Q
from openuniversity.apps.core.models import Language

class TranslateAble(models.Model):
    
    def get_trans_field_names(self):
        """
        default translate able field name = name
        """
        return ("name", )

    def save(self, force_insert=False, force_update=False, using=None):
#        print "translateable.save()"
        models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using)
        #super(TranslateAble, self).save()
#        print self.id

    def saveTranslations(self, *args, **kwargs):
        for field_name in self.get_trans_field_names():
            t = Translation()
            t.object = self
            t.field_name = field_name
            t.text = kwargs[field_name]
            t.language = kwargs['language']
            t.save()
    
    def t9n(self, field_name=None, language_code="en-us", show_language=False,\
             allow_blank=False, current_language=False):
        """
            allow blank has a higher priority to show_language
        """
#        print field_name
        if field_name:
            from django.utils import translation
            if current_language:
                self.t9n(field_name=field_name, language_code=translation.get_language(), \
                     allow_blank=True, show_language=show_language, current_language=False)
            try:
                if show_language:
                    lng = "("+language_code+")"
                    
                    if language_code != translation.get_language():
                        return self.translations.get(Q(field_name=field_name)&
                        Q(language=Language.objects.get(code=language_code))).text+lng
                    else:
                        return self.translations.get(Q(field_name=field_name)&
                        Q(language=Language.objects.get(code=language_code))).text
                else:
                    return self.translations.get(Q(field_name=field_name)&
                Q(language=Language.objects.get(code=language_code))).text
                    
            except:
                if allow_blank:
                    return u""
                if show_language:
                    for l in Language.objects.all():
                        try:
                            return self.translations.get(Q(field_name=field_name)&Q(language=l)).text + \
                                "(" + l.code + ")"
                        except:
                            pass
            return u""
        else:
            return _("no translation") #TODO: add translation here 
    
    def __unicode__(self):
        return u"translateable: " + unicode(self.id)
    
    def get_available_translation_languages(self):
        """
            gets the list of languages of available translations
        """
        from django.db.models import Count
        available_translations = self.translations.values("language").annotate(Count("id")).order_by()
        return [ Language.objects.get(pk=at['language']) for at in available_translations ]
    
    def get_translations_by_language(self):
        """
            list of translations grouped by language, field_name, text
        """
        from django.db.models import Count
        available_translations = self.translations.values("language", "field_name", "text").annotate(Count("id"))
        return available_translations

class Translation(models.Model):
    language = models.ForeignKey(Language)
    field_name = models.CharField(max_length=30)
    text = models.CharField(max_length=12500)
    object = models.ForeignKey(TranslateAble, null=True, blank=True, related_name="translations")
    
    def __unicode__(self):
        return "(" + self.language.code + ") " + self.field_name + " -- " + self.text