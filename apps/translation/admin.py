'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.translation.forms import TranslationForm
from openuniversity.apps.translation.models import Translation

class TranslationAdmin(admin.ModelAdmin):
    list_display = ('object', '__unicode__')
    form = TranslationForm

admin.site.register(Translation, TranslationAdmin)
