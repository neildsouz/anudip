��          �      <      �  )   �     �     �  -   �          (     9  _   S  %   �  N   �  >   (     g  '   p     �     �     �     �  j  �  A   B  1   �  (   �  ^   �     >     L  7   g  �   �  @   u  �   �  K   P     �  N   �  *   	  #   6	  )   Z	     �	                                                                 	                   
                 A user with that username already exists. Activate users E-mail I have read and agree to the Terms of Service Password Password (again) Re-send activation emails Registration using free email addresses is prohibited. Please supply a different email address. The two password fields didn't match. This email address is already in use. Please supply a different email address. This value must contain only letters, numbers and underscores. Username You must agree to the terms to register activation key registration profile registration profiles user Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-08-06 12:17+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 Хэрэглэгчийн нэр бүртгэгдсэн байна Хэрэглэгчдийг идэвхжүүлэх Цахим шуудангийн хаяг Би үйлчилгээний нөхцөлийг уншсан ба зөвшөөрч байна Нууц үг Нууц үг давтах Идэвхжүүлэх майл дахин илгээх Үнэгүй цахим шуудангийн хаяг ашиглан бүртгүүлэхийг хориглосон байна та өөр цахим шуудангийн хаягаар бүртгүүлнэ үү. Нууц үг хоорондоо тохирохгүй байна Цахим шуудангийн хаяг бүртгэлтэй байна, Та өөр цахим шуудангийн хаяг бүртгүүлнэ үү. Зөвхөн үсэг, тоо, доогуур зураас ашиглана Хэрэглэгчийн нэр Та үйлчилгээний нөхцөлийг зөвшөөрөх ёстой идэвхжүүлэх түлхүүр үг бүртгэлийн профайл бүртгэлийн профайлууд хэрэглэгч 