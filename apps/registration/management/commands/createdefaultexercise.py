from django.core.management.base import BaseCommand, CommandError
from django.core.management.base import NoArgsCommand
from openuniversity.apps.contents.practical.models import Lesson
from openuniversity.apps.contents.practical.models import Exercise
from openuniversity.apps.contents.practical.utils import createUserSuggestedExercise

class Command(NoArgsCommand):
    help = 'Creates default student suggested exercise for each lesson if it doesnt exist'

    def handle_noargs(self, **options):
        lessons = Lesson.objects.all()
        for lesson in lessons:
            exercises = Exercise.objects.filter(lesson=lesson)
            found = False
            for exercise in exercises:
                if exercise.t9n(field_name="name") == "Student suggested practice":
                    found = True
                    self.stdout.write('Found default exercise for "%s", skipping\n' % lesson)
                    break
            if not found:
                lesson.create_default_exercise()
                self.stdout.write('Created default exercise for "%s"\n' % lesson)