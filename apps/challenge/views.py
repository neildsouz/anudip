# coding: utf-8
# Create your views here.
from django.template.context import RequestContext
from django.shortcuts import render_to_response

def home(request):
    context = RequestContext(request)
    return render_to_response("challenge/index.html", 
                              context_instance=context)