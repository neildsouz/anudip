from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.challenge.views',
    url(r'^$', 'home'),
    
    url(r'^home/$', 'home',name="challenge_home"),
)