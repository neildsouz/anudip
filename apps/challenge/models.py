from django.db import models
from openuniversity.apps.core.models import Account
from openuniversity.apps.translation.models import TranslateAble
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Challenge(TranslateAble):
    file = models.FileField(verbose_name=_("Related document"), 
                            upload_to="challenges/")
    created_date = models.DateTimeField(verbose_name=_("Created date"), 
                                        auto_now=True)
    challengers = models.ManyToManyField(Account,
                                         verbose_name=_("Team members"))
    
    def get_trans_field_names(self):
        return ("name", "description")
    
class Challenge_Members(TranslateAble):
    account = models.ForeignKey(Account)
    challenge = models.ForeignKey(Challenge)
#    role = models.CharField(max_length=50,null=True, blank=True) #TODO: add role table and foreign key
    
    def get_trans_field_names(self):
        return ("role")