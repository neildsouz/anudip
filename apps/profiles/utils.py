"""
Utility functions for retrieving and generating forms for the
site-specific user profile model specified in the
``AUTH_PROFILE_MODULE`` setting.

"""

from django import forms
from django.db import models
from django.conf import settings
from django.contrib.auth.models import SiteProfileNotAvailable
from django.db.models import get_model
from django.forms.widgets import DateInput, RadioSelect
from django.contrib.admin.widgets import AdminDateWidget
from django.forms import widgets
from openuniversity.apps.core.models import Account
from django.db.models.fields import DateField
from openuniversity.apps.contents.practical.models import Lesson
from openuniversity.apps.contents.stats.models import Account_Lesson
from openuniversity.apps.contents.contents_core.models import Topic, Subject


def get_profile_model():
    """
    Return the model class for the currently-active user profile
    model, as defined by the ``AUTH_PROFILE_MODULE`` setting. If that
    setting is missing, raise
    ``django.contrib.auth.models.SiteProfileNotAvailable``.
    
    """
    if (not hasattr(settings, 'AUTH_PROFILE_MODULE')) or \
           (not settings.AUTH_PROFILE_MODULE):
        raise SiteProfileNotAvailable
    profile_mod = get_model(*settings.AUTH_PROFILE_MODULE.split('.'))
    if profile_mod is None:
        raise SiteProfileNotAvailable
    return profile_mod

def make_custom_datefield(f):
    formfield = f.formfield()
    if isinstance(f, DateField):
        #formfield.widget.format = '%m/%d/%Y'
        formfield.widget.attrs.update({'class':'datePicker', 'readonly':'true'})
    return formfield


def get_profile_form():
    """
    Return a form class (a subclass of the default ``ModelForm``)
    suitable for creating/editing instances of the site-specific user
    profile model, as defined by the ``AUTH_PROFILE_MODULE``
    setting. If that setting is missing, raise
    ``django.contrib.auth.models.SiteProfileNotAvailable``.
    
    """
    profile_mod = get_profile_model()
    class _ProfileForm(forms.ModelForm):
        #formfield_callback = make_custom_datefield
        class Meta:
            model = profile_mod
            exclude = ('user','points', 'teacher') # User will be filled in by the view.
            #teacher = Account.get_teacher_name()
            #fields = ('teacher','mobile_number','location' ,'gender' , 'birthdate')
            #date = forms.DateTimeField(widget= widgets.AdminDateWidget()))
            widgets = {
                'gender': RadioSelect(),
                #date = forms.DateTimeField(widget=widgets.AdminDateWidget())
            }
            
    return _ProfileForm

def get_account_lessons(profile, age_group=0, subject_id=0):
    profile = profile.user
    # no filtering returning all lessons taken by user
    if(age_group==0 and subject_id==0):
        return Account_Lesson.objects.filter(account=profile)
    else:
        topic_query = Topic.objects
        if(subject_id>0):
            subject = Subject.objects.filter(id=subject_id)
            topic_query = topic_query.filter(subject=subject)
        if(age_group>0):
            topic_query = topic_query.filter(age_group=age_group)
        values = topic_query.values_list('pk', flat=True)
        print list(values)  
        # list causes two queries as per django documentation mysql doesnt optimize inner queries
        values = Lesson.objects.filter(topic__in=list(values)).values_list('pk', flat=True)
        print list(values)
        
        return Account_Lesson.objects.filter(account=profile).filter(lesson__in=list(values))