from django import forms
from django.utils.translation import ugettext_lazy as _

from openuniversity.apps.core.models import AgeGroup, Division
from django.contrib.auth.models import User
from openuniversity.apps.core.models import Account
from openuniversity.apps.teacher_student.student.models import Student_Division

# I put this on all required fields, because it's easier to pick up
# on them with CSS or JavaScript if they have a class of "required"
# in the HTML. Your mileage may vary. If/when Django ticket #3515
# lands in trunk, this will no longer be necessary.
attrs_dict = {'class': 'required'}
GENDER_CHOICES = (
    (True,_('Male')),
    (False,_('Female')),
)

ALL_AGE_GROUPS = AgeGroup.objects.all()
ALL_DIVISIONS = Division.objects.all()

class DivisionModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name

class ProfileForm(forms.Form):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                             label=_("E-mail"))
    gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=forms.RadioSelect())
    birthdate = forms.DateField(widget=forms.DateInput(attrs=attrs_dict),label=_("Birth date"))
    location = forms.CharField(max_length=100)
    
    def save(self,profile_obj):
        data = self.cleaned_data
        user = User.objects.get(id=profile_obj.user.id)
        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.email = data['email']
        user.save()
        account = Account.objects.get(user=user)
        account.gender = data['gender']
        account.birthdate = data['birthdate']
        account.location = data['location']
        account.save()
        
        
class StudentProfileForm(ProfileForm):
        age = forms.ModelChoiceField(queryset=ALL_AGE_GROUPS, empty_label=_("Choose your grade"), required=False)
        division = DivisionModelChoiceField(queryset=ALL_DIVISIONS, empty_label=_("Choose grade first"), required=False)
        
        def save(self,profile_obj):
            data = self.cleaned_data
            super(StudentProfileForm,self).save(profile_obj)
            try:
                student = profile_obj.student_division
                student.division = data['division']
            except Student_Division.DoesNotExist:
                student = Student_Division.objects.create(student=profile_obj, division = data['division'])
            student.save()