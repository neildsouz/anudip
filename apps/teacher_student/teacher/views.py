from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

from openuniversity.apps.teacher_student.teacher.models import Topic_Teacher
from openuniversity.apps.core.models import Account, Division
from openuniversity.apps.contents.contents_core.models import Topic

import json

@login_required
def add_course(request, **kwargs):
    try:
        #profile_obj = Account.objects.get(id = request.user.id )
        profile_obj = request.user.get_profile()
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('profiles_create_profile'))
    
    if request.method == 'POST' and request.is_ajax():
        post_params = request.POST

        division = Division.objects.get(id=post_params['division'])
        topic = Topic.objects.get(id=post_params['topic'])
        try:
            topic_teacher = Topic_Teacher.objects.get(teacher=profile_obj, division=division, topic=topic)
            returnjson = json.dumps({'exception':"Entry already exists"})
        except Topic_Teacher.DoesNotExist:
            topic_teacher = Topic_Teacher(teacher=profile_obj, division=division, topic=topic)
            topic_teacher.save()
            returnjson = json.dumps({'id':topic_teacher.id, 'division': division.name, 'topic':str(topic)})
        return HttpResponse(returnjson,mimetype='application/json')
    
@login_required
def remove_course(request, **kwargs):
    try:
        #profile_obj = Account.objects.get(id = request.user.id )
        profile_obj = request.user.get_profile()
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('profiles_create_profile'))
    
    if request.method == 'POST' and request.is_ajax():
        post_params = request.POST
        
        try:
            tt_id = int(post_params['id'])
            topic_teacher = Topic_Teacher.objects.get(id=tt_id)
            topic_teacher.delete()
            returnjson = json.dumps({'success':"Entry deleted"})
        except Topic_Teacher.DoesNotExist:
            returnjson = json.dumps({'exception':"Entry not found"})
        return HttpResponse(returnjson,mimetype='application/json')