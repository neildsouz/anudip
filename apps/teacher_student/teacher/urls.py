from django.conf.urls.defaults import *
from openuniversity.apps.teacher_student.teacher import views

urlpatterns = patterns('',
    url(r'^add_course/$', views.add_course, name='teacher_add_course'),
    url(r'^remove_course/$', views.remove_course, name='teacher_remove_course'),
)
