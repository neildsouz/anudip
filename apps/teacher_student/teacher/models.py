from django.db import models
from openuniversity.apps.core.models import Account, Division
from openuniversity.apps.contents.contents_core.models import Topic

# Create your models here.
class Topic_Teacher(models.Model):
    teacher = models.ForeignKey(Account)
    division = models.ForeignKey(Division)
    topic = models.ForeignKey(Topic)
    
    def __unicode__(self):
        return str(self.teacher) +" "+ str(self.topic) +" "+str(self.division)