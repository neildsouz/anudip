from django.db import models
from openuniversity.apps.core.models import Account
from openuniversity.apps.translation.models import TranslateAble

# Create your models here.
class TeacherStudentContent(models.Model):
    teacher = models.ForeignKey(Account, related_name="content_students")
    student = models.ForeignKey(Account, related_name="content_teachers")
    content = models.ForeignKey(TranslateAble) # Courses

class TeacherStudent(models.Model):
    teacher = models.ForeignKey(Account, related_name="students")
    student = models.ForeignKey(Account, related_name="teachers")
    is_active = models.BooleanField()

class TeacherStudentHistory(models.Model):
    row = models.ForeignKey(TeacherStudent)
    started_date = models.DateTimeField()
    finished_date = models.DateTimeField()