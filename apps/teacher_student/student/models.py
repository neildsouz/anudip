from django.db import models
from openuniversity.apps.core.models import Account, Division

# Create your models here.
class Student_Division(models.Model):
    student = models.OneToOneField(Account)
    division = models.ForeignKey(Division)
    
    def __unicode__(self):
        return self.student.user.username +" "+ self.division.age.age +" "+self.division.name