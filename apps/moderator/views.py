# coding: utf-8
# Create your views here.
from datetime import datetime, timedelta
from django import forms
from django.contrib.auth.decorators import user_passes_test, permission_required
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import translation
from openuniversity.apps.common.forms import SuggesttedQuestionForm
from openuniversity.apps.common.models import SubType, Difficulty
from openuniversity.apps.contents.contents_core.forms import SubjectForm, \
    TopicForm, ContentAliasHiddenForm
from openuniversity.apps.contents.contents_core.models import Subject, \
    ContentAlias, Topic
from openuniversity.apps.contents.files.models import Video
from openuniversity.apps.contents.practical.forms import LessonForm, \
    ExerciseForm, QuestionForm
from openuniversity.apps.contents.practical.models import Lesson, Exercise, \
    Question, Exercise_Question
from openuniversity.apps.moderator.util import \
    render_to_response_with_breadcramb, get_avail_trans_from_content_object
from openuniversity.apps.translation.models import Translation


def is_moderator(user):
    return user.get_profile().is_moderator()

@user_passes_test(is_moderator)
def home(request):
    return render_to_response_with_breadcramb("moderators/index.html", 
                              context_instance=RequestContext(request), 
                              ou_mod_breadcramb={"level":0})

@user_passes_test(is_moderator)
def subject_list(request):
    print ":subject_list:"
    if request.POST.has_key('filterSubjectName'):
        name = request.POST['filterSubjectName']
        
        # subject -iin id iig avna
        subject_id = Subject.objects.filter().values_list('id')
        
        # translation - aas subjectuudiig haina
        trans = Translation.objects.filter(object__in = subject_id)
        
        # translation - aas filter hiij bna
        transid = trans.filter(text__icontains = name).values_list('object')
        subjects = Subject.objects.filter(id__in = transid)
        
    else:
        subjects = Subject.objects.all()
    
    return render_to_response_with_breadcramb("moderators/subject/list.html", 
                          locals(),
                          context_instance=RequestContext(request),
                          ou_mod_breadcramb={"level":1})

@user_passes_test(is_moderator)
def subject_add(request, s_str=None):
    from django.db import models
    
    form = SubjectForm
    if s_str:
        submit = "edit subject"
        try:
            subject = ContentAlias.objects.get(alias=s_str).content_object
        except:
            subject = Subject.objects.get(pk=int(s_str))
    else:
        submit = "add subject"
        subject = Subject()
        
    if request.POST:
        form = SubjectForm(request.POST, instance=subject)
        form.alias_nested = SubjectForm.alias_nested(request.POST, instance=subject)
        
        if form.is_valid() and form.alias_nested.is_valid():
            subject = form.save()
            form.alias_nested.save()
            return HttpResponseRedirect(redirect_to=reverse(subject_view, 
                                            args=[s_str or subject.alias_or_id()]))
#        else:
#            print "subject_add form.erros:"
#            print form.errors
#            pass

    elif s_str != None:
        form = SubjectForm(instance=subject)
        form.alias_nested = SubjectForm.alias_nested_hidden(instance=subject)

    return render_to_response_with_breadcramb("moderators/subject/add.html", 
          locals(), 
          context_instance=RequestContext(request),
          ou_mod_breadcramb={"level":1, "function":submit})

@user_passes_test(is_moderator)
def subject_view(request, s_str=None):
    print ":subject_view:"
    
    # filter-iin agegroup gargahad ashiglaj bna
    formTopic = TopicForm
    
    # filter hiisen vv shalgaj bna
    if request.POST.has_key('filterName') or request.POST.has_key('filterDescription') or request.POST.has_key('age_group'):
        print ":FILTER TOPIC:"
        
        s_int = None
        try:
            s_int = int(s_str)
            c = Subject.objects.get(pk=s_int)
        except:
            alias = ContentAlias.objects.get(alias=s_str)
            c = alias.content_object
        if s_str:
            topics = c.topic_set.all()
        else:
            topics = Topic.objects.all()
            
        if request.POST.has_key('filterDescription'):
            if request.POST['filterDescription']:
                fdescription = request.POST['filterDescription']
                
                # topic -iin id iig avna
                topic_id = Topic.objects.filter().values_list('id')
                
                # translation - aas topicuudiig haina
                trans = Translation.objects.filter(object__in = topic_id)
                
                # translation - aas filter hiij bna
                transid = trans.filter(Q(text__icontains = fdescription) & Q(field_name='description')).values_list('object')
                topics = topics.filter(id__in = transid)
        
        if request.POST.has_key('filterName'):
            
            if request.POST['filterName']:
                name = request.POST['filterName']
                print "name: ",name
                # topic -iin id iig avna
                topic_id = Topic.objects.filter().values_list('id')
                
                # translation - aas topicuudiig haina
                trans = Translation.objects.filter(object__in = topic_id)
                
                # translation - aas filter hiij bna
                transid = trans.filter(Q(text__icontains = name) & Q(field_name='name')).values_list('object')
                topics = topics.filter(id__in = transid)

        if request.POST.has_key('age_group'):
            if request.POST['age_group']:
                fageGroup = request.POST['age_group']
                topics = topics.filter(age_group = fageGroup)
                formTopic = TopicForm(request.POST)
                
    else:
        s_int = None
        try:
            s_int = int(s_str)
            c = Subject.objects.get(pk=s_int)
        except:
            alias = ContentAlias.objects.get(alias=s_str)
            c = alias.content_object
        if s_str:
            topics = c.topic_set.all()
        else:
            topics = Topic.objects.all()
    return render_to_response_with_breadcramb("moderators/subject/view.html", 
          locals(),
          ou_mod_breadcramb={"level":1,"pobjects":[s_str,]},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def subject_translate(request, s_str):
    form = SubjectForm
    submit = "translate"

    try:
        subject = ContentAlias.objects.get(alias=s_str).content_object    
    except:
        subject = Subject.objects.get(pk=int(s_str))
    available_translations = get_avail_trans_from_content_object(subject, 
                                            translation.get_language())
    
    if request.POST:
        form = SubjectForm(request.POST, instance=subject)
        
        if form.is_valid():
            subject = form.save()
            return HttpResponseRedirect(redirect_to=reverse(subject_list))
#        else:
#            print "subject_translate form.errors:"
#            print form.errors
#            pass
    else:
        form = SubjectForm(instance=subject)
    form.make_translatable()

    return render_to_response_with_breadcramb("moderators/translate.html", 
          locals(), 
          ou_mod_breadcramb={"level":1, "function":submit},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def subject_remove(request, s_str):
    try:
        ContentAlias.objects.get(alias=s_str).content_object.delete()
    except: 
        Subject.objects.get(pk=int(s_str)).delete()
    return HttpResponseRedirect(redirect_to=reverse(subject_list))

@user_passes_test(is_moderator)
def topic_list(request, s_str=None):
    print ":topic_list:"
    try:
        c = ContentAlias.objects.get(alias=s_str).content_object
    except:
        c = Subject.objects.get(pk=int(s_str))
        
    if s_str:
        topics = c.topic_set.all()
    else:
        topics = Topic.objects.all()
    return render_to_response_with_breadcramb("moderators/topic/list.html", 
          locals(), 
          ou_mod_breadcramb={"level":2,"pobjects":[s_str,]},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def topic_add(request, s_str, t_str=None):
    print ":topic_add, edit:"
    from django.db import models
    
    form = TopicForm
    if t_str:
        submit = "edit topic"
        try:
            topic = Topic.objects.get(pk=int(t_str))
        except:
            topic = ContentAlias.objects.get(alias=t_str).content_object
    else:
        submit = "add topic"
        topic = Topic()

    if request.POST:
        topic.suggest_status = 0
        if not hasattr(topic, "subject"):
            try:
                topic.subject = ContentAlias.objects.get(alias=s_str).content_object
            except:
                topic.subject = Subject.objects.get(pk=int(s_str))
        
        form = TopicForm(request.POST, instance=topic)
        form.alias_nested = TopicForm.alias_nested(request.POST, instance=topic)
        
        if form.is_valid() and form.alias_nested.is_valid():
            topic = form.save()
            form.alias_nested.save()
            return HttpResponseRedirect(redirect_to=reverse(topic_view, 
                                                args=[s_str, topic.alias()]))
#        else:
#            print "topic_add form errors:"
#            print form.errors
#            pass
    elif t_str != None:
        form = TopicForm(instance=topic)
        form.alias_nested = TopicForm.alias_nested_hidden(instance=topic)

    return render_to_response_with_breadcramb("moderators/topic/add.html", 
          locals(),
          ou_mod_breadcramb={"level":2,"pobjects":[s_str, t_str],"function":submit},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def topic_view(request, s_str, t_str):
    print ":topic_view:"
    t_int = None
    
    if request.POST.has_key('filterName') or request.POST.has_key('filterDescription'):
        print ":filter lesson:"
        try:
            t_int = int(t_str)
            c = Topic.objects.get(pk=t_int)
        except:
            c = ContentAlias.objects.get(alias=t_str).content_object
        lessons = c.lessons.all()
        if request.POST.has_key('filterName'):
            if request.POST['filterName']:
                name = request.POST['filterName']
                
                # topic -iin id iig avna
                lessons_id = Lesson.objects.filter().values_list('id')
                
                # translation - aas topicuudiig haina
                trans = Translation.objects.filter(object__in = lessons_id)
                
                # translation - aas filter hiij bna
                transid = trans.filter(Q(text__icontains = name) & Q(field_name='name')).values_list('object')
                lessons = Lesson.objects.filter(id__in = transid)
                
        if request.POST.has_key('filterDescription'):
            if request.POST['filterDescription']:
                fdescription = request.POST['filterDescription']
                
                # topic -iin id iig avna
                lessons_id = Lesson.objects.filter().values_list('id')
                
                # translation - aas topicuudiig haina
                trans = Translation.objects.filter(object__in = lessons_id)
                
                # translation - aas filter hiij bna
                transid = trans.filter(Q(text__icontains = fdescription) & Q(field_name='description')).values_list('object')
                lessons = Lesson.objects.filter(id__in = transid)
        
    else:
        
        try:
            t_int = int(t_str)
            c = Topic.objects.get(pk=t_int)
        except:
            c = ContentAlias.objects.get(alias=t_str).content_object
        lessons = c.lessons.all()
    
    return render_to_response_with_breadcramb("moderators/topic/view.html", 
          locals(),
          ou_mod_breadcramb={"level":2,"pobjects":[s_str,t_str]},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def topic_translate(request, s_str, t_str):
    
    form = TopicForm
    submit = "translate"

    # used to show available translations
    try:
        topic = ContentAlias.objects.get(alias=t_str).content_object    
    except:
        topic = Topic.objects.get(pk=int(t_str))
    available_translations = get_avail_trans_from_content_object(topic, 
                                             translation.get_language())
    
    if request.POST:
        form = TopicForm(request.POST, instance=topic)
        
        if form.is_valid():
            topic = form.save()
            return HttpResponseRedirect(reverse(subject_view, args=[s_str]))
#        else:
#            print "topic_translate form.errors:"
#            print form.errors
#            pass
    else:
        form = TopicForm(instance=topic)
    form.make_translatable()

    return render_to_response_with_breadcramb("moderators/translate.html", 
          locals(),
          ou_mod_breadcramb={"level":2,"pobjects":[s_str,t_str],'function':submit},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def topic_remove(request, s_str, t_str):
    t_id = None
    try:
        t_id = int(t_str)
    except:
        pass
    if t_id:                                 # TO
        Topic.objects.get(pk=t_id).delete()  # BE 
    else:                                    # REMOVED
        ContentAlias.objects.get(alias=t_str).content_object.delete()
    return HttpResponseRedirect(redirect_to=reverse(subject_view, args=[s_str]))

@user_passes_test(is_moderator)
def lesson_list(request, s_str, t_str):
    print ":lesson_list:"

    try:
        c = ContentAlias.objects.get(alias=t_str).content_object
    except:
        c = Topic.objects.get(pk=int(t_str))
    lessons = c.lessons.all()
    return render_to_response_with_breadcramb("moderators/lesson/list.html", 
          locals(),
          ou_mod_breadcramb={"level":3,"pobjects":[s_str,t_str]},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def lesson_add(request, s_str, t_str, l_str=None):
    
    form = LessonForm
    submit = "edit lesson"
    if l_str:
        try:
            lesson = ContentAlias.objects.get(alias=l_str).content_object
        except:
            lesson = Lesson.objects.get(pk=int(l_str))
    else:
        submit = "add lesson"
        lesson = Lesson()

    if request.POST:
        try:
            lesson.topic = Topic.objects.get(pk=int(t_str))
        except:
            lesson.topic = ContentAlias.objects.get(alias=t_str).content_object
        form = LessonForm(request.POST,request.FILES, instance=lesson)
        form.alias_nested = LessonForm.alias_nested(request.POST, instance=lesson)
        form.pdf_nested = LessonForm.pdf_nested(request.POST, request.FILES, instance=lesson)
        form.image_nested = LessonForm.image_nested(request.POST, request.FILES, instance=lesson)
        form.video_nested = LessonForm.video_nested(request.POST, request.FILES, instance=lesson)

        if form.is_valid() and form.alias_nested.is_valid() and\
        form.image_nested.is_valid() and form.video_nested.is_valid() and\
        form.pdf_nested.is_valid():
            lesson_instance = form.save()
            form.alias_nested.save()
            form.pdf_nested.save()
            form.image_nested.save()
            lesson_instance.create_thumbnails()
            form.video_nested.save()

            return HttpResponseRedirect(redirect_to=reverse(lesson_view, 
                            args=[s_str, t_str, lesson_instance.alias()]))
            
        else:
            print "lesson_add form.errors: "
            print form.errors
            print form.alias_nested.errors
            print form.pdf_nested.errors
            print form.image_nested.errors
            print form.video_nested.errors
            pass
        
    elif l_str != None:
        form = LessonForm(instance=lesson)
        form.pdf_nested = LessonForm.pdf_nested(instance=lesson)
        form.image_nested = LessonForm.image_nested(instance=lesson)
        form.alias_nested = LessonForm.alias_nested_hidden(instance=lesson)
        form.video_nested = LessonForm.video_nested(instance=lesson)

    return render_to_response_with_breadcramb("moderators/lesson/add.html", 
          locals(),
          ou_mod_breadcramb={"level":3,"pobjects":[s_str, t_str, l_str],
                             "function":submit}, 
                              context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def lesson_set_thumbnail(request, s_str, t_str, l_str):
    submit = "set thumbnail"
    try:
        lesson = ContentAlias.objects.get(alias=l_str).content_object
    except:
        lesson = Lesson.objects.get(pk=int(l_str))
    if request.POST:
        print request.POST
        for key in request.POST.keys():
            v = Video.objects.get(pk=int(key))
            v.thumbnail_path = request.POST[key]
            v.save()
        return HttpResponseRedirect(redirect_to=reverse(lesson_view, 
                                        args=[s_str, t_str, lesson.alias()]))
    else:
        id_vals = [{"id":v.id}for v in lesson.video_set.all()]
        
        for video in lesson.video_set.all():
            if video.is_localized and not video.has_thumbnails:
                video.create_video_thumbnails()
                video.has_thumbnails = True
                video.save()
    
    return render_to_response_with_breadcramb("moderators/lesson/set_thumbnail.html", 
          locals(),
          ou_mod_breadcramb={"level":3,"pobjects":[s_str, t_str, l_str],
                             "function":submit}, 
                              context_instance=RequestContext(request))
    
@user_passes_test(is_moderator)
def lesson_view(request, s_str, t_str, l_str):
    from openuniversity.apps.core.models import Points
    
    formExer = ExerciseForm
    l_int = None
    
    if request.POST.has_key('created_date') or request.POST.has_key('bonus_points'):
        try:
            l_int = int(l_str)
            c = Lesson.objects.get(id=l_int)
        except:
            c = ContentAlias.objects.get(alias=l_str).content_object
        exercises = c.exercises.all()
        
        if request.POST.has_key('created_date') and request.POST.has_key('end_date'):
            if request.POST['created_date'] and request.POST.has_key('end_date'):
                createddate = datetime.strptime(str(request.POST['created_date']),"%Y-%m-%d")
                print createddate
                enddate = datetime.strptime(str(request.POST['end_date']),"%Y-%m-%d")
                print enddate
                exercises = exercises.filter(created_date__range = (createddate, enddate))
                
        if request.POST.has_key('bonus_points'):
            if request.POST['bonus_points']:
                bonus = request.POST['bonus_points']
                try:
                    point = Points.objects.get(points = int(bonus))
                    exercises = exercises.filter(bonus_points = point)
                except:
                    pass
                formExer = ExerciseForm(request.POST)
    else:
        try:
            l_int = int(l_str)
            c = Lesson.objects.get(id=l_int)
        except:
            c = ContentAlias.objects.get(alias=l_str).content_object
        exercises = c.exercises.all()

    return render_to_response_with_breadcramb("moderators/lesson/view.html", 
          locals(),
          ou_mod_breadcramb={"level":3,"pobjects":[s_str, t_str, l_str]},
                            context_instance=RequestContext(request))
    
@user_passes_test(is_moderator)
def lesson_remove(request, s_str, t_str, l_str):
    l_id = None
    try:
        l_id=int(l_str)
        Lesson.objects.get(pk=l_id).delete()
    except:
        pass
    if not l_id:
        ContentAlias.objects.get(alias=l_str).delete()
    return HttpResponseRedirect(redirect_to=reverse(topic_view, args=[s_str, t_str]))

@user_passes_test(is_moderator)
def lesson_translate(request, s_str, t_str, l_str):
    form = LessonForm
    submit = "translate"
    
    try:
        lesson = ContentAlias.objects.get(alias=l_str).content_object
    except:
        lesson = Lesson.objects.get(pk=int(l_str))
    available_translations = get_avail_trans_from_content_object(lesson, 
                                             translation.get_language())
    
    if request.POST:
        form = LessonForm(request.POST, instance=lesson)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(redirect_to=reverse(topic_view, args=[s_str, t_str]))
#        else:
#            print "lesson_translate form.errors: "
#            print form.errors
#            pass
    else:
        form = LessonForm(instance=lesson)
    form.make_translatable()

    return render_to_response_with_breadcramb("moderators/translate.html", 
          locals(),
          ou_mod_breadcramb={"level":3,"pobjects":[s_str, t_str, l_str],
                             'function':submit},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_list(request, s_str, t_str, l_str):
    print ":exercise_list:"
    try:
        lesson = ContentAlias.objects.get(alias=l_str).content_object
    except:
        lesson = Lesson.objects.get(pk=int(l_str))
    exercises = lesson.exercises.all()
    return render_to_response_with_breadcramb("moderators/exercise/list.html", 
          locals(),
          ou_mod_breadcramb={"level":4,"pobjects":[s_str, t_str, l_str]},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_add(request, s_str, t_str, l_str, e_str=None):
    form = ExerciseForm
    
    if e_str != None:
        submit = "edit exercise"
        try:
            exercise = ContentAlias.objects.get(alias=e_str).content_object
        except:
            exercise = Exercise.objects.get(pk=int(e_str))
    else:
        submit = "add exercise"
        exercise = Exercise()

    if request.POST:
        try:
            exercise.lesson = Lesson.objects.get(pk=int(l_str))
        except:
            exercise.lesson = ContentAlias.objects.get(alias=l_str).content_object
        form = ExerciseForm(request.POST, instance=exercise)
        form.alias_nested = ExerciseForm.alias_nested(request.POST, instance=exercise)
        if form.is_valid() and form.alias_nested.is_valid():
            exercise = form.save()
            form.alias_nested.save()
            try:
                e_str = ContentAlias.objects.get(object_id=exercise.id).alias
            except:
                e_str = exercise.id
            form.formset = ExerciseForm.formset(request.POST, instance=exercise, question_submitted_by=request.user.get_profile())
#            form.video_formset = ExerciseForm.video_formset(request.POST, request.FILES, instance=exercise)

            if form.formset.is_valid():
                form.formset.save()
                return HttpResponseRedirect(redirect_to=reverse(exercise_view, args=[s_str, t_str, l_str, e_str]))
#            else:
#                print "exercise_add form.formset.errors: "
#                print form.formset.errors
#                pass
#                
#        else:
#            print "exercise_add form.errors"
#            print form.errors
#            pass
        
    elif e_str != None:
        form = ExerciseForm(instance=exercise)
        
        form.alias_nested = ExerciseForm.alias_nested_hidden(instance=exercise)
        
        form.formset = ExerciseForm.formset(instance=exercise)
        form.formset.extra = 1
        form.formset._construct_forms()
        
        form.video_formset = ExerciseForm.video_formset(instance=exercise)
        form.video_formset.extra = 0
        form.video_formset._construct_forms()

    return render_to_response_with_breadcramb("moderators/exercise/add.html", 
          locals(),
          ou_mod_breadcramb={"level":4,"pobjects":[s_str, t_str, l_str, e_str],
                             "function":submit}, 
                              context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_view(request, s_str, t_str, l_str, e_str=None):
    from openuniversity.apps.core.models import Points
    print ":exercise_view:"
    e_int = None
    formQuestion = QuestionForm
    if request.POST.has_key('answer_type') or request.POST.has_key('points'):
        try:
            e_int = int(e_str)
            c = Exercise.objects.get(pk=e_int)
        except:
            c = ContentAlias.objects.get(alias=e_str).content_object
        questions = c.questions.all()
        
        if request.POST.has_key('answer_type'):
            if request.POST['answer_type']:
                at = request.POST['answer_type']
                subt = SubType.objects.get(id = at)
                questions = questions.filter(answer_type=subt)
                formQuestion = QuestionForm(request.POST)
        if request.POST.has_key('points'):
            if request.POST['points']:
                points = request.POST['points']
                try:
                    p = Points.objects.get(id = points)
                    questions = questions.filter(points=p)
                    formQuestion = QuestionForm(request.POST)
                except:
                    pass
        if request.POST.has_key('filterpublished'):
            if request.POST['filterpublished']:
                questions = questions.filter(is_published='1')
                
        if request.POST.has_key('created_date') and request.POST.has_key('end_date'):
            if request.POST['created_date'] and request.POST.has_key('end_date'):
                createddate = datetime.strptime(str(request.POST['created_date']),"%Y-%m-%d")
                print createddate
                enddate = datetime.strptime(str(request.POST['end_date']),"%Y-%m-%d")
                print enddate
                questions = questions.filter(created_date__range = (createddate, enddate))
    else:
        try:
            e_int = int(e_str)
            c = Exercise.objects.get(pk=e_int)
        except:
            c = ContentAlias.objects.get(alias=e_str).content_object
        questions = c.questions.all()
    return render_to_response_with_breadcramb("moderators/exercise/view.html", 
          locals(),
          ou_mod_breadcramb={"level":4,
                             "pobjects":[s_str, t_str, l_str, e_str]},
                              context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_translate(request, s_str, t_str, l_str, e_str):
    from django.db import models
    
    form = ExerciseForm
    submit = "translate"

    try:
        exercise = ContentAlias.objects.get(alias=e_str).content_object
    except:
        exercise = Exercise.objects.get(pk=int(e_str))
    available_translations = get_avail_trans_from_content_object(exercise, 
                                                 translation.get_language())
    
    if request.POST:
        form = ExerciseForm(request.POST, instance=exercise)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(redirect_to=reverse(lesson_view, 
                                            args=[s_str, t_str, l_str]))
#        else:
#            pass
    else:
#        exercise = Exercise.objects.get(pk=e_id)
        form = ExerciseForm(instance=exercise)
    form.make_translatable()

    return render_to_response_with_breadcramb("moderators/translate.html", 
          locals(),
          ou_mod_breadcramb={"level":4,
                             "pobjects":[s_str,t_str, l_str, e_str],
                             'function':submit},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_remove(request, s_str, t_str, l_str, e_str):
    e_id = None
    try:
        e_id = int(e_str)
    except:
        pass
    if not e_id:
        ContentAlias.objects.get(alias=e_str).content_object.delete()
    else:
        Exercise.objects.get(pk=e_id).delete()
    return HttpResponseRedirect(redirect_to=reverse(lesson_view, 
                                    args=[s_str, t_str, l_str]))

@user_passes_test(is_moderator)
def exercise_question_list(request, s_str, t_str, l_str, e_str):
    try:
        exercise = ContentAlias.objects.get(alias=e_str).content_object    
    except:
        exercise = Exercise.objects.get(pk=int(e_str))
    questions = exercise.questions.all()
    return render_to_response_with_breadcramb(
          "moderators/exercise-question/list.html", 
          locals(),
          ou_mod_breadcramb={"level":5,"pobjects":[s_str, t_str, l_str, e_str]},
          context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_question_view(request, s_str, t_str, l_str, e_str, q_str):
    q_id = None
    try:
        q_id = int(q_str)
    except:
        pass
    if q_id:
        c = Question.objects.get(id=q_id)
    else:
        c = ContentAlias.objects.get(alias=q_str).content_object
    return render_to_response_with_breadcramb("moderators/exercise-question/view.html", 
          locals(),
          ou_mod_breadcramb={"level":5,"pobjects":[s_str, t_str, l_str, e_str, q_str]},
                              context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_question_translate(request, s_str, t_str, l_str, e_str, q_id): 
    form = QuestionForm
    submit = "translate"

    question = Question.objects.get(pk=q_id)
    available_translations = get_avail_trans_from_content_object(question, translation.get_language())
    
    if request.POST:
        form = QuestionForm(request.POST, instance=question)
        
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(redirect_to=reverse(exercise_view, 
                                    args=[s_str, t_str, l_str, e_str]))
    else:
        form = QuestionForm(instance=question)
    form.make_translatable()
    
    return render_to_response_with_breadcramb("moderators/translate.html", 
          locals(), 
          ou_mod_breadcramb={"level":5,
                             "pobjects":[s_str, t_str, l_str, e_str, q_id],
                             "function": submit},
                              context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def exercise_question_add(request, s_str, t_str, l_str, e_str, q_id=None):
    text_answer_id = SubType.objects.get(type_int=1).id
    single_choice_answer_id = SubType.objects.get(type_int=2).id
    multi_choice_answer_id = SubType.objects.get(type_int=3).id
    
    form = QuestionForm
    if q_id:
        submit = "edit question"
        question = Question.objects.get(pk=q_id)
    else:
        submit = "add question"
        question = Question()
    
    if request.POST:
        form = QuestionForm(request.POST, instance=question)
        if form.is_valid():
            question = form.save()
            form.formset = QuestionForm.formset(request.POST,
                                request.FILES, 
                                instance=question)
            form.video_nested = QuestionForm.video_nested(request.POST, 
                                request.FILES, 
                                instance=question)
            if form.formset.is_valid() and form.video_nested.is_valid():
                # deletes additional m2m rows, there must always be one::start
                try:
                    m2m_exercise = ContentAlias.objects.get(alias=e_str).\
                    content_object
                except:
                    m2m_exercise=Exercise.objects.get(pk=int(e_str))
                count = Exercise_Question.objects.filter(
                           Q(m2m_question=question)&
                           Q(m2m_exercise=m2m_exercise)).count()
                if count > 1:
                    for e_q_item in Exercise_Question.objects.filter(
                         Q(m2m_question=question)&
                         Q(m2m_exercise=m2m_exercise)).all()[1:]:
                        e_q_item.delete()
                # deletes additional m2m rows, there must always be one::end
                    # if there is a exercise_question data
                    e_q = Exercise_Question.objects.get(Q(m2m_question=question)&
                      Q(m2m_exercise=m2m_exercise))
                    e_q.difficulty =\
                      Difficulty.objects.get(pk=int(request.POST['difficulty']))
                    e_q.save()
                elif count == 0: # if there are now exercise_question data
                    Exercise_Question(
                      m2m_question=question,
                      m2m_exercise=m2m_exercise,
                      difficulty =\
                      Difficulty.objects.get(pk=\
                                    int(request.POST['difficulty']))
                    ).save()
                    
                form.formset.save()
                form.video_nested.save()
                return HttpResponseRedirect(redirect_to=
                                        reverse(exercise_view, 
                                        args=[s_str, t_str, l_str, e_str]))
    elif bool(q_id):
        form = QuestionForm(instance=question)
        # deletes additional m2m rows, there must always be one::start
        try:
            m2m_exercise = ContentAlias.objects.get(alias=e_str).\
            content_object
        except:
            m2m_exercise=Exercise.objects.get(pk=int(e_str))
        count = Exercise_Question.objects.filter(Q(m2m_question=question)&
                    Q(m2m_exercise=m2m_exercise)).count()
        if count > 1:
            for e_q_item in Exercise_Question.objects.filter(
                 Q(m2m_question=question)&
                 Q(m2m_exercise=m2m_exercise)).all()[1:]:
                e_q_item.delete()
        # deletes additional m2m rows, there must always be one::end
        try:
            exercise_question = Exercise_Question.objects.get(
              Q(m2m_question=question)&
              Q(m2m_exercise=ContentAlias.objects.get(alias=e_str)
              .content_object))
        except:
            exercise_question = Exercise_Question.objects.get(
              Q(m2m_question=question)&
              Q(m2m_exercise=Exercise.objects.get(pk=int(e_str)))
            )
        
        form.fields['difficulty']._set_choices(Difficulty.objects.get_choice_tuple())
        form.fields['difficulty'].initial = \
        exercise_question.difficulty
        form.formset = QuestionForm.formset(instance=question)
        form.video_nested = QuestionForm.video_nested(instance=question)
        # hard coded for text answers
        if question.answer_type.type_int == 1 and question.answer_set.count(): 
            form.formset.max_num = 1
            form.formset._construct_forms()
    return render_to_response_with_breadcramb(
                  "moderators/exercise-question/add.html", 
                  locals(), 
                  ou_mod_breadcramb={"level":5,
                                     "pobjects":[s_str, t_str, l_str, e_str],
                                     "function":submit}, 
                                  context_instance=RequestContext(request))
    
@user_passes_test(is_moderator)
def exercise_question_remove(request, s_str, t_str, l_str, e_str, q_id):
    Question.objects.get(pk=q_id).delete()
    return HttpResponseRedirect(redirect_to=reverse(exercise_view, 
                                        args=[s_str, t_str, l_str, e_str]))

##############################################################################################################
@user_passes_test(is_moderator)
def suggested_question_list(request):
    
    
    #questions_submitted = Question.objects.all()
    questions_submitted = Question.objects.filter(is_published='0')
    
    return render_to_response("moderators/student_question_list.html", locals(),
                              context_instance=RequestContext(request))
    

@user_passes_test(is_moderator)
def edit_suggested_question(request, id=None):
    text_answer_id = SubType.objects.get(type_int=1).id
    single_choice_answer_id = SubType.objects.get(type_int=2).id
    multi_choice_answer_id = SubType.objects.get(type_int=3).id
        
    student = request.user.get_profile()
    form = QuestionForm
    if id:
        submit = "edit question"
        question = Question.objects.get(pk=id)
    else:
        submit = "add question"
        question = Question()
    
    if request.POST:
        form = QuestionForm(request.POST, instance=question)
        if form.is_valid():
            question = form.save()
            form.formset = QuestionForm.formset(request.POST,
                                request.FILES, 
                                instance=question)

            if form.formset.is_valid():
                    
                form.formset.save()
                return HttpResponseRedirect(redirect_to="/moderator/suggested-questions/")
                                       
    elif bool(id):
        form = QuestionForm(instance=question)
        form.formset = QuestionForm.formset(instance=question)
        if question.answer_type.type_int == 1 and question.answer_set.count(): 
            form.formset.max_num = 1
            form.formset._construct_forms()
    return render_to_response( "moderators/student_add_question.html", 
                               locals(), context_instance=RequestContext(request))

@user_passes_test(is_moderator)
def remove_suggested_question(request, id=None):
    Question.objects.get(pk=id).delete()
    return HttpResponseRedirect(redirect_to="/moderator/suggested-questions/")
