'''
Created on Sep 27, 2011

@author: javkhlan
'''
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from openuniversity.apps.contents.contents_core.models import ContentAlias, \
    Subject, Topic
from openuniversity.apps.contents.practical.models import Lesson, Exercise, \
    Question
from openuniversity.apps.core.models import Language
from openuniversity.apps.translation.models import TranslateAble

def content_unicode(str_val):
    try:
        return ContentAlias.objects.get(alias=str_val).content_object.__unicode__()
    except:
        try:
            return Subject.objects.get(id=int(str_val)).__unicode__()
        except:
            try:
                return Topic.objects.get(id=int(str_val)).__unicode__()
            except:
                try:
                    return Lesson.objects.get(id=int(str_val)).__unicode__()
                except:
                    return Exercise.objects.get(id=int(str_val)).__unicode__()

def question_unicode_by_id(int_val):
    return Question.objects.get(id=int_val).__unicode__()

def render_to_response_with_breadcramb(*args, **kwargs):
    """
    levels:
        0: moderators
        1: subjects
        2: topic
        3: lesson
        4: exercise
        5: exercise question
        6: question answer
    functions:
        add:
        edit:
        translate:
        list:
        view:
    """
    from django.shortcuts import render_to_response
    from openuniversity.apps.moderator.views import *

    ou_mod_bc = kwargs['ou_mod_breadcramb']
    
    if len(args) == 1:
        args = (args[0], {})
    args[1]['ou_mod_breadcramb'] = []
    
    if ou_mod_bc.has_key('pobjects'):
        for item in ou_mod_bc['pobjects']:
            if not item:
                ou_mod_bc['pobjects'].remove(item)

    # moderate
    if ou_mod_bc['level'] >= 0:
        args[1]['ou_mod_breadcramb'].append({ 
         "href": reverse(home),
         "value": "Moderate"
        })
        
    # subjects
    if ou_mod_bc['level'] >= 1:
        href = reverse(subject_list)
        value = "subjects"
        args[1]['ou_mod_breadcramb'].append({ 
         "href": href,
         "value": value
        })
        if ou_mod_bc.has_key('pobjects') and len(ou_mod_bc['pobjects']) > 0:
            href = reverse(subject_view, args=[ou_mod_bc['pobjects'][0],])
            value = content_unicode(ou_mod_bc['pobjects'][0])
            args[1]['ou_mod_breadcramb'].append({ 
             "href": href,
             "value": value
            })
    
    # topics
    if ou_mod_bc['level'] >= 2:
        if ou_mod_bc.has_key('pobjects') and len(ou_mod_bc['pobjects']) > 1:
            href = reverse(topic_view, args=[ou_mod_bc['pobjects'][0],
                                             ou_mod_bc['pobjects'][1],])
            value = content_unicode(ou_mod_bc['pobjects'][1])
            args[1]['ou_mod_breadcramb'].append({ 
             "href": href,
             "value": value
            })
            
    # lessons
    if ou_mod_bc['level'] >= 3:
        if ou_mod_bc.has_key('pobjects') and len(ou_mod_bc['pobjects']) > 2:
            href = reverse(lesson_view, args=[ou_mod_bc['pobjects'][0],
                                              ou_mod_bc['pobjects'][1],
                                              ou_mod_bc['pobjects'][2]])
            value = content_unicode(ou_mod_bc['pobjects'][2])
            args[1]['ou_mod_breadcramb'].append({ 
             "href": href,
             "value": value
            })
            
    # exercises
    if ou_mod_bc['level'] >= 4:
        if ou_mod_bc.has_key('pobjects') and len(ou_mod_bc['pobjects']) > 3:
            href = reverse(exercise_view, args=[ou_mod_bc['pobjects'][0],
                                              ou_mod_bc['pobjects'][1],
                                              ou_mod_bc['pobjects'][2],
                                              ou_mod_bc['pobjects'][3]])
            value = content_unicode(ou_mod_bc['pobjects'][3])
            args[1]['ou_mod_breadcramb'].append({ 
             "href": href,
             "value": value
            })
    # questions
    if ou_mod_bc['level'] >= 5:
        if ou_mod_bc.has_key('pobjects') and len(ou_mod_bc['pobjects']) > 4:
            href = reverse(exercise_question_view, args=[ou_mod_bc['pobjects'][0],
                                              ou_mod_bc['pobjects'][1],
                                              ou_mod_bc['pobjects'][2],
                                              ou_mod_bc['pobjects'][3],
                                              ou_mod_bc['pobjects'][4]])
            value = question_unicode_by_id(ou_mod_bc['pobjects'][4])
            args[1]['ou_mod_breadcramb'].append({ 
             "href": href,
             "value": value
            })
    
    if ou_mod_bc.has_key('function') and \
    ou_mod_bc['function'].split(" ")[0] in ("add", "edit", "view", "translate"):
        args[1]['ou_mod_breadcramb'].append({ 
         "href": "./",
         "value": "%s" % ou_mod_bc['function']
        })

    del(kwargs['ou_mod_breadcramb'])
    return render_to_response(*args, **kwargs)

def get_avail_trans_from_content_object(content_object, current_language):
    # used to show available translations
    t = TranslateAble.objects.get(pk=content_object.id)
    available_translations = t.get_translations_by_language()
    for at in available_translations:
        at['language'] = Language.objects.get(pk=at['language'])
    return [ at for at in available_translations if at['language'].code != current_language ]