#coding: utf-8
from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields.related import OneToOneField
from django.db.models.query_utils import Q
from django.utils.translation import ugettext as _
from openuniversity import settings
import datetime
from datetime import date

GENDER_CHOICES = (
    (False,_('female')),
    (True,_('male')),
)

# Create your models here.
class Account(models.Model):
    user = OneToOneField(User,related_name='user',
                                     verbose_name=_('user'))
    birthdate = models.DateField(null=True, blank=True, 
                                     verbose_name=_('birthdate'))
    location = models.CharField(max_length=100,verbose_name=_('location'))
    mobile_number = models.CharField(max_length=50,
                                     verbose_name=_('mobile number'))
    gender = models.BooleanField(choices=GENDER_CHOICES,
                                     verbose_name=_('gender')) 
    
    def __unicode__(self):
        return self.user.username
    
#    def get_points(self):
#        points_num = 0
#        for a_t in Account_Topic.objects.filter(account__id=self.id):
#            for l in a_t.completed_lessons.all():
#                points_num += l.get_question_points()
#        return points_num
    def get_last_user_msg(self):
        msg = list(self.user.message_set.all())[-1]
        self.user.message_set.all().delete()
        return msg
    
    def get_age(self):
        today = date.today()
        born = self.birthdate
        try: # raised when birth date is February 29 and the current year is not a leap year
            birthday = born.replace(year=today.year)
        except ValueError:
            birthday = born.replace(year=today.year, day=born.day-1)
        if birthday > today:
            return today.year - born.year - 1
        else:
            return today.year - born.year 
    
    def is_moderator(self):
        from django.contrib.auth.models import Group
        #TODO add user messages 
        try:
            moderator = Group.objects.get(name="moderator")
            return moderator in self.user.groups.all()
        except:
            return False

class Language(models.Model):
    language = models.CharField(max_length=50)
    code = models.CharField(max_length=10)
    
    def __unicode__(self):
        return self.code + u" -- " + self.language
    
class AgeGroup(models.Model):
    age = models.CharField(max_length=10)
    
    def __unicode__(self):
        return _("Grade: " + self.age)
        
class Points(models.Model):
    points = models.IntegerField()
    class Meta:
        verbose_name_plural = u"Points"
    
    def __unicode__(self):
        return unicode(self.points)
    
class Division(models.Model):
    age = models.ForeignKey(AgeGroup)
    name = models.CharField(max_length=10) 
    
    def __unicode__(self):
        return self.age.age + " " +self.name
