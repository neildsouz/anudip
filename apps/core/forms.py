#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms
from django.forms.models import ModelForm
from openuniversity.apps.common.models import SubType
from openuniversity.apps.core.models import Language, Account, AgeGroup, Points, Division
from openuniversity.apps.translation.forms import TranslateAbleForm

class LanguageForm(ModelForm):
    
    class Meta:
        model = Language
        
class AccountForm(ModelForm):
    
    class Meta:
        model = Account
        
class AgeGroupForm(ModelForm):
        
    class Meta:
        model = AgeGroup

class PointsForm(ModelForm):
    class Meta:
        model = Points

class DivisionForm(ModelForm):
    class Meta:
	model = Division
