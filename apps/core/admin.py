#coding: utf-8
from django.contrib import admin
from openuniversity.apps.core.forms import LanguageForm, AgeGroupForm, \
    PointsForm, AccountForm, DivisionForm
from openuniversity.apps.core.models import Language, AgeGroup, Points, Account, Division

class LanguageAdmin(admin.ModelAdmin):
    form = LanguageForm

class AgeGroupAdmin(admin.ModelAdmin):
    form = AgeGroupForm
    
class PointsAdmin(admin.ModelAdmin):
    form = PointsForm

class AccountAdmin(admin.ModelAdmin):
    form = AccountForm

class DivisionAdmin(admin.ModelAdmin):
    form = DivisionForm

admin.site.register(Language, LanguageAdmin)
admin.site.register(AgeGroup, AgeGroupAdmin)
admin.site.register(Points, PointsAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Division, DivisionAdmin)
