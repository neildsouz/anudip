'''
Created on Sep 27, 2011

@author: javkhlan
'''
from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.common.views',
    url(r'^vote/(?P<id>\d+)/$', 'vote_for_question'),
    url(r'^(?P<e_alias>[\w\-]+)/suggest/','suggest'),
    url(r'^suggest/','ajax_suggest'),
)