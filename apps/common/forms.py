#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms, forms
from django.contrib.contenttypes.generic import generic_inlineformset_factory
from django.forms.models import inlineformset_factory, inlineformset_factory, \
    BaseInlineFormSet, ModelForm
from openuniversity.apps.common.models import SubType, Difficulty
from openuniversity.apps.contents.practical.forms import AnswerBaseFormSet, \
    QuestionAnswerForm
from openuniversity.apps.contents.practical.models import Answer, Question
from openuniversity.apps.core.models import Language
from openuniversity.apps.translation.forms import TranslateAbleForm
from openuniversity.apps.translation.models import TranslateAble

class StudentQuestionForm(TranslateAbleForm):
    answer_type = forms.ModelChoiceField(queryset=SubType.objects.filter(table_name=Answer._meta.db_table))
    question = forms.CharField(widget=forms.TextInput)
    hint = forms.CharField(widget=forms.TextInput)
    language = forms.ModelChoiceField(queryset=Language.objects.all())
    formset = inlineformset_factory(Question, Answer, formset=
                    AnswerBaseFormSet, 
                    extra=1, exclude=("question"))

    class Meta:
        model = Question
        exclude = ("exercises", "answered_accounts","votes", "suggest_status","submitted_by", "is_required","accounts_voted")
        
    def save(self, **kwargs):
        self.instance.is_published = False
        self.instance.suggest_status = 1
        return super(StudentQuestionForm, self).save(commit=True)
    
    def save_m2m(self, **kwargs):
        pass

#--------------------------------------------- Student suggested question form-------------------------------------------------#
class SuggesttedQuestionForm(TranslateAbleForm):
    answer_type = forms.ModelChoiceField(queryset=SubType.objects.filter(table_name=Answer._meta.db_table))
    question = forms.CharField(widget=forms.TextInput)
    hint = forms.CharField(widget=forms.TextInput, required=False)
    difficulty = forms.ModelChoiceField(queryset=Difficulty.objects.all())
    language = forms.ModelChoiceField(queryset=Language.objects.all())
    formset = inlineformset_factory(Question, Answer, formset=
                    AnswerBaseFormSet, 
                    extra=1,
                    form=QuestionAnswerForm, 
                    exclude=("question"),
                    can_delete=True)

                   
    class Meta:
        model = Question
        exclude = ("exercises", "answered_accounts","votes","suggest_status", 
                   "voted_accounts", "submitted_by", "is_required","is_published")
                   
                   
        
    def save(self, **kwargs):
        self.instance.is_published = False
        self.instance.suggest_status = 1
        return super(SuggesttedQuestionForm, self).save(commit=True)
    
    def save_m2m(self, **kwargs):
        pass
    
    def make_translatable(self):
        for field_key in self.fields.keys():
            if field_key not in self.instance.get_trans_field_names():
                self.fields[field_key].widget=forms.HiddenInput()

class SubTypeForm(TranslateAbleForm):   
    name = forms.CharField(max_length=50)
    description = forms.CharField(widget=forms.TextInput)
    language = forms.ModelChoiceField(queryset=Language.objects.all())
    
    class Meta:
        model = SubType
        
    def save(self, **kwargs):
        return super(SubTypeForm, self).save(commit=True)
    
    def save_m2m(self, **kwargs):
        pass

class DifficultyForm(TranslateAbleForm):
    difficulty_text = forms.CharField(max_length=30)
    language = forms.ModelChoiceField(queryset=Language.objects.all())
    
    class Meta:
        model = Difficulty
        
    def save(self, **kwargs):
        return super(DifficultyForm, self).save(commit=True)
    
    def save_m2m(self, **kwargs):
        pass