# Create your views here.
import json

from django.contrib.auth.decorators import login_required
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from openuniversity.apps.common.forms import SuggesttedQuestionForm
from openuniversity.apps.common.models import Difficulty, SubType
from openuniversity.apps.contents.contents_core.models import ContentAlias
from openuniversity.apps.contents.practical.models import Question, Answer, Exercise_Question, Exercise
from openuniversity.apps.core.models import Language, Points, Account

@login_required()
def suggest(request, e_alias, id=None):

    text_answer_id = SubType.objects.get(type_int=1).id
    single_choice_answer_id = SubType.objects.get(type_int=2).id
    multi_choice_answer_id = SubType.objects.get(type_int=3).id
        
    student = request.user.get_profile()
    form = SuggesttedQuestionForm
    if id:
        submit = "edit question"
        question = Question.objects.get(pk=q_id)
    else:
        submit = "add question"
        question = Question()
    
    if request.POST:
        form = SuggesttedQuestionForm(request.POST, instance=question)
        if form.is_valid():
            question = form.save()
            form.formset = SuggesttedQuestionForm.formset(request.POST,
                                request.FILES, 
                                instance=question)

            if form.formset.is_valid():
                try:
                    m2m_exercise = ContentAlias.objects.get(alias=e_alias).\
                    content_object
                except:
                    m2m_exercise=Exercise.objects.get(pk=int(e_alias))
                count = Exercise_Question.objects.filter(Q(m2m_question=question)&
                            Q(m2m_exercise=m2m_exercise)).count()
                if count > 1:
                    for e_q_item in Exercise_Question.objects.filter(
                         Q(m2m_question=question)&
                         Q(m2m_exercise=m2m_exercise)).all()[1:]:
                        e_q_item.delete()
                # deletes additional m2m rows, there must always be one::end
                    # if there is a exercise_question data
                    e_q = Exercise_Question.objects.get(Q(m2m_question=question)&
                      Q(m2m_exercise=m2m_exercise))
                    e_q.difficulty =\
                      Difficulty.objects.get(pk=int(request.POST['difficulty']))
                    e_q.save()
                elif count == 0: # if there are now exercise_question data
                    Exercise_Question(
                      m2m_question=question,
                      m2m_exercise=m2m_exercise,
                      difficulty =\
                      Difficulty.objects.get(pk=\
                                    int(request.POST['difficulty']))
                    ).save()
                    
                form.formset.save()
                return HttpResponseRedirect(redirect_to="/courses/")
                                       
    elif bool(id):
        form = SuggesttedQuestionForm(instance=question)
        # deletes additional m2m rows, there must always be one::start
        try:
            m2m_exercise = ContentAlias.objects.get(alias=e_alias).\
            content_object
        except:
            m2m_exercise=Exercise.objects.get(pk=int(e_alias))
        count = Exercise_Question.objects.filter(Q(m2m_question=question)&
                    Q(m2m_exercise=m2m_exercise)).count()
        if count > 1:
            for e_q_item in Exercise_Question.objects.filter(
                 Q(m2m_question=question)&
                 Q(m2m_exercise=m2m_exercise)).all()[1:]:
                e_q_item.delete()
        # deletes additional m2m rows, there must always be one::end
        try:
            exercise_question = Exercise_Question.objects.get(
              Q(m2m_question=question)&
              Q(m2m_exercise=ContentAlias.objects.get(alias=e_alias)
              .content_object))
        except:
            exercise_question = Exercise_Question.objects.get(
              Q(m2m_question=question)&
              Q(m2m_exercise=Exercise.objects.get(pk=int(e_alias)))
            )
        
        form.fields['difficulty']._set_choices(Difficulty.objects.get_choice_tuple())
        form.fields['difficulty'].initial = \
        exercise_question.difficulty
        form.formset = SuggesttedQuestionForm.formset(instance=question)
        if question.answer_type.type_int == 1 and question.answer_set.count(): 
            form.formset.max_num = 1
            form.formset._construct_forms()
    return render_to_response( "courses/students/add_suggest_questions.html", 
                               locals(), context_instance=RequestContext(request))
                 
@login_required()
def ajax_suggest(request):
    student = request.user.get_profile()
    single_choice_answer = SubType.objects.get(type_int=2)
    multi_choice_answer = SubType.objects.get(type_int=3)
    #QueryDict: <QueryDict: {u'answer-count': [u'0'], u'exercise-id': [u'23'], u'answer-input': [u''], u'correct-answers': [u''], u'question-type-select': [u'1'], u'correctRadio': [u''], u'question': [u'j'], u'answer': [u'j'], u'wrong-answers': [u'']}>
    exercise = Exercise.objects.get(id=request.POST.get("exercise-id"))
    questionText = request.POST.get("question")
    questionType = long(request.POST.get("question-type-select"))
    
    question = Question()
    kwargs = {"language": Language.objects.get(code=request.LANGUAGE_CODE),
              "question": questionText,
              "hint":"",}
    question.submitted_by = student
    question.is_published = False
    question.suggest_status = 1
    question.points = Points.objects.get(points=1)
    question.answer_type = SubType.objects.get(type_int=questionType)
    question.save()
    question.saveTranslations(**kwargs)
     
    #Text answer
    if questionType == 1:
        answerText = request.POST.get("answer")
        answer = Answer()
        answer.answer = answerText
        answer.is_correct = True
        answer.question = question
        answer.save()
    elif questionType == 2 or questionType == 3:
        correctAns = request.POST.get("correct-answers").split(",")
        wrongAns = request.POST.get("wrong-answers").split(",")
        for ans in correctAns:
            answer = Answer()
            answer.answer = ans
            answer.is_correct = True
            answer.question = question
            answer.save()
        for ans in wrongAns:
            answer = Answer()
            answer.answer = ans
            answer.is_correct = False
            answer.question = question
            answer.save()
    Exercise_Question(m2m_question=question, m2m_exercise=exercise, difficulty=Difficulty.objects.get(difficulty_int=1)).save()
    return HttpResponse(json.dumps({"message":"Thank you for submitting your question, a teacher will validate and publish it later"}),mimetype='application/json')
                     
                                  
def vote_for_question(request, id=None):
    if request.user.is_authenticated() and id!=None:
        question = Question.objects.get(pk=id)
        account = request.user.get_profile()
        if question not in account.voted_questions.all():
            account.voted_questions.add(question)
            question.votes += 1
            question.save()
            return HttpResponse(_("success"))
        return HttpResponse(_("you have already voted for this question"))
    return HttpResponse(_("please sign in with your account"))    