from django.db import models
from openuniversity.apps.translation.models import TranslateAble

# Create your models here.
class SubType(TranslateAble):
    table_name = models.CharField(max_length=50)
    type_int = models.IntegerField(null=False, blank=False, unique=True)
    
    def get_trans_field_names(self):
        return ("name", "description")
    
    def __unicode__(self):
        return self.t9n(field_name="name", current_language=True, 
                    show_language=True)
        
    
class DifficultyManager(models.Manager):
    def get_choice_tuple(self):
        retVal = []
        for obj in self.all():
            retVal.append((obj.id, obj.t9n("difficulty_text")))
            
        return tuple(retVal)
            
class Difficulty(TranslateAble):
    difficulty_int = models.IntegerField()
    objects = DifficultyManager()
    
    class Meta:
        verbose_name_plural = u"difficulties"
    
    def __unicode__(self):
        return self.t9n(field_name="difficulty_text", current_language=True, 
            show_language=True)
    
    def get_trans_field_names(self):
        return ("difficulty_text",)