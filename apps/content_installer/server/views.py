from django.http import HttpResponse
from openuniversity.apps.contents.contents_core.models import Subject
from openuniversity.apps.contents.practical.models import Lesson
import json
import simplejson

# Create your views here.
def lesson_list_json(request):
    """
       step 1: get the list
    """
    print "lesson_list_json::start"
    top_level = []
    subjects = Subject.objects.filter(offline=False)
    for si in subjects:
        s = {}
        s['name'] = si.t9n("name")
        s['unique'] = si.unique
        s['children'] = []
        for ti in si.topic_set.filter(offline=False):
            t = {}
            t['name'] = ti.t9n("name")
            t['unique'] = ti.unique
            t['children'] = []
            s['children'].append(t)
            for li in ti.lessons.filter(offline=False):
                l = {} 
                l['name'] = li.t9n("name")
                l['unique'] = li.unique
                l['children'] = li.exercises.filter(offline=True) or ""
                t['children'].append(l)
        top_level.append(s)
                
#    lessons = Lesson.objects.all()
#    lessons_json = [ l.t9n("name") for l in lessons ]
    print "lesson_list_json::content(python)"
    print top_level
    print "lesson_list_json::content(json)"
    print json.dumps(top_level)
    print "lesson_list_json::end"
    print "--=-- " * 20
    return HttpResponse(simplejson.dumps(top_level), mimetype="application/json")

def install_content_json(request):
    # TODO: language for the content needed
    print "install_content_json::start"
#    print request.POST['content_uniques'].split(",")
    content_unique_keys = [ key.strip() for key in request.POST['content_uniques'].split(",")]
    print content_unique_keys
    toplevel = {}
    for key in content_unique_keys:
        print "key"
        if Lesson.objects.filter(unique=key).count() == 1:
            print "if 1"
            lesson = Lesson.objects.get(unique=key)
            if lesson.topic.subject.unique not in [ tl_key for tl_key in toplevel.keys() ]:
                print "if 1-1", lesson.topic.subject.alias_or_id()
                toplevel[lesson.topic.subject.unique] = {
                      "name":lesson.topic.subject.t9n("name"), 
                      #"desc":lesson.topic.subject.t9n("description"),
                      "alias":lesson.topic.subject.alias_or_id(),
                      "level":0,
                }
                print "if 1-2"
            print "if 2"
            if lesson.topic.unique not in [ topic_key for topic_key in toplevel.keys() ]:
                print "if 2-1"
                toplevel[lesson.topic.unique] = {
                      "name":lesson.topic.t9n("name"),
                      "desc":lesson.topic.t9n("description"),
                      "alias":lesson.topic.alias(),
                      "age_group":lesson.topic.age_group.id,
                      "parent":lesson.topic.subject.unique,
                      "level":1,
                }
                print "if 2-2"
            print "if 3"
            if lesson.unique not in [ lesson_key for lesson_key in toplevel.keys() ]:
                print "if 3-1"
                toplevel[lesson.unique] = {
                      "name":lesson.t9n("name"),
                      "desc":lesson.t9n("description"),
                      "alias":lesson.alias(),
                      "parent":lesson.topic.unique,
                      "level":2,
                }
            print "for 1"
            # add exercise, questions and answers here
            for e in lesson.exercises.all():
                print "for 1-1"
                toplevel[e.unique] = {
                       "name":e.t9n("name"),
                       "desc":e.t9n("description"),
                       "alias":e.alias(),
                       "parent":e.lesson.unique,
                       "children": []
                }
                for q in e.questions.all():
                    print "for 1-1-1"
                    toplevel[e.unique]["children"].append({
                        "question":q.t9n("question"),
                        "hint":q.t9n("hint"),
                        "answer_type_int":q.answer_type.type_int,
                        "children":[],
                        "points_id":q.points.id
                    })
                    for a in q.answer_set.all():
                        print "for 1-1-1-1"
                        toplevel[e.unique]['children'][-1]['children'].append({
                            "answer": a.answer,
                            "is_correct": a.is_correct
                        })
    
    print "install_content_json::contents"
    print toplevel
    print "install_content_json::contents::json"
    print simplejson.dumps(toplevel)
    print "install_content_json::end"
    
    print "--=-- " * 20
    return HttpResponse(simplejson.dumps(toplevel))
