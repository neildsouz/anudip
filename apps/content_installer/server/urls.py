from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.content_installer.server.views',
    url(r'^lesson_list_json$', 'lesson_list_json'),
    url(r'^install_content_json$', 'install_content_json')
)

#urlpatterns += patterns('django.contrib.auth.views',
#    url(r'^logout/$', 'logout',{'template_name': 'index.html'}),
#)
