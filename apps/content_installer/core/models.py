from django.db import models
from openuniversity import settings
import datetime

# Create your models here.
class UniqueDTCharField(models.CharField): # Unique DatetimeCharField
    def pre_save(self, *args, **kwargs):
        val = super(UniqueDTCharField, self).pre_save(*args, **kwargs)
        if bool(val):
            return val
        if settings.OFFLINE_VERSION:
            return ""
        return datetime.datetime.now().__str__()

class OfflineContentBooleanField(models.BooleanField): 
    """ 
    True if the content was added out of sync in offline version, 
    False if it's in sync with server and was added in online version
    """
    def pre_save(self, *args, **kwargs):
        return settings.OFFLINE_VERSION

class ContentOnOffMarker(models.Model):
    # True if the content was added in offline version
    # False if the content was downloaded from online server
    offline = OfflineContentBooleanField(default=False)
    # unique identifier for the content
    unique = UniqueDTCharField(max_length=50)
    
    def save(self, *args, **kwargs):
        return super(ContentOnOffMarker).save(*args, **kwargs)