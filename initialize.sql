-- MySQL dump 10.13  Distrib 5.1.61, for redhat-linux-gnu (i386)
--
-- Host: localhost    Database: dev
-- ------------------------------------------------------
-- Server version	5.1.61

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'moderator');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (1,1,13),(2,1,14),(3,1,15),(4,1,16),(5,1,17),(6,1,18),(7,1,19),(8,1,20),(9,1,21),(10,1,22),(11,1,23),(12,1,24),(13,1,25),(14,1,26),(15,1,27),(16,1,28),(17,1,29),(18,1,30),(19,1,31),(20,1,32),(21,1,33),(22,1,34),(23,1,35),(24,1,36),(25,1,37),(26,1,38),(27,1,39),(28,1,40),(29,1,41),(30,1,42),(31,1,43),(32,1,44),(33,1,45),(34,1,46),(35,1,47),(36,1,48),(37,1,49),(38,1,50),(39,1,51),(40,1,52),(41,1,53),(42,1,54),(43,1,55),(44,1,56),(45,1,57),(46,1,58),(47,1,59),(48,1,60),(49,1,61),(50,1,62),(51,1,63),(52,1,64),(53,1,65),(54,1,66),(55,1,67),(56,1,68),(57,1,69),(58,1,70),(59,1,71),(60,1,72),(61,1,73),(62,1,74),(63,1,75),(64,1,76),(65,1,77),(66,1,78),(67,1,79),(68,1,80),(69,1,81),(70,1,82),(71,1,83),(72,1,84),(73,1,85),(74,1,86),(75,1,87),(76,1,88),(77,1,89),(78,1,90),(79,1,91),(80,1,92),(81,1,93),(82,1,94),(83,1,95),(84,1,96),(85,1,97),(86,1,98),(87,1,99),(88,1,100),(89,1,101),(90,1,102),(91,1,103),(92,1,104),(93,1,105),(94,1,106),(95,1,107),(96,1,108),(97,1,109),(98,1,110),(99,1,111),(100,1,112),(101,1,113),(102,1,114),(103,1,115),(104,1,116),(105,1,117);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_403f60f` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_message`
--

LOCK TABLES `auth_message` WRITE;
/*!40000 ALTER TABLE `auth_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add message',4,'add_message'),(11,'Can change message',4,'change_message'),(12,'Can delete message',4,'delete_message'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add account',8,'add_account'),(23,'Can change account',8,'change_account'),(24,'Can delete account',8,'delete_account'),(25,'Can add language',9,'add_language'),(26,'Can change language',9,'change_language'),(27,'Can delete language',9,'delete_language'),(28,'Can add age group',10,'add_agegroup'),(29,'Can change age group',10,'change_agegroup'),(30,'Can delete age group',10,'delete_agegroup'),(31,'Can add points',11,'add_points'),(32,'Can change points',11,'change_points'),(33,'Can delete points',11,'delete_points'),(34,'Can add content on off marker',12,'add_contentonoffmarker'),(35,'Can change content on off marker',12,'change_contentonoffmarker'),(36,'Can delete content on off marker',12,'delete_contentonoffmarker'),(37,'Can add translate able',13,'add_translateable'),(38,'Can change translate able',13,'change_translateable'),(39,'Can delete translate able',13,'delete_translateable'),(40,'Can add translation',14,'add_translation'),(41,'Can change translation',14,'change_translation'),(42,'Can delete translation',14,'delete_translation'),(43,'Can add badge',15,'add_badge'),(44,'Can change badge',15,'change_badge'),(45,'Can delete badge',15,'delete_badge'),(46,'Can add sub type',16,'add_subtype'),(47,'Can change sub type',16,'change_subtype'),(48,'Can delete sub type',16,'delete_subtype'),(49,'Can add difficulty',17,'add_difficulty'),(50,'Can change difficulty',17,'change_difficulty'),(51,'Can delete difficulty',17,'delete_difficulty'),(52,'Can add subject',18,'add_subject'),(53,'Can change subject',18,'change_subject'),(54,'Can delete subject',18,'delete_subject'),(55,'Can add topic',19,'add_topic'),(56,'Can change topic',19,'change_topic'),(57,'Can delete topic',19,'delete_topic'),(58,'Can add content alias',20,'add_contentalias'),(59,'Can change content alias',20,'change_contentalias'),(60,'Can delete content alias',20,'delete_contentalias'),(61,'Can add video',21,'add_video'),(62,'Can change video',21,'change_video'),(63,'Can delete video',21,'delete_video'),(64,'Can add pdf file',22,'add_pdffile'),(65,'Can change pdf file',22,'change_pdffile'),(66,'Can delete pdf file',22,'delete_pdffile'),(67,'Can add image',23,'add_image'),(68,'Can change image',23,'change_image'),(69,'Can delete image',23,'delete_image'),(70,'Can add lesson',24,'add_lesson'),(71,'Can change lesson',24,'change_lesson'),(72,'Can delete lesson',24,'delete_lesson'),(73,'Can add question',25,'add_question'),(74,'Can change question',25,'change_question'),(75,'Can delete question',25,'delete_question'),(76,'Can add exercise',26,'add_exercise'),(77,'Can change exercise',26,'change_exercise'),(78,'Can delete exercise',26,'delete_exercise'),(79,'Can add exercise_ question',27,'add_exercise_question'),(80,'Can change exercise_ question',27,'change_exercise_question'),(81,'Can delete exercise_ question',27,'delete_exercise_question'),(82,'Can add answer',28,'add_answer'),(83,'Can change answer',28,'change_answer'),(84,'Can delete answer',28,'delete_answer'),(85,'Can add course',29,'add_course'),(86,'Can change course',29,'change_course'),(87,'Can delete course',29,'delete_course'),(88,'Can add course contents',30,'add_coursecontents'),(89,'Can change course contents',30,'change_coursecontents'),(90,'Can delete course contents',30,'delete_coursecontents'),(91,'Can add account_ lesson',31,'add_account_lesson'),(92,'Can change account_ lesson',31,'change_account_lesson'),(93,'Can delete account_ lesson',31,'delete_account_lesson'),(94,'Can add account_ lesson_ question',32,'add_account_lesson_question'),(95,'Can change account_ lesson_ question',32,'change_account_lesson_question'),(96,'Can delete account_ lesson_ question',32,'delete_account_lesson_question'),(97,'Can add completed exercise',33,'add_completedexercise'),(98,'Can change completed exercise',33,'change_completedexercise'),(99,'Can delete completed exercise',33,'delete_completedexercise'),(100,'Can add contact anon',34,'add_contactanon'),(101,'Can change contact anon',34,'change_contactanon'),(102,'Can delete contact anon',34,'delete_contactanon'),(103,'Can add contact',35,'add_contact'),(104,'Can change contact',35,'change_contact'),(105,'Can delete contact',35,'delete_contact'),(106,'Can add registration profile',36,'add_registrationprofile'),(107,'Can change registration profile',36,'change_registrationprofile'),(108,'Can delete registration profile',36,'delete_registrationprofile'),(109,'Can add teacher student content',37,'add_teacherstudentcontent'),(110,'Can change teacher student content',37,'change_teacherstudentcontent'),(111,'Can delete teacher student content',37,'delete_teacherstudentcontent'),(112,'Can add teacher student',38,'add_teacherstudent'),(113,'Can change teacher student',38,'change_teacherstudent'),(114,'Can delete teacher student',38,'delete_teacherstudent'),(115,'Can add teacher student history',39,'add_teacherstudenthistory'),(116,'Can change teacher student history',39,'change_teacherstudenthistory'),(117,'Can delete teacher student history',39,'delete_teacherstudenthistory'),(118,'Can add log entry',40,'add_logentry'),(119,'Can change log entry',40,'change_logentry'),(120,'Can delete log entry',40,'delete_logentry'),(121,'Can add division',41,'add_division'),(122,'Can change division',41,'change_division'),(123,'Can delete division',41,'delete_division'),(124,'Can add student_ division',42,'add_student_division'),(125,'Can change student_ division',42,'change_student_division'),(126,'Can delete student_ division',42,'delete_student_division'),(127,'Can add topic_ teacher',43,'add_topic_teacher'),(128,'Can change topic_ teacher',43,'change_topic_teacher'),(129,'Can delete topic_ teacher',43,'delete_topic_teacher');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'nomadadmin','','','brian@teachaclass.org','sha1$95d55$3dbad539e2b2a90597dd2860417a135d98273705',1,1,1,'2012-04-27 03:46:33','2012-02-24 19:33:24'),(2,'brian','Brian','T','brian@teachaclass.org','sha1$748ac$1a7ab40815c76834db5d2e30b0d09aea4eba61d0',0,1,0,'2012-04-24 03:20:26','2012-02-24 19:37:20'),(3,'neil','Neil','Dsouza','neildsouz@gmail.com','sha1$dea25$f96a54388eb3fc159d8edf3b2468c913e123a33e',1,1,0,'2012-04-25 05:02:01','2012-02-24 21:25:11'),(4,'rohan','Rohan','Sahgal','rohan@gmail.com','sha1$778ae$bb38b4a0c6bc6a40b63e5ba3f03b4f9a0a33738c',0,1,0,'2012-03-30 15:19:29','2012-03-19 08:29:19');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (3,2,1),(2,3,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM AUTO_INCREMENT=421 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (420,2,117),(419,2,116),(418,2,115),(417,2,114),(416,2,113),(415,2,112),(414,2,111),(413,2,110),(412,2,109),(411,2,108),(410,2,107),(409,2,106),(408,2,105),(407,2,104),(406,2,103),(405,2,102),(404,2,101),(403,2,100),(402,2,99),(401,2,98),(400,2,97),(399,2,96),(398,2,95),(397,2,94),(396,2,93),(395,2,92),(394,2,91),(393,2,90),(392,2,89),(391,2,88),(390,2,87),(389,2,86),(388,2,85),(387,2,84),(386,2,83),(385,2,82),(384,2,81),(383,2,80),(382,2,79),(381,2,78),(380,2,77),(379,2,76),(378,2,75),(377,2,74),(376,2,73),(375,2,72),(374,2,71),(373,2,70),(372,2,69),(371,2,68),(370,2,67),(369,2,66),(368,2,65),(367,2,64),(366,2,63),(365,2,62),(364,2,61),(363,2,60),(362,2,59),(361,2,58),(360,2,57),(359,2,56),(358,2,55),(357,2,54),(356,2,53),(355,2,52),(354,2,51),(353,2,50),(352,2,49),(351,2,48),(350,2,47),(349,2,46),(348,2,45),(347,2,44),(346,2,43),(345,2,42),(344,2,41),(343,2,40),(342,2,39),(341,2,38),(340,2,37),(339,2,36),(338,2,35),(337,2,34),(336,2,33),(335,2,32),(334,2,31),(333,2,30),(332,2,29),(331,2,28),(330,2,27),(329,2,26),(328,2,25),(327,2,24),(326,2,23),(325,2,22),(324,2,21),(323,2,20),(322,2,19),(321,2,18),(320,2,17),(319,2,16),(318,2,15),(317,2,14),(316,2,13);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_badge`
--

DROP TABLE IF EXISTS `badge_badge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_badge` (
  `translateable_ptr_id` int(11) NOT NULL,
  `points_num` int(11) NOT NULL,
  `exercise_id` int(11) DEFAULT NULL,
  `is_special` tinyint(1) NOT NULL,
  `file` varchar(300) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `badge_badge_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_badge`
--

LOCK TABLES `badge_badge` WRITE;
/*!40000 ALTER TABLE `badge_badge` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_badge_accounts`
--

DROP TABLE IF EXISTS `badge_badge_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_badge_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `badge_id` (`badge_id`,`account_id`),
  KEY `badge_badge_accounts_7f24a4dc` (`badge_id`),
  KEY `badge_badge_accounts_6f2fe10e` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_badge_accounts`
--

LOCK TABLES `badge_badge_accounts` WRITE;
/*!40000 ALTER TABLE `badge_badge_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_badge_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_difficulty`
--

DROP TABLE IF EXISTS `common_difficulty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_difficulty` (
  `translateable_ptr_id` int(11) NOT NULL,
  `difficulty_int` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_difficulty`
--

LOCK TABLES `common_difficulty` WRITE;
/*!40000 ALTER TABLE `common_difficulty` DISABLE KEYS */;
INSERT INTO `common_difficulty` VALUES (6,1),(7,2),(8,3);
/*!40000 ALTER TABLE `common_difficulty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_subtype`
--

DROP TABLE IF EXISTS `common_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_subtype` (
  `translateable_ptr_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `type_int` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `type_int` (`type_int`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_subtype`
--

LOCK TABLES `common_subtype` WRITE;
/*!40000 ALTER TABLE `common_subtype` DISABLE KEYS */;
INSERT INTO `common_subtype` VALUES (9,'practical_answer',3),(10,'practical_answer',2),(11,'practical_answer',1);
/*!40000 ALTER TABLE `common_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_contentalias`
--

DROP TABLE IF EXISTS `contents_core_contentalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_contentalias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `contents_core_contentalias_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_contentalias`
--

LOCK TABLES `contents_core_contentalias` WRITE;
/*!40000 ALTER TABLE `contents_core_contentalias` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_core_contentalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_subject`
--

DROP TABLE IF EXISTS `contents_core_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_subject` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_subject`
--

LOCK TABLES `contents_core_subject` WRITE;
/*!40000 ALTER TABLE `contents_core_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_core_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_topic`
--

DROP TABLE IF EXISTS `contents_core_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_topic` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `age_group_id` int(11) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `contents_core_topic_386a7d9c` (`age_group_id`),
  KEY `contents_core_topic_638462f1` (`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_topic`
--

LOCK TABLES `contents_core_topic` WRITE;
/*!40000 ALTER TABLE `contents_core_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_core_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_account`
--

DROP TABLE IF EXISTS `core_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_account`
--

LOCK TABLES `core_account` WRITE;
/*!40000 ALTER TABLE `core_account` DISABLE KEYS */;
INSERT INTO `core_account` VALUES (1,2,'1990-01-01','Mongolia','0',1),(2,3,'1982-01-07','Mongolia','0',1),(3,4,'1990-01-01','Mongolia','0',1);
/*!40000 ALTER TABLE `core_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_agegroup`
--

DROP TABLE IF EXISTS `core_agegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_agegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_agegroup`
--

LOCK TABLES `core_agegroup` WRITE;
/*!40000 ALTER TABLE `core_agegroup` DISABLE KEYS */;
INSERT INTO `core_agegroup` VALUES (1,'1'),(2,'2'),(3,'3');
/*!40000 ALTER TABLE `core_agegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_contentonoffmarker`
--

DROP TABLE IF EXISTS `core_contentonoffmarker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_contentonoffmarker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offline` tinyint(1) NOT NULL,
  `unique` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_contentonoffmarker`
--

LOCK TABLES `core_contentonoffmarker` WRITE;
/*!40000 ALTER TABLE `core_contentonoffmarker` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_contentonoffmarker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_division`
--

DROP TABLE IF EXISTS `core_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age_id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_division_7fee3046` (`age_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_division`
--

LOCK TABLES `core_division` WRITE;
/*!40000 ALTER TABLE `core_division` DISABLE KEYS */;
INSERT INTO `core_division` VALUES (1,2,'A'),(2,2,'B'),(3,1,'A'),(4,1,'B'),(5,3,'A'),(6,3,'B');
/*!40000 ALTER TABLE `core_division` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_language`
--

DROP TABLE IF EXISTS `core_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(50) NOT NULL,
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_language`
--

LOCK TABLES `core_language` WRITE;
/*!40000 ALTER TABLE `core_language` DISABLE KEYS */;
INSERT INTO `core_language` VALUES (1,'English','en-us'),(2,'Mongolian','mn'),(3,'Russian','ru');
/*!40000 ALTER TABLE `core_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_points`
--

DROP TABLE IF EXISTS `core_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_points`
--

LOCK TABLES `core_points` WRITE;
/*!40000 ALTER TABLE `core_points` DISABLE KEYS */;
INSERT INTO `core_points` VALUES (3,3),(2,2),(1,1);
/*!40000 ALTER TABLE `core_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_course`
--

DROP TABLE IF EXISTS `courses_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_course` (
  `translateable_ptr_id` int(11) NOT NULL,
  `created_account_id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `edited_date` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `courses_course_418bb3` (`created_account_id`),
  KEY `courses_course_279e212f` (`course_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_course`
--

LOCK TABLES `courses_course` WRITE;
/*!40000 ALTER TABLE `courses_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_coursecontents`
--

DROP TABLE IF EXISTS `courses_coursecontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_coursecontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `added_account_id` int(11) DEFAULT NULL,
  `content_level_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_coursecontents_b7271b` (`course_id`),
  KEY `courses_coursecontents_cc8ff3c` (`content_id`),
  KEY `courses_coursecontents_370986a3` (`added_account_id`),
  KEY `courses_coursecontents_7aaf98d1` (`content_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_coursecontents`
--

LOCK TABLES `courses_coursecontents` WRITE;
/*!40000 ALTER TABLE `courses_coursecontents` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_coursecontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2012-02-24 19:40:01',1,3,'2','brian',2,'Changed user_permissions.'),(2,'2012-02-24 19:44:21',1,3,'2','brian',2,'No fields changed.'),(3,'2012-02-24 21:22:23',1,2,'1','moderator',1,''),(4,'2012-02-24 21:23:43',1,3,'2','brian',2,'Changed groups.'),(5,'2012-02-24 21:27:29',1,3,'3','neil',2,'Changed groups.'),(6,'2012-02-25 12:57:56',1,9,'1','en-us -- English',1,''),(7,'2012-02-25 12:58:28',1,9,'2','mn -- Mongolian',1,''),(8,'2012-02-25 12:58:53',1,9,'3','ru -- Russian',1,''),(9,'2012-02-26 15:31:11',1,17,'6','easy',1,''),(10,'2012-02-26 15:31:36',1,17,'7','moderate',1,''),(11,'2012-02-26 15:32:06',1,17,'8','hard',1,''),(12,'2012-02-26 15:34:38',1,16,'9','multiple choice',1,''),(13,'2012-02-26 15:35:28',1,16,'10','single choice',1,''),(14,'2012-02-26 15:36:19',1,16,'11','text answer',1,''),(15,'2012-02-26 15:40:11',1,21,'1','http://www.youtube.com/watch?feature=player_embedded&v=bixg6QUNHDg',2,'Changed url_path_embed.'),(16,'2012-03-19 08:25:33',1,3,'2','brian',2,'Changed first_name and last_name.'),(17,'2012-04-24 03:12:41',1,25,'13','Hi how are you?',3,''),(18,'2012-04-24 03:13:04',1,24,'5','Asking Questions',3,''),(19,'2012-04-24 03:13:04',1,24,'3','speaking-saying-hello-and-goodbye',3,''),(20,'2012-04-24 03:14:03',1,3,'8','neil2',3,''),(21,'2012-04-24 03:14:03',1,3,'5','rohan2',3,''),(22,'2012-04-24 03:14:03',1,3,'6','rohan3',3,''),(23,'2012-04-24 03:14:03',1,3,'7','rohan4',3,''),(24,'2012-04-24 03:15:29',1,20,'3','how',3,''),(25,'2012-04-24 03:15:29',1,20,'2','test1',3,''),(26,'2012-04-24 03:15:29',1,20,'1','test',3,''),(27,'2012-04-24 03:16:17',1,18,'1','English',3,''),(28,'2012-04-24 03:17:46',1,41,'3','1 A',1,''),(29,'2012-04-24 03:17:53',1,41,'4','1 B',1,''),(30,'2012-04-24 03:18:01',1,41,'5','3 A',1,''),(31,'2012-04-24 03:18:10',1,41,'6','3 B',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'message','auth','message'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'account','core','account'),(9,'language','core','language'),(10,'age group','core','agegroup'),(11,'points','core','points'),(12,'content on off marker','core','contentonoffmarker'),(13,'translate able','translation','translateable'),(14,'translation','translation','translation'),(15,'badge','badge','badge'),(16,'sub type','common','subtype'),(17,'difficulty','common','difficulty'),(18,'subject','contents_core','subject'),(19,'topic','contents_core','topic'),(20,'content alias','contents_core','contentalias'),(21,'video','files','video'),(22,'pdf file','files','pdffile'),(23,'image','files','image'),(24,'lesson','practical','lesson'),(25,'question','practical','question'),(26,'exercise','practical','exercise'),(27,'exercise_ question','practical','exercise_question'),(28,'answer','practical','answer'),(29,'course','courses','course'),(30,'course contents','courses','coursecontents'),(31,'account_ lesson','stats','account_lesson'),(32,'account_ lesson_ question','stats','account_lesson_question'),(33,'completed exercise','stats','completedexercise'),(34,'contact anon','feedback','contactanon'),(35,'contact','feedback','contact'),(36,'registration profile','registration','registrationprofile'),(37,'teacher student content','teacher_student_core','teacherstudentcontent'),(38,'teacher student','teacher_student_core','teacherstudent'),(39,'teacher student history','teacher_student_core','teacherstudenthistory'),(40,'log entry','admin','logentry'),(41,'division','core','division'),(42,'student_ division','student','student_division'),(43,'topic_ teacher','teacher','topic_teacher');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('669bdc546745be855dcd9e31015c1768','NWRkNGY1MWU1YWY4NjYwYzUyYzYwOWUwYzI4ZTJjNzM4MWQ5NzNiZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n','2012-03-09 21:23:09'),('5f395e56a6664d1b8318ba32280c69c0','OTdmYmM0NjRkNmM3NjEyMjRhOGFkOTg5ZDI3ZTE4MGEwNjQ5MmNhYjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAG1ucQNzLg==\n','2012-03-11 20:27:11'),('8a46b44c870c26f3a3283d9b64f86e3e','NzI0OTU5MzJlNmZiODJiN2E3ZjZhNmQ0MDRhMGZmNjE3NGQ3MjRkMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQJ1Lg==\n','2012-03-11 16:07:24'),('bbb989d29d1a11eb9b5a3509cf593cf4','Yjg2MzMyZWJkMzc2MWJkNTBlZmE5NGZjZmZmY2I4MDU1YjJiN2QwMTqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAHJ1cQNzLg==\n','2012-03-11 03:32:55'),('136520475d42b2c9c6772f55f2a9f694','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2012-03-09 21:23:59'),('0c34a8fe9161641f2f585c217bef1a43','ODI3YWVkMjEwNmM4YWU2OTkyYTYxZjczMThjNWZjYWRjNmI1MGJjZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQN1Lg==\n','2012-03-10 02:03:04'),('ca5f21157a9ba9a263df3739add3e6e7','OTdmYmM0NjRkNmM3NjEyMjRhOGFkOTg5ZDI3ZTE4MGEwNjQ5MmNhYjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAG1ucQNzLg==\n','2012-03-11 03:53:49'),('da260386944468bfa932a6f7b9cf5210','OTdmYmM0NjRkNmM3NjEyMjRhOGFkOTg5ZDI3ZTE4MGEwNjQ5MmNhYjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAG1ucQNzLg==\n','2012-03-11 11:55:39'),('107060d3dd92897b6203488687e4756c','Yjg2MzMyZWJkMzc2MWJkNTBlZmE5NGZjZmZmY2I4MDU1YjJiN2QwMTqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAHJ1cQNzLg==\n','2012-03-11 12:49:29'),('a9d74039adc0461b26de429def7184a8','NWRkNGY1MWU1YWY4NjYwYzUyYzYwOWUwYzI4ZTJjNzM4MWQ5NzNiZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n','2012-03-11 15:58:36'),('763eb506842f0d49186a9d70f6f45b2b','Yjg2MzMyZWJkMzc2MWJkNTBlZmE5NGZjZmZmY2I4MDU1YjJiN2QwMTqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAHJ1cQNzLg==\n','2012-03-11 21:18:39'),('b3f0933de4a701961298bf21f2ebf50f','OTdmYmM0NjRkNmM3NjEyMjRhOGFkOTg5ZDI3ZTE4MGEwNjQ5MmNhYjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAG1ucQNzLg==\n','2012-03-12 02:08:37'),('7ee87176eb0d78e3187e21db44e0b764','Yjg2MzMyZWJkMzc2MWJkNTBlZmE5NGZjZmZmY2I4MDU1YjJiN2QwMTqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlcQJYAgAAAHJ1cQNzLg==\n','2012-03-12 05:56:10'),('ad764870632a337975515a9863be2dd6','NWRkNGY1MWU1YWY4NjYwYzUyYzYwOWUwYzI4ZTJjNzM4MWQ5NzNiZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n','2012-03-12 06:50:41'),('da0c1c13d662ee88b9d7b6a51ca6bce8','NWRkNGY1MWU1YWY4NjYwYzUyYzYwOWUwYzI4ZTJjNzM4MWQ5NzNiZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n','2012-03-12 09:12:25'),('d93ab4cfd559f7e17aad8cc27cde740a','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEEigEDdS5jNGZlNDZjNGJjZjYxMWQwNDQz\nYWVjYmZkYTAxZTRlOA==\n','2012-04-13 15:30:11'),('572655fef68426334a54d38b474369cd','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2012-05-08 03:01:51'),('b625b5d413bd4c9aa594e295139e481c','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2012-05-08 03:01:53'),('25e7f9fe328696f0e809ed158b049f18','ODI3YWVkMjEwNmM4YWU2OTkyYTYxZjczMThjNWZjYWRjNmI1MGJjZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQN1Lg==\n','2012-05-08 13:48:09'),('f2e1a4e60fd4b430482209afbb416725','OGQ0YmVmNTYyNjg2ZDc0YTg0Mzc5ZTdmODMwYjcyYjFkZGRiNmVjYzqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRxAooBA1USX2F1dGhfdXNlcl9iYWNrZW5kcQNVKWRqYW5nby5jb250cmliLmF1dGguYmFj\na2VuZHMuTW9kZWxCYWNrZW5kcQRVDXNhdmVfcXVlc3Rpb25xBUsBdS4=\n','2012-05-10 07:13:09'),('3e82fbdfb0a6c57e5323ea4d207448f0','MDY4YzdmZWE2MGU5MDcyMmEzNTg4ZDI2ZDNkNzgwYThjMDllOTJkNTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2012-05-11 03:46:33'),('7639a72ab2550fda22110960d92dcea6','ODI3YWVkMjEwNmM4YWU2OTkyYTYxZjczMThjNWZjYWRjNmI1MGJjZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQN1Lg==\n','2012-05-09 05:02:01');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_contact`
--

DROP TABLE IF EXISTS `feedback_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_contact`
--

LOCK TABLES `feedback_contact` WRITE;
/*!40000 ALTER TABLE `feedback_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_contactanon`
--

DROP TABLE IF EXISTS `feedback_contactanon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_contactanon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_contactanon`
--

LOCK TABLES `feedback_contactanon` WRITE;
/*!40000 ALTER TABLE `feedback_contactanon` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_contactanon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_image`
--

DROP TABLE IF EXISTS `files_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file` varchar(100) DEFAULT NULL,
  `content_id` int(11) NOT NULL,
  `is_localized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `files_image_cc8ff3c` (`content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_image`
--

LOCK TABLES `files_image` WRITE;
/*!40000 ALTER TABLE `files_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_pdffile`
--

DROP TABLE IF EXISTS `files_pdffile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_pdffile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `is_localized` tinyint(1) NOT NULL,
  `url_path` varchar(200) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `files_pdffile_cc8ff3c` (`content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_pdffile`
--

LOCK TABLES `files_pdffile` WRITE;
/*!40000 ALTER TABLE `files_pdffile` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_pdffile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_video`
--

DROP TABLE IF EXISTS `files_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_path` varchar(200) DEFAULT NULL,
  `url_path_embed` varchar(200) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `is_localized` tinyint(1) NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `points_id` int(11) DEFAULT NULL,
  `has_thumbnails` tinyint(1) NOT NULL,
  `thumbnail_path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_video_7ab48146` (`language_id`),
  KEY `files_video_cc8ff3c` (`content_id`),
  KEY `files_video_4b623205` (`points_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_video`
--

LOCK TABLES `files_video` WRITE;
/*!40000 ALTER TABLE `files_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_answer`
--

DROP TABLE IF EXISTS `practical_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(200) DEFAULT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `question_id` int(11) NOT NULL,
  `image_file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `practical_answer_1f92e550` (`question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_answer`
--

LOCK TABLES `practical_answer` WRITE;
/*!40000 ALTER TABLE `practical_answer` DISABLE KEYS */;
INSERT INTO `practical_answer` VALUES (6,'bye',0,19,''),(5,'hi',1,19,'');
/*!40000 ALTER TABLE `practical_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_exercise`
--

DROP TABLE IF EXISTS `practical_exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_exercise` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `bonus_points_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `practical_exercise_6d25d6e2` (`lesson_id`),
  KEY `practical_exercise_1f37ef1b` (`bonus_points_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_exercise`
--

LOCK TABLES `practical_exercise` WRITE;
/*!40000 ALTER TABLE `practical_exercise` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_exercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_exercise_question`
--

DROP TABLE IF EXISTS `practical_exercise_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_exercise_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m2m_question_id` int(11) NOT NULL,
  `m2m_exercise_id` int(11) NOT NULL,
  `difficulty_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practical_exercise_question_97556c3` (`m2m_question_id`),
  KEY `practical_exercise_question_4c24863b` (`m2m_exercise_id`),
  KEY `practical_exercise_question_269a6dbd` (`difficulty_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_exercise_question`
--

LOCK TABLES `practical_exercise_question` WRITE;
/*!40000 ALTER TABLE `practical_exercise_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_exercise_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_lesson`
--

DROP TABLE IF EXISTS `practical_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_lesson` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `practical_lesson_57732028` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_lesson`
--

LOCK TABLES `practical_lesson` WRITE;
/*!40000 ALTER TABLE `practical_lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_question`
--

DROP TABLE IF EXISTS `practical_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_question` (
  `translateable_ptr_id` int(11) NOT NULL,
  `answer_type_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_required` tinyint(1) NOT NULL,
  `votes` int(11) NOT NULL,
  `points_id` int(11) NOT NULL,
  `submitted_by_id` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `suggest_status` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `practical_question_6a0693de` (`answer_type_id`),
  KEY `practical_question_4b623205` (`points_id`),
  KEY `practical_question_3c12418b` (`submitted_by_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_question`
--

LOCK TABLES `practical_question` WRITE;
/*!40000 ALTER TABLE `practical_question` DISABLE KEYS */;
INSERT INTO `practical_question` VALUES (19,10,'2012-04-24 03:23:27',0,0,3,NULL,1,0);
/*!40000 ALTER TABLE `practical_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_question_voted_accounts`
--

DROP TABLE IF EXISTS `practical_question_voted_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_question_voted_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_id` (`question_id`,`account_id`),
  KEY `practical_question_voted_accounts_1f92e550` (`question_id`),
  KEY `practical_question_voted_accounts_6f2fe10e` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_question_voted_accounts`
--

LOCK TABLES `practical_question_voted_accounts` WRITE;
/*!40000 ALTER TABLE `practical_question_voted_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_question_voted_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration_registrationprofile`
--

DROP TABLE IF EXISTS `registration_registrationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_registrationprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activation_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration_registrationprofile`
--

LOCK TABLES `registration_registrationprofile` WRITE;
/*!40000 ALTER TABLE `registration_registrationprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration_registrationprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_account_lesson`
--

DROP TABLE IF EXISTS `stats_account_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_account_lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `video` tinyint(1) NOT NULL,
  `textbook` tinyint(1) NOT NULL,
  `submitted_questions_num` int(11) NOT NULL,
  `started_date` datetime DEFAULT NULL,
  `finished_date` datetime DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_account_lesson_6f2fe10e` (`account_id`),
  KEY `stats_account_lesson_6d25d6e2` (`lesson_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_account_lesson`
--

LOCK TABLES `stats_account_lesson` WRITE;
/*!40000 ALTER TABLE `stats_account_lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_account_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_account_lesson_question`
--

DROP TABLE IF EXISTS `stats_account_lesson_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_account_lesson_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_lesson_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `is_answered` tinyint(1) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `attempts` int(11) NOT NULL,
  `hint_is_used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_account_lesson_question_516e674a` (`account_lesson_id`),
  KEY `stats_account_lesson_question_1f92e550` (`question_id`),
  KEY `stats_account_lesson_question_2799bae2` (`exercise_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_account_lesson_question`
--

LOCK TABLES `stats_account_lesson_question` WRITE;
/*!40000 ALTER TABLE `stats_account_lesson_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_account_lesson_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_completedexercise`
--

DROP TABLE IF EXISTS `stats_completedexercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_completedexercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_lesson_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `completed_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_completedexercise_516e674a` (`account_lesson_id`),
  KEY `stats_completedexercise_2799bae2` (`exercise_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_completedexercise`
--

LOCK TABLES `stats_completedexercise` WRITE;
/*!40000 ALTER TABLE `stats_completedexercise` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_completedexercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_student_division`
--

DROP TABLE IF EXISTS `student_student_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_student_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `student_id` (`student_id`),
  KEY `student_student_division_7fcb34a3` (`division_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_student_division`
--

LOCK TABLES `student_student_division` WRITE;
/*!40000 ALTER TABLE `student_student_division` DISABLE KEYS */;
INSERT INTO `student_student_division` VALUES (1,2,2),(2,3,1);
/*!40000 ALTER TABLE `student_student_division` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudent`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudent_161e15c3` (`teacher_id`),
  KEY `teacher_student_core_teacherstudent_42ff452e` (`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudent`
--

LOCK TABLES `teacher_student_core_teacherstudent` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudent` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudentcontent`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudentcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudentcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudentcontent_161e15c3` (`teacher_id`),
  KEY `teacher_student_core_teacherstudentcontent_42ff452e` (`student_id`),
  KEY `teacher_student_core_teacherstudentcontent_cc8ff3c` (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudentcontent`
--

LOCK TABLES `teacher_student_core_teacherstudentcontent` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudentcontent` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudentcontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudenthistory`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudenthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudenthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `finished_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudenthistory_42e828ff` (`row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudenthistory`
--

LOCK TABLES `teacher_student_core_teacherstudenthistory` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudenthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudenthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_topic_teacher`
--

DROP TABLE IF EXISTS `teacher_topic_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_topic_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_topic_teacher_161e15c3` (`teacher_id`),
  KEY `teacher_topic_teacher_7fcb34a3` (`division_id`),
  KEY `teacher_topic_teacher_57732028` (`topic_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_topic_teacher`
--

LOCK TABLES `teacher_topic_teacher` WRITE;
/*!40000 ALTER TABLE `teacher_topic_teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_topic_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translateable`
--

DROP TABLE IF EXISTS `translation_translateable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translateable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translateable`
--

LOCK TABLES `translation_translateable` WRITE;
/*!40000 ALTER TABLE `translation_translateable` DISABLE KEYS */;
INSERT INTO `translation_translateable` VALUES (6),(7),(8),(9),(10),(11),(19);
/*!40000 ALTER TABLE `translation_translateable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translation`
--

DROP TABLE IF EXISTS `translation_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `field_name` varchar(30) NOT NULL,
  `text` varchar(12500) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translation_translation_7ab48146` (`language_id`),
  KEY `translation_translation_7d61c803` (`object_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translation`
--

LOCK TABLES `translation_translation` WRITE;
/*!40000 ALTER TABLE `translation_translation` DISABLE KEYS */;
INSERT INTO `translation_translation` VALUES (32,1,'question','hi',19),(33,1,'hint','hi',19),(10,1,'difficulty_text','easy',6),(11,1,'difficulty_text','moderate',7),(12,1,'difficulty_text','hard',8),(13,1,'name','multiple choice',9),(14,1,'description','multiple choice',9),(15,1,'name','single choice',10),(16,1,'description','single choice',10),(17,1,'name','text answer',11),(18,1,'description','text answer',11);
/*!40000 ALTER TABLE `translation_translation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-26 19:48:55
