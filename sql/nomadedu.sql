-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: openuniversity
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'moderator');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM AUTO_INCREMENT=214 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (104,1,2),(103,1,1),(204,1,102),(203,1,101),(202,1,100),(201,1,99),(200,1,98),(199,1,97),(198,1,96),(197,1,95),(196,1,94),(195,1,93),(194,1,92),(193,1,91),(192,1,90),(191,1,89),(190,1,88),(189,1,87),(188,1,86),(187,1,85),(186,1,84),(185,1,83),(184,1,82),(183,1,81),(182,1,80),(181,1,79),(180,1,78),(179,1,77),(178,1,76),(177,1,75),(176,1,74),(175,1,73),(174,1,72),(173,1,71),(172,1,70),(171,1,69),(170,1,68),(169,1,67),(168,1,66),(167,1,65),(166,1,64),(165,1,63),(164,1,62),(163,1,61),(162,1,60),(161,1,59),(160,1,58),(159,1,57),(158,1,56),(157,1,55),(156,1,54),(155,1,53),(154,1,52),(153,1,51),(152,1,50),(151,1,49),(150,1,48),(149,1,47),(148,1,46),(147,1,45),(146,1,44),(145,1,43),(144,1,42),(143,1,41),(142,1,40),(141,1,39),(140,1,38),(139,1,37),(138,1,36),(137,1,35),(136,1,34),(135,1,33),(134,1,32),(133,1,31),(132,1,30),(131,1,29),(130,1,28),(129,1,27),(128,1,26),(127,1,25),(126,1,24),(125,1,23),(124,1,22),(123,1,21),(122,1,20),(121,1,19),(120,1,18),(119,1,17),(118,1,16),(117,1,15),(116,1,14),(115,1,13),(114,1,12),(113,1,11),(112,1,10),(111,1,9),(110,1,8),(109,1,7),(108,1,6),(107,1,5),(106,1,4),(105,1,3),(205,1,103),(206,1,104),(207,1,105),(208,1,106),(209,1,107),(210,1,108),(211,1,109),(212,1,110),(213,1,111);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_403f60f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_message`
--

LOCK TABLES `auth_message` WRITE;
/*!40000 ALTER TABLE `auth_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add message',4,'add_message'),(11,'Can change message',4,'change_message'),(12,'Can delete message',4,'delete_message'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add account',8,'add_account'),(23,'Can change account',8,'change_account'),(24,'Can delete account',8,'delete_account'),(25,'Can add language',9,'add_language'),(26,'Can change language',9,'change_language'),(27,'Can delete language',9,'delete_language'),(28,'Can add age group',10,'add_agegroup'),(29,'Can change age group',10,'change_agegroup'),(30,'Can delete age group',10,'delete_agegroup'),(31,'Can add points',11,'add_points'),(32,'Can change points',11,'change_points'),(33,'Can delete points',11,'delete_points'),(34,'Can add content on off marker',12,'add_contentonoffmarker'),(35,'Can change content on off marker',12,'change_contentonoffmarker'),(36,'Can delete content on off marker',12,'delete_contentonoffmarker'),(37,'Can add translate able',13,'add_translateable'),(38,'Can change translate able',13,'change_translateable'),(39,'Can delete translate able',13,'delete_translateable'),(40,'Can add translation',14,'add_translation'),(41,'Can change translation',14,'change_translation'),(42,'Can delete translation',14,'delete_translation'),(43,'Can add badge',15,'add_badge'),(44,'Can change badge',15,'change_badge'),(45,'Can delete badge',15,'delete_badge'),(46,'Can add sub type',16,'add_subtype'),(47,'Can change sub type',16,'change_subtype'),(48,'Can delete sub type',16,'delete_subtype'),(49,'Can add difficulty',17,'add_difficulty'),(50,'Can change difficulty',17,'change_difficulty'),(51,'Can delete difficulty',17,'delete_difficulty'),(52,'Can add subject',18,'add_subject'),(53,'Can change subject',18,'change_subject'),(54,'Can delete subject',18,'delete_subject'),(55,'Can add topic',19,'add_topic'),(56,'Can change topic',19,'change_topic'),(57,'Can delete topic',19,'delete_topic'),(58,'Can add account_ topic',20,'add_account_topic'),(59,'Can change account_ topic',20,'change_account_topic'),(60,'Can delete account_ topic',20,'delete_account_topic'),(61,'Can add content alias',21,'add_contentalias'),(62,'Can change content alias',21,'change_contentalias'),(63,'Can delete content alias',21,'delete_contentalias'),(64,'Can add video',22,'add_video'),(65,'Can change video',22,'change_video'),(66,'Can delete video',22,'delete_video'),(67,'Can add pdf file',23,'add_pdffile'),(68,'Can change pdf file',23,'change_pdffile'),(69,'Can delete pdf file',23,'delete_pdffile'),(70,'Can add image',24,'add_image'),(71,'Can change image',24,'change_image'),(72,'Can delete image',24,'delete_image'),(73,'Can add lesson',25,'add_lesson'),(74,'Can change lesson',25,'change_lesson'),(75,'Can delete lesson',25,'delete_lesson'),(76,'Can add question',26,'add_question'),(77,'Can change question',26,'change_question'),(78,'Can delete question',26,'delete_question'),(79,'Can add exercise',27,'add_exercise'),(80,'Can change exercise',27,'change_exercise'),(81,'Can delete exercise',27,'delete_exercise'),(82,'Can add exercise_ question',28,'add_exercise_question'),(83,'Can change exercise_ question',28,'change_exercise_question'),(84,'Can delete exercise_ question',28,'delete_exercise_question'),(85,'Can add answer',29,'add_answer'),(86,'Can change answer',29,'change_answer'),(87,'Can delete answer',29,'delete_answer'),(88,'Can add account_ topic_ question',30,'add_account_topic_question'),(89,'Can change account_ topic_ question',30,'change_account_topic_question'),(90,'Can delete account_ topic_ question',30,'delete_account_topic_question'),(91,'Can add course',31,'add_course'),(92,'Can change course',31,'change_course'),(93,'Can delete course',31,'delete_course'),(94,'Can add course contents',32,'add_coursecontents'),(95,'Can change course contents',32,'change_coursecontents'),(96,'Can delete course contents',32,'delete_coursecontents'),(97,'Can add registration profile',33,'add_registrationprofile'),(98,'Can change registration profile',33,'change_registrationprofile'),(99,'Can delete registration profile',33,'delete_registrationprofile'),(100,'Can add teacher student content',34,'add_teacherstudentcontent'),(101,'Can change teacher student content',34,'change_teacherstudentcontent'),(102,'Can delete teacher student content',34,'delete_teacherstudentcontent'),(103,'Can add teacher student',35,'add_teacherstudent'),(104,'Can change teacher student',35,'change_teacherstudent'),(105,'Can delete teacher student',35,'delete_teacherstudent'),(106,'Can add teacher student history',36,'add_teacherstudenthistory'),(107,'Can change teacher student history',36,'change_teacherstudenthistory'),(108,'Can delete teacher student history',36,'delete_teacherstudenthistory'),(109,'Can add log entry',37,'add_logentry'),(110,'Can change log entry',37,'change_logentry'),(111,'Can delete log entry',37,'delete_logentry');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'admin','','','javkhlan.sh@nmma.co','sha1$f7ac0$03c563aeb4f21d07c128693f800a2781d7d04122',1,1,1,'2011-10-03 23:42:26','2011-09-24 12:42:01'),(2,'javkhlan','','','javkhlan.sh@nmma.co','sha1$907ed$ede5a748955bb0a15a3227d4e0301fda21b6ca01',0,1,0,'2011-09-28 14:20:20','2011-09-24 12:43:56'),(3,'brian','','','brian@teachaclass.org','sha1$063a6$f3906e774c58fd0206832778e1cd0eb18b47326c',0,1,0,'2011-10-04 17:40:01','2011-09-24 16:42:49'),(4,'nomad','','','brian@teachaclass.org','sha1$db377$7111e108314c7c72aed41f975159b34102153ed0',0,1,0,'2011-10-03 23:42:59','2011-10-03 23:37:44');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (3,2,1),(4,3,1),(5,4,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_badge`
--

DROP TABLE IF EXISTS `badge_badge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_badge` (
  `translateable_ptr_id` int(11) NOT NULL,
  `points_num` int(11) NOT NULL,
  `exercise_id` int(11) DEFAULT NULL,
  `is_special` tinyint(1) NOT NULL,
  `file` varchar(300) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `badge_badge_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_badge`
--

LOCK TABLES `badge_badge` WRITE;
/*!40000 ALTER TABLE `badge_badge` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_badge_accounts`
--

DROP TABLE IF EXISTS `badge_badge_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_badge_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `badge_id` (`badge_id`,`account_id`),
  KEY `badge_badge_accounts_7f24a4dc` (`badge_id`),
  KEY `badge_badge_accounts_6f2fe10e` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_badge_accounts`
--

LOCK TABLES `badge_badge_accounts` WRITE;
/*!40000 ALTER TABLE `badge_badge_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_badge_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_difficulty`
--

DROP TABLE IF EXISTS `common_difficulty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_difficulty` (
  `translateable_ptr_id` int(11) NOT NULL,
  `difficulty_int` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_difficulty`
--

LOCK TABLES `common_difficulty` WRITE;
/*!40000 ALTER TABLE `common_difficulty` DISABLE KEYS */;
INSERT INTO `common_difficulty` VALUES (4,1),(5,2),(6,3);
/*!40000 ALTER TABLE `common_difficulty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_subtype`
--

DROP TABLE IF EXISTS `common_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_subtype` (
  `translateable_ptr_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `type_int` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `type_int` (`type_int`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_subtype`
--

LOCK TABLES `common_subtype` WRITE;
/*!40000 ALTER TABLE `common_subtype` DISABLE KEYS */;
INSERT INTO `common_subtype` VALUES (1,'practical_answer',1),(2,'practical_answer',2),(3,'practical_answer',3);
/*!40000 ALTER TABLE `common_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_contentalias`
--

DROP TABLE IF EXISTS `contents_core_contentalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_contentalias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `contents_core_contentalias_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_contentalias`
--

LOCK TABLES `contents_core_contentalias` WRITE;
/*!40000 ALTER TABLE `contents_core_contentalias` DISABLE KEYS */;
INSERT INTO `contents_core_contentalias` VALUES (1,'science',18,7),(2,'add',24,9),(3,'Mathmatics',18,10),(4,'Arithmatic',19,11),(5,'Basic-Addition',24,12),(6,'Addition-2',24,13),(7,'level-2-additio',24,14),(8,'addition-3',24,15),(9,'addition-4',24,16),(10,'subtraction-2',24,18),(11,'why-borrowing-w',24,21),(12,'lvl4-subtractio',24,22),(13,'subtracting-dec',24,24),(14,'basic-multiplic',24,25),(15,'Multiplication2',24,26),(16,'Multiplication3',24,27),(17,'Multiplication5',24,29),(18,'Multiplication6',24,30),(19,'Multiplication7',24,31),(20,'lattice-multipl',24,34),(21,'division-1',24,36),(22,'division-2',24,37),(23,'division-3',24,38),(24,'Lvl4-division',24,39),(25,'cnv-frac-to-dec',24,40),(26,'percent-decimal',24,41),(27,'dividing-decima',24,42),(28,'Ord-num-exp',24,43),(29,'grtst-cmn-divis',24,44),(30,'lst-cmn-multipl',24,45),(31,'equivalent-frac',24,46),(32,'mx-num-imp-fra',24,47),(33,'algebra',19,48),(34,'simple-equation',24,49),(35,'equations2',24,50),(36,'equations-3',24,51),(37,'develop-math',19,52),(38,'adding-decimals',24,53),(39,'add-frac-w-like',24,54),(40,'add-frac-unlk-d',24,55),(41,'addwhlenumapp1',24,56),(56,'Alg-long-dev',24,75),(43,'test2',24,58),(45,'dfsdfdsf',19,61),(46,'fgsdfgsf',24,62),(50,'new-subject',18,66),(48,'new-topic',18,64),(49,'lvl-4-nwtopc',24,65),(52,'example-topic',19,68),(53,'example-lesson',24,69),(55,'computers',18,72),(57,'physics',19,76),(58,'convex-lenses',24,77),(59,'place-values-2',24,78),(60,'place-values',24,79),(61,'place-values-3',24,80),(62,'dec-plc-value',24,85),(63,'dec-plc-value2',24,86),(64,'Multi-dec3',24,87),(65,'rnd-whole-numb',24,88),(66,'rnd-whole-numb2',24,89),(67,'div-m-num-frac',24,90),(68,'rnd-est-diff',24,91),(69,'biology',19,93),(70,'photosynthesis',24,94),(71,'osteology',19,95),(72,'bone-hum-body',24,96),(73,'nome-bones',26,97);
/*!40000 ALTER TABLE `contents_core_contentalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_subject`
--

DROP TABLE IF EXISTS `contents_core_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_subject` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_subject`
--

LOCK TABLES `contents_core_subject` WRITE;
/*!40000 ALTER TABLE `contents_core_subject` DISABLE KEYS */;
INSERT INTO `contents_core_subject` VALUES (7,7),(10,10),(99,99),(107,107),(72,72);
/*!40000 ALTER TABLE `contents_core_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_topic`
--

DROP TABLE IF EXISTS `contents_core_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_topic` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `age_group_id` int(11) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `contents_core_topic_386a7d9c` (`age_group_id`),
  KEY `contents_core_topic_638462f1` (`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_topic`
--

LOCK TABLES `contents_core_topic` WRITE;
/*!40000 ALTER TABLE `contents_core_topic` DISABLE KEYS */;
INSERT INTO `contents_core_topic` VALUES (48,48,'2011-09-25 17:13:43',2,10),(11,11,'2011-10-04 15:28:03',1,10),(52,52,'2011-09-25 17:19:47',2,10),(76,76,'2011-10-04 00:17:38',3,7),(93,93,'2011-10-04 00:49:20',3,7),(95,95,'2011-10-04 01:05:56',3,7),(103,103,'2011-10-04 16:04:27',1,99),(100,100,'2011-10-04 15:59:11',1,99),(108,108,'2011-10-04 16:09:05',1,107),(112,112,'2011-10-04 16:24:35',3,107),(115,115,'2011-10-04 16:29:35',3,107);
/*!40000 ALTER TABLE `contents_core_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_account`
--

DROP TABLE IF EXISTS `core_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_account`
--

LOCK TABLES `core_account` WRITE;
/*!40000 ALTER TABLE `core_account` DISABLE KEYS */;
INSERT INTO `core_account` VALUES (1,2,'1990-01-01','Mongolia','0',1),(2,3,'1990-01-01','Mongolia','0',1),(3,4,'1990-01-01','Mongolia','0',1);
/*!40000 ALTER TABLE `core_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_agegroup`
--

DROP TABLE IF EXISTS `core_agegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_agegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_agegroup`
--

LOCK TABLES `core_agegroup` WRITE;
/*!40000 ALTER TABLE `core_agegroup` DISABLE KEYS */;
INSERT INTO `core_agegroup` VALUES (1,'1'),(2,'2'),(3,'3');
/*!40000 ALTER TABLE `core_agegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_contentonoffmarker`
--

DROP TABLE IF EXISTS `core_contentonoffmarker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_contentonoffmarker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offline` tinyint(1) NOT NULL,
  `unique` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_contentonoffmarker`
--

LOCK TABLES `core_contentonoffmarker` WRITE;
/*!40000 ALTER TABLE `core_contentonoffmarker` DISABLE KEYS */;
INSERT INTO `core_contentonoffmarker` VALUES (7,0,'2011-09-24 12:50:02.693287'),(14,0,'2011-09-25 10:40:34.026253'),(13,0,'2011-09-25 10:38:20.206827'),(10,0,'2011-09-24 16:45:08.768453'),(11,0,'2011-09-25 00:22:21.805058'),(12,0,'2011-09-25 00:28:06.259623'),(15,0,'2011-09-25 10:42:26.711130'),(16,0,'2011-09-25 10:43:51.762897'),(17,0,'2011-09-25 10:45:35.397039'),(18,0,'2011-09-25 10:52:49.806580'),(19,0,'2011-09-25 10:54:47.620276'),(20,0,'2011-09-25 11:02:21.655836'),(21,0,'2011-09-25 11:03:26.625127'),(22,0,'2011-09-25 11:05:07.521653'),(23,0,'2011-09-25 11:08:09.824071'),(24,0,'2011-09-25 11:10:30.107391'),(25,0,'2011-09-25 11:11:51.245885'),(26,0,'2011-09-25 11:13:28.379110'),(27,0,'2011-09-25 11:14:38.975337'),(28,0,'2011-09-25 11:15:46.763057'),(29,0,'2011-09-25 11:16:53.430249'),(30,0,'2011-09-25 11:18:00.201864'),(31,0,'2011-09-25 11:19:36.520586'),(32,0,'2011-09-25 11:21:42.505798'),(34,0,'2011-09-25 13:43:13.254572'),(35,0,'2011-09-25 13:44:22.787817'),(36,0,'2011-09-25 13:45:52.690294'),(37,0,'2011-09-25 13:49:50.659285'),(38,0,'2011-09-25 13:51:57.342937'),(39,0,'2011-09-25 17:01:02.007397'),(40,0,'2011-09-25 17:02:56.215943'),(41,0,'2011-09-25 17:04:10.760253'),(42,0,'2011-09-25 17:05:19.955094'),(43,0,'2011-09-25 17:06:43.241778'),(44,0,'2011-09-25 17:08:05.110674'),(45,0,'2011-09-25 17:09:38.710164'),(46,0,'2011-09-25 17:11:07.807146'),(47,0,'2011-09-25 17:12:13.493156'),(48,0,'2011-09-25 17:13:43.857780'),(49,0,'2011-09-25 17:15:08.351087'),(50,0,'2011-09-25 17:16:58.770631'),(51,0,'2011-09-25 17:18:37.765596'),(52,0,'2011-09-25 17:19:47.024124'),(53,0,'2011-09-25 17:21:44.384136'),(54,0,'2011-09-25 17:24:33.449808'),(55,0,'2011-09-25 17:45:31.678956'),(56,0,'2011-09-25 17:47:13.337440'),(78,0,'2011-10-04 00:27:26.064441'),(86,0,'2011-10-04 00:38:49.604506'),(79,0,'2011-10-04 00:28:18.495235'),(76,0,'2011-10-04 00:17:38.933884'),(77,0,'2011-10-04 00:19:05.293847'),(75,0,'2011-10-04 00:10:54.079796'),(104,0,'2011-10-04 16:05:18.091271'),(72,0,'2011-10-03 23:37:08.704322'),(80,0,'2011-10-04 00:29:03.747943'),(81,0,'2011-10-04 00:31:32.003798'),(82,0,'2011-10-04 00:32:48.438631'),(83,0,'2011-10-04 00:33:42.660247'),(84,0,'2011-10-04 00:34:15.885242'),(85,0,'2011-10-04 00:36:47.552933'),(87,0,'2011-10-04 00:40:43.580848'),(88,0,'2011-10-04 00:42:21.167127'),(89,0,'2011-10-04 00:43:01.567922'),(90,0,'2011-10-04 00:44:12.985736'),(91,0,'2011-10-04 00:45:33.169900'),(92,0,'2011-10-04 00:46:57.846959'),(93,0,'2011-10-04 00:49:20.504210'),(94,0,'2011-10-04 01:02:24.359869'),(95,0,'2011-10-04 01:05:56.745550'),(96,0,'2011-10-04 01:06:49.612801'),(97,0,'2011-10-04 01:16:36.513099'),(103,0,'2011-10-04 16:04:27.306823'),(102,0,'2011-10-04 16:02:18.226158'),(99,0,'2011-10-04 15:58:10.271712'),(105,0,'2011-10-04 16:06:24.778916'),(106,0,'2011-10-04 16:07:40.149815'),(107,0,'2011-10-04 16:08:03.472833'),(108,0,'2011-10-04 16:09:05.464271'),(109,0,'2011-10-04 16:17:32.742316'),(110,0,'2011-10-04 16:20:33.916111'),(111,0,'2011-10-04 16:22:31.408095'),(112,0,'2011-10-04 16:24:35.074942'),(113,0,'2011-10-04 16:25:19.538866'),(114,0,'2011-10-04 16:27:01.537304'),(115,0,'2011-10-04 16:29:35.757415');
/*!40000 ALTER TABLE `core_contentonoffmarker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_language`
--

DROP TABLE IF EXISTS `core_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(50) NOT NULL,
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_language`
--

LOCK TABLES `core_language` WRITE;
/*!40000 ALTER TABLE `core_language` DISABLE KEYS */;
INSERT INTO `core_language` VALUES (1,'English','en-us'),(2,'Монгол','mn'),(3,'Русский','ru');
/*!40000 ALTER TABLE `core_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_points`
--

DROP TABLE IF EXISTS `core_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_points`
--

LOCK TABLES `core_points` WRITE;
/*!40000 ALTER TABLE `core_points` DISABLE KEYS */;
INSERT INTO `core_points` VALUES (1,1),(2,2),(3,3);
/*!40000 ALTER TABLE `core_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_course`
--

DROP TABLE IF EXISTS `courses_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_course` (
  `translateable_ptr_id` int(11) NOT NULL,
  `created_account_id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `edited_date` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `courses_course_418bb3` (`created_account_id`),
  KEY `courses_course_279e212f` (`course_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_course`
--

LOCK TABLES `courses_course` WRITE;
/*!40000 ALTER TABLE `courses_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_coursecontents`
--

DROP TABLE IF EXISTS `courses_coursecontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_coursecontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `added_account_id` int(11) DEFAULT NULL,
  `content_level_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_coursecontents_b7271b` (`course_id`),
  KEY `courses_coursecontents_cc8ff3c` (`content_id`),
  KEY `courses_coursecontents_370986a3` (`added_account_id`),
  KEY `courses_coursecontents_7aaf98d1` (`content_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_coursecontents`
--

LOCK TABLES `courses_coursecontents` WRITE;
/*!40000 ALTER TABLE `courses_coursecontents` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_coursecontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2011-09-24 12:43:17',1,2,'1','moderator',1,''),(2,'2011-09-24 12:44:37',1,3,'2','javkhlan',2,'Changed groups.'),(3,'2011-09-24 12:45:10',1,11,'1','en-us -- English',1,''),(4,'2011-09-24 12:45:21',1,11,'2','mn -- Монгол',1,''),(5,'2011-09-24 12:45:32',1,11,'3','ru -- Русский',1,''),(6,'2011-09-24 12:47:16',1,15,'1','text answer',1,''),(7,'2011-09-24 12:47:32',1,15,'2','single choice',1,''),(8,'2011-09-24 12:47:50',1,15,'3','multiple choice',1,''),(9,'2011-09-24 12:48:08',1,13,'1','1',1,''),(10,'2011-09-24 12:48:12',1,13,'2','2',1,''),(11,'2011-09-24 12:48:17',1,13,'3','3',1,''),(12,'2011-09-24 12:48:43',1,25,'4','easy',1,''),(13,'2011-09-24 12:48:52',1,25,'5','moderate',1,''),(14,'2011-09-24 12:48:58',1,25,'6','hard',1,''),(15,'2011-09-24 12:49:18',1,12,'1','AgeGroup: 1',1,''),(16,'2011-09-24 12:49:21',1,12,'2','AgeGroup: 2',1,''),(17,'2011-09-24 12:49:25',1,12,'3','AgeGroup: 3',1,''),(18,'2011-09-24 16:43:35',1,3,'3','brian',2,'Changed groups.'),(19,'2011-09-28 14:20:05',1,3,'2','javkhlan',2,'No fields changed.'),(20,'2011-10-03 23:35:47',1,3,'3','brian',2,'No fields changed.'),(21,'2011-10-03 23:36:14',1,2,'1','moderator',2,'Changed permissions.'),(22,'2011-10-03 23:38:03',1,3,'4','nomad',2,'Changed groups.'),(23,'2011-10-03 23:40:37',1,18,'67','example subject',3,''),(24,'2011-10-03 23:40:56',1,18,'66','New subject',3,''),(25,'2011-10-03 23:40:56',1,18,'60','test',3,''),(26,'2011-10-03 23:42:32',1,19,'57','test',3,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES 
(1,'permission','auth','permission'),
(2,'group','auth','group'),
(3,'user','auth','user'),
(4,'message','auth','message'),
(5,'content type','contenttypes','contenttype'),
(6,'session','sessions','session'),
(7,'site','sites','site'),
(8,'account','core','account'),
(9,'language','core','language'),
(10,'age group','core','agegroup'),
(11,'points','core','points'),
(12,'content on off marker','core','contentonoffmarker'),
(13,'translate able','translation','translateable'),
(14,'translation','translation','translation'),
(15,'badge','badge','badge'),
(16,'sub type','common','subtype'),
(17,'difficulty','common','difficulty'),
(18,'subject','contents_core','subject'),
(19,'topic','contents_core','topic'),
(20,'content alias','contents_core','contentalias'),
(21,'video','files','video'),
(22,'pdf file','files','pdffile'),
(23,'image','files','image'),
(24,'lesson','practical','lesson'),
(25,'question','practical','question'),
(26,'exercise','practical','exercise'),
(27,'exercise_ question','practical','exercise_question'),
(28,'answer','practical','answer'),
(29,'course','courses','course'),
(30,'course contents','courses','coursecontents'),
(31,'account_ lesson','stats','account_lesson'),
(32,'account_ lesson_ question','stats','account_lesson_question'),
(33,'completed exercise','stats','completedexercise'),
(34,'registration profile','registration','registrationprofile'),
(35,'teacher student content','teacher_student_core','teacherstudentcontent'),
(36,'teacher student','teacher_student_core','teacherstudent'),
(37,'teacher student history','teacher_student_core','teacherstudenthistory'),
(38,'log entry','admin','logentry');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1497b235f85804bc1779543e2f42148d','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-08 15:00:53'),('b1ab7b074c7147e81eb6c0427a49b1f0','ODI3YWVkMjEwNmM4YWU2OTkyYTYxZjczMThjNWZjYWRjNmI1MGJjZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQN1Lg==\n','2011-10-11 19:42:44'),('d94a4a45bc248f4eeef580be06df4354','NzI0OTU5MzJlNmZiODJiN2E3ZjZhNmQ0MDRhMGZmNjE3NGQ3MjRkMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQJ1Lg==\n','2011-10-08 12:49:40'),('a53d23591e0b8140c88d59d96a1db0a0','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-12 15:42:57'),('f8baf1fe14c3f23b31cc4d4ddc6e8cd4','ODI3YWVkMjEwNmM4YWU2OTkyYTYxZjczMThjNWZjYWRjNmI1MGJjZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQN1Lg==\n','2011-10-08 16:44:20'),('c912785e822a4f870978c9a08bc55201','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-13 18:00:58'),('20e02d23ce2024d18e3a1a2ed4538dfb','NzI0OTU5MzJlNmZiODJiN2E3ZjZhNmQ0MDRhMGZmNjE3NGQ3MjRkMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQJ1Lg==\n','2011-10-09 19:09:24'),('a1fd28d4750a4faa66786299fdcd450e','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-09 21:27:06'),('e6ae7462c7c6b8217fec1d27ea5bf718','NzI0OTU5MzJlNmZiODJiN2E3ZjZhNmQ0MDRhMGZmNjE3NGQ3MjRkMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQJ1Lg==\n','2011-10-10 16:17:21'),('200301027caeac3dd756be8493dc6043','NzI0OTU5MzJlNmZiODJiN2E3ZjZhNmQ0MDRhMGZmNjE3NGQ3MjRkMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQJ1Lg==\n','2011-10-12 14:20:20'),('a59d3d6263383ec8da69165a091283a3','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-17 21:59:34'),('734d9f35819674e2d2f2e75d027305e7','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-17 21:59:31'),('4d151256829feaf1e87781f114c60629','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-17 21:59:00'),('ff810e928d175ca2ea63376042b777f1','YjU0ZjdkZTU4YmEwMjNhMjlhMjM2NzdjMzQ5YzliZmQ3MTIyOTAzODqAAn1xAShVD2RqYW5nb19s\nYW5ndWFnZVgFAAAAZW4tdXNVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0\naC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVDV9hdXRoX3VzZXJfaWSKAQRVDXNhdmVfcXVlc3Rpb25L\nAXUu\n','2011-10-18 01:46:04'),('4652d9562be3cf0d56898716dc08c3a0','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-18 10:48:21'),('1b4681dc836c5f3b4656809bb4fe10d6','MjlkOWMwNTA0MDgzMTgyNzgwODVhMjhlZjEyMzVhNGNkODdjNmQ2NTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVDV9h\ndXRoX3VzZXJfaWSKAQNVD2RqYW5nb19sYW5ndWFnZVgFAAAAZW4tdXNxAnUu\n','2011-10-18 16:15:27'),('ee4502009f45a183ae82ffcfbc7f7a68','ZWJmNjQ3MjgwNzExZWRiZjQwMjEzNGM1OTAxY2FjMzg0YmRiZTJhZjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlWAUAAABlbi11c3ECcy4=\n','2011-10-18 17:30:40'),('ac49b0e47850a961faf8132c849a78fe','ZWJmNjQ3MjgwNzExZWRiZjQwMjEzNGM1OTAxY2FjMzg0YmRiZTJhZjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlWAUAAABlbi11c3ECcy4=\n','2011-10-18 15:20:10'),('aaa6bbb42070d163aba84f1efbb18a02','ZWJmNjQ3MjgwNzExZWRiZjQwMjEzNGM1OTAxY2FjMzg0YmRiZTJhZjqAAn1xAVUPZGphbmdvX2xh\nbmd1YWdlWAUAAABlbi11c3ECcy4=\n','2011-10-18 16:02:42'),('e28f2fdb559dd86d6675f39d45bc767e','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-18 15:57:17'),('203f0887afedf8e65711d30b6b7061ec','MjAwOGY2YzM3ODdmZjk0YmM5NmIwNzAxYmI2MTUwZjk1MzMwZmQ4MjqAAn1xAS4=\n','2011-10-18 15:58:15'),('6a50915c4fa306bf3504488f03b8b2b1','NWRkNGY1MWU1YWY4NjYwYzUyYzYwOWUwYzI4ZTJjNzM4MWQ5NzNiZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n','2011-10-18 16:08:14'),('ef1e29a972a97a432baeffbaf3381f2d','ODI3YWVkMjEwNmM4YWU2OTkyYTYxZjczMThjNWZjYWRjNmI1MGJjZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQN1Lg==\n','2011-10-18 17:40:01');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_image`
--

DROP TABLE IF EXISTS `files_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file` varchar(100) DEFAULT NULL,
  `content_id` int(11) NOT NULL,
  `is_localized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `files_image_cc8ff3c` (`content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_image`
--

LOCK TABLES `files_image` WRITE;
/*!40000 ALTER TABLE `files_image` DISABLE KEYS */;
INSERT INTO `files_image` VALUES (1,'content/science/osteology/bone-hum-body/img/skeleton.jpg',96,1),(2,'content/science/osteology/bone-hum-body/img/humerus.png',96,1),(3,'content/science/osteology/bone-hum-body/img/tibia.1.jpg',96,1),(4,'content/science/osteology/bone-hum-body/img/8915.jpg',96,1),(5,'content/science/osteology/bone-hum-body/img/skapula.jpg',96,1),(6,'content/science/osteology/bone-hum-body/img/femur.jpg',96,1);
/*!40000 ALTER TABLE `files_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_pdffile`
--

DROP TABLE IF EXISTS `files_pdffile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_pdffile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) DEFAULT NULL,
  `is_localized` tinyint(1) NOT NULL,
  `url_path` varchar(200) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_pdffile_cc8ff3c` (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_pdffile`
--

LOCK TABLES `files_pdffile` WRITE;
/*!40000 ALTER TABLE `files_pdffile` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_pdffile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_video`
--

DROP TABLE IF EXISTS `files_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_path` varchar(200) DEFAULT NULL,
  `url_path_embed` varchar(200) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `is_localized` tinyint(1) NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `points_id` int(11) DEFAULT NULL,
  `has_thumbnails` tinyint(1) NOT NULL,
  `thumbnail_path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_video_7ab48146` (`language_id`),
  KEY `files_video_cc8ff3c` (`content_id`),
  KEY `files_video_4b623205` (`points_id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_video`
--

LOCK TABLES `files_video` WRITE;
/*!40000 ALTER TABLE `files_video` DISABLE KEYS */;
INSERT INTO `files_video` VALUES (4,'http://www.youtube.com/watch?v=t2L3JFOqTEk','','','',0,1,'2011-09-25 10:38:20',13,1),(2,'http://www.youtube.com/watch?v=AuX7nPBqDts','','','',0,1,'2011-09-25 00:28:06',12,1),(3,'','','content/Mathmatics/Arithmatic/Basic-Addition/vdo/Basic Addition - Mongolian.flv','',1,2,'2011-10-04 00:23:43',12,1),(5,'http://www.youtube.com/watch?v=27Kp7HJYj2c','','','',0,1,'2011-09-25 10:40:34',14,1),(6,'http://www.youtube.com/watch?v=e_SpXIw_Qts','','','',0,1,'2011-09-25 10:42:26',15,1),(7,'http://www.youtube.com/watch?v=fOXo4p4WDKM','','','',0,1,'2011-09-25 10:43:51',16,1),(8,'http://www.youtube.com/watch?v=aNqG4ChKShI','','','',0,1,'2011-09-25 10:45:35',17,1),(9,'http://www.youtube.com/watch?v=incKJchBCLo','','','',0,1,'2011-09-25 10:52:49',18,1),(10,'http://www.youtube.com/watch?v=ZaqOUE3H1mE','','','',0,1,'2011-09-25 10:54:47',19,1),(11,'http://www.youtube.com/watch?v=GBtcGO44e-A','','','',0,1,'2011-09-25 11:02:21',20,1),(12,'http://www.youtube.com/watch?v=fWan_T0enj4','','','',0,1,'2011-09-25 11:03:26',21,1),(13,'http://www.youtube.com/watch?v=omUfrXtHtN0','','','',0,1,'2011-09-25 11:05:07',22,1),(14,'http://www.youtube.com/watch?v=w616LEmrHVE','','','',0,1,'2011-09-25 11:08:09',23,1),(16,'http://www.youtube.com/watch?v=Eq4mVCd-yyo','','','',0,1,'2011-09-25 11:10:30',24,1),(17,'http://www.youtube.com/watch?v=mvOkMYCygps','','','',0,1,'2011-09-25 11:11:51',25,1),(18,'http://www.youtube.com/watch?v=xO_1bYgoQvA','','','',0,1,'2011-09-25 11:13:28',26,1),(19,'http://www.youtube.com/watch?v=qihoczo1Ujk','','','',0,1,'2011-09-25 11:14:38',27,1),(20,'http://www.youtube.com/watch?v=OYYYc7ygd38','','','',0,1,'2011-09-25 11:15:46',28,1),(21,'http://www.youtube.com/watch?v=t8m0NalQtEk','','','',0,1,'2011-09-25 11:16:53',29,1),(22,'http://www.youtube.com/watch?v=-h3Oqhl8fPg','','','',0,1,'2011-09-25 11:18:00',30,1),(23,'http://www.youtube.com/watch?v=_k3aWF6_b4w','','','',0,1,'2011-09-25 11:19:36',31,1),(24,'http://www.youtube.com/watch?v=m5z6pOsxF_8','','','',0,1,'2011-09-25 11:21:42',32,1),(26,'http://www.youtube.com/watch?v=gS6TfWUv97I','','','',0,1,'2011-09-25 13:43:13',34,1),(27,'http://www.youtube.com/watch?v=S3z4XqC_YSc','','','',0,1,'2011-09-25 13:44:22',35,1),(28,'http://www.youtube.com/watch?v=MTzTqvzWzm8','','','',0,1,'2011-09-25 13:45:52',36,1),(29,'http://www.youtube.com/watch?v=8Ft5iHhauJ0','','','',0,1,'2011-09-25 13:49:50',37,1),(30,'http://www.youtube.com/watch?v=NcADzGz3bSI','','','',0,1,'2011-09-25 13:51:57',38,1),(31,'http://www.youtube.com/watch?v=gHTH6PKfpMc','','','',0,1,'2011-09-25 17:01:02',39,1),(32,'http://www.youtube.com/watch?v=Gn2pdkvdbGQ','','','',0,1,'2011-09-25 17:02:56',40,1),(33,'http://www.youtube.com/watch?v=RvtdJnYFNhc','','','',0,1,'2011-09-25 17:04:10',41,1),(34,'http://www.youtube.com/watch?v=S0uuK7SQcA8','','','',0,1,'2011-09-25 17:05:19',42,1),(35,'http://www.youtube.com/watch?v=Llt-KkHugRQ','','','',0,1,'2011-09-25 17:06:43',43,1),(36,'http://www.youtube.com/watch?v=jFd-6EPfnec','','','',0,1,'2011-09-25 17:08:05',44,1),(37,'http://www.youtube.com/watch?v=cH-jaMCzIRk','','','',0,1,'2011-09-25 17:09:38',45,1),(38,'http://www.youtube.com/watch?v=U2ovEuEUxXQ','','','',0,1,'2011-09-25 17:11:07',46,1),(39,'http://www.youtube.com/watch?v=1xuf6ZKF1_I','','','',0,1,'2011-09-25 17:12:13',47,1),(40,'http://www.youtube.com/watch?v=9Ek61w1LxSc','','','',0,1,'2011-09-25 17:15:08',49,1),(41,'','','content/Mathmatics/algebra/simple-equation/vdo/Simple equations in Mongolian.flv','',1,2,'2011-10-04 00:24:36',49,1),(42,'http://www.youtube.com/watch?v=XoEn1LfVoTo','','','',0,1,'2011-09-25 17:16:58',50,1),(43,'','','content/Mathmatics/algebra/equations2/vdo/Equation-2 Mongolian.flv','',1,2,'2011-10-04 00:35:25',50,1),(44,'http://www.youtube.com/watch?v=f15zA0PhSek','','','',0,1,'2011-09-25 17:18:37',51,1),(45,'http://www.youtube.com/watch?v=w616LEmrHVE','','','',0,1,'2011-09-25 17:21:44',53,1),(46,'http://www.youtube.com/watch?v=dBpEwq5g0Es','','','',0,2,'2011-09-25 17:21:44',53,1),(47,'http://www.youtube.com/watch?v=EJjnEau6aeI','','','',0,1,'2011-09-25 17:24:33',54,1),(48,'http://www.youtube.com/watch?v=OVMceVL_CEQ','','','',0,1,'2011-09-25 17:45:31',55,1),(49,'http://www.youtube.com/watch?v=b22tMEc6Kko','','','',0,1,'2011-09-25 17:47:13',56,1),(57,'','','content/Mathmatics/develop-math/place-values-2/vdo/Place values 2 in Mongolian.flv','',1,2,'2011-10-04 00:27:26',78,1),(53,'','','content/Mathmatics/algebra/Alg-long-dev/vdo/Algebraic Long Division.flv','',1,1,'2011-10-04 00:10:54',75,1),(54,'','','content/Mathmatics/algebra/Alg-long-dev/vdo/Algebraic Long Division_2.flv','content/Mathmatics/algebra/Alg-long-dev/vdo/Algebraic_Long_Division.mn_1.srt',1,2,'2011-10-04 00:53:44',75,1),(55,'','','content/science/physics/convex-lenses/vdo/convex lenses Lesson in Mongolian.flv','',1,2,'2011-10-04 00:19:05',77,1),(56,'','','content/Mathmatics/Arithmatic/23/vdo/Adding decimals - Mongolian.flv','',1,2,'2011-10-04 00:20:42',23,1),(58,'','','content/Mathmatics/develop-math/place-values/vdo/Place values in Mongolian.flv','',1,2,'2011-10-04 00:28:18',79,1),(59,'','','content/Mathmatics/develop-math/place-values-3/vdo/Place values 3 in Mongolian.flv','',1,2,'2011-10-04 00:29:03',80,1),(60,'','','content/Mathmatics/develop-math/81/vdo/multiplying whole number and application 5.flv','',1,2,'2011-10-04 00:31:32',81,1),(61,'','','content/Mathmatics/develop-math/82/vdo/multiplying-whole-numbers-and-applications-4.flv','',1,2,'2011-10-04 00:32:48',82,1),(62,'','','content/Mathmatics/develop-math/83/vdo/Multiplying Whole Numbers and Applications 2.flv','',1,2,'2011-10-04 00:33:42',83,1),(63,'','','content/Mathmatics/develop-math/84/vdo/Multiplying Whole Numbers and Applications 1.flv','',1,2,'2011-10-04 00:34:15',84,1),(64,'','','content/Mathmatics/develop-math/dec-plc-value/vdo/Decimal Place Value in Mongolian.flv','',1,2,'2011-10-04 00:36:47',85,1),(65,'','','content/Mathmatics/develop-math/dec-plc-value2/vdo/Decimal Place Value 2 Mongolian.flv','',1,2,'2011-10-04 00:38:49',86,1),(66,'','','content/Mathmatics/develop-math/Multi-dec3/vdo/multiplying desimals 3.flv','',1,2,'2011-10-04 00:40:43',87,1),(67,'','','content/Mathmatics/develop-math/rnd-whole-numb/vdo/Rounding Whole numbers.flv','',1,2,'2011-10-04 00:42:21',88,1),(68,'','','content/Mathmatics/develop-math/rnd-whole-numb2/vdo/Rounding Whole numbers 2.flv','',1,2,'2011-10-04 00:43:01',89,1),(69,'','','content/Mathmatics/develop-math/div-m-num-frac/vdo/dividing mixed numbers and fractions.flv','',1,2,'2011-10-04 00:44:13',90,1),(70,'','','content/Mathmatics/develop-math/rnd-est-diff/vdo/Rounding to Estimate Differences.flv','',1,2,'2011-10-04 00:45:33',91,1),(71,'','','content/Mathmatics/develop-math/92/vdo/Adding Whole Numbers and Applications 2.flv','',1,2,'2011-10-04 00:46:57',92,1),(72,'','','content/science/biology/photosynthesis/vdo/Photosynthesis.flv','content/science/biology/photosynthesis/vdo/Photosynthesis.mn.srt',1,2,'2011-10-04 01:02:24',94,1),(73,'http://youtu.be/Thi_Lza5KlE','','','',0,1,'2011-10-04 15:37:44',102,3),(76,'http://youtu.be/-EGuJIHRIt0','','','',0,1,'2011-10-04 16:05:18',104,1),(75,'http://youtu.be/-EGuJIHRIt0','','','',0,1,'2011-10-04 16:02:18',102,1),(77,'http://youtu.be/Thi_Lza5KlE','','','',0,1,'2011-10-04 16:06:24',105,1),(78,'http://youtu.be/nHQ8Zt6Hsi4','','','',0,1,'2011-10-04 16:07:40',106,1),(79,'http://youtu.be/lpoFG_YL0q8','','','',0,1,'2011-10-04 16:17:32',109,1),(80,'http://youtu.be/6A9_lVlNYEo','','','',0,1,'2011-10-04 16:20:33',110,1),(81,'http://youtu.be/Q9eWin3lr4Q','','','',0,1,'2011-10-04 16:25:19',113,2),(82,'http://youtu.be/TKjxpStUdDY','','','',0,1,'2011-10-04 16:27:01',114,2);
/*!40000 ALTER TABLE `files_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_answer`
--

DROP TABLE IF EXISTS `practical_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(200) DEFAULT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `question_id` int(11) NOT NULL,
  `image_file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `practical_answer_1f92e550` (`question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_answer`
--

LOCK TABLES `practical_answer` WRITE;
/*!40000 ALTER TABLE `practical_answer` DISABLE KEYS */;
INSERT INTO `practical_answer` VALUES (1,'21',0,71,''),(2,'',0,98,'content/science/osteology/bone-hum-body/exercises/nome-bones/98/answers/skapula.jpg'),(3,'',0,98,'content/science/osteology/bone-hum-body/exercises/nome-bones/98/answers/tibia.1.jpg'),(4,'',1,98,'content/science/osteology/bone-hum-body/exercises/nome-bones/98/answers/femur.jpg'),(5,'',0,98,'content/science/osteology/bone-hum-body/exercises/nome-bones/98/answers/humerus.png');
/*!40000 ALTER TABLE `practical_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_exercise`
--

DROP TABLE IF EXISTS `practical_exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_exercise` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `bonus_points_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `practical_exercise_6d25d6e2` (`lesson_id`),
  KEY `practical_exercise_1f37ef1b` (`bonus_points_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_exercise`
--

LOCK TABLES `practical_exercise` WRITE;
/*!40000 ALTER TABLE `practical_exercise` DISABLE KEYS */;
INSERT INTO `practical_exercise` VALUES (97,97,'2011-10-04 01:16:36',96,1);
/*!40000 ALTER TABLE `practical_exercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_exercise_question`
--

DROP TABLE IF EXISTS `practical_exercise_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_exercise_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m2m_question_id` int(11) NOT NULL,
  `m2m_exercise_id` int(11) NOT NULL,
  `difficulty_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practical_exercise_question_97556c3` (`m2m_question_id`),
  KEY `practical_exercise_question_4c24863b` (`m2m_exercise_id`),
  KEY `practical_exercise_question_269a6dbd` (`difficulty_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_exercise_question`
--

LOCK TABLES `practical_exercise_question` WRITE;
/*!40000 ALTER TABLE `practical_exercise_question` DISABLE KEYS */;
INSERT INTO `practical_exercise_question` VALUES (2,98,97,4);
/*!40000 ALTER TABLE `practical_exercise_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_lesson`
--

DROP TABLE IF EXISTS `practical_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_lesson` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `practical_lesson_57732028` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_lesson`
--

LOCK TABLES `practical_lesson` WRITE;
/*!40000 ALTER TABLE `practical_lesson` DISABLE KEYS */;
INSERT INTO `practical_lesson` VALUES (13,13,11),(12,12,11),(14,14,11),(15,15,11),(16,16,11),(17,17,11),(18,18,11),(19,19,11),(20,20,11),(21,21,11),(22,22,11),(23,23,11),(24,24,11),(25,25,11),(26,26,11),(27,27,11),(28,28,11),(29,29,11),(30,30,11),(31,31,11),(32,32,11),(34,34,11),(35,35,11),(36,36,11),(37,37,11),(38,38,11),(39,39,11),(40,40,11),(41,41,11),(42,42,11),(43,43,11),(44,44,11),(45,45,11),(46,46,11),(47,47,11),(49,49,48),(50,50,48),(51,51,48),(53,53,52),(54,54,52),(55,55,52),(56,56,52),(86,86,52),(78,78,52),(75,75,48),(77,77,76),(79,79,52),(80,80,52),(81,81,52),(82,82,52),(83,83,52),(84,84,52),(85,85,52),(87,87,52),(88,88,52),(89,89,52),(90,90,52),(91,91,52),(92,92,52),(94,94,93),(96,96,95),(102,102,100),(101,101,100),(104,104,103),(105,105,103),(106,106,103),(109,109,103),(110,110,103),(111,111,103),(113,113,112),(114,114,108);
/*!40000 ALTER TABLE `practical_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_question`
--

DROP TABLE IF EXISTS `practical_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_question` (
  `translateable_ptr_id` int(11) NOT NULL,
  `answer_type_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_required` tinyint(1) NOT NULL,
  `votes` int(11) NOT NULL,
  `points_id` int(11) NOT NULL,
  `submitted_by_id` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `suggest_status` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `practical_question_6a0693de` (`answer_type_id`),
  KEY `practical_question_4b623205` (`points_id`),
  KEY `practical_question_3c12418b` (`submitted_by_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_question`
--

LOCK TABLES `practical_question` WRITE;
/*!40000 ALTER TABLE `practical_question` DISABLE KEYS */;
INSERT INTO `practical_question` VALUES (71,1,'2011-10-03 21:36:44',0,0,1,2,1,0),(98,2,'2011-10-04 01:18:23',0,0,1,3,1,0);
/*!40000 ALTER TABLE `practical_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_question_voted_accounts`
--

DROP TABLE IF EXISTS `practical_question_voted_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_question_voted_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_id` (`question_id`,`account_id`),
  KEY `practical_question_voted_accounts_1f92e550` (`question_id`),
  KEY `practical_question_voted_accounts_6f2fe10e` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_question_voted_accounts`
--

LOCK TABLES `practical_question_voted_accounts` WRITE;
/*!40000 ALTER TABLE `practical_question_voted_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_question_voted_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration_registrationprofile`
--

DROP TABLE IF EXISTS `registration_registrationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_registrationprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activation_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration_registrationprofile`
--

LOCK TABLES `registration_registrationprofile` WRITE;
/*!40000 ALTER TABLE `registration_registrationprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration_registrationprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_account_lesson`
--

DROP TABLE IF EXISTS `stats_account_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_account_lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `video` tinyint(1) NOT NULL,
  `textbook` tinyint(1) NOT NULL,
  `submitted_questions_num` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `finished_date` datetime DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_account_lesson_6f2fe10e` (`account_id`),
  KEY `stats_account_lesson_6d25d6e2` (`lesson_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_account_lesson`
--

LOCK TABLES `stats_account_lesson` WRITE;
/*!40000 ALTER TABLE `stats_account_lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_account_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_account_lesson_question`
--

DROP TABLE IF EXISTS `stats_account_lesson_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_account_lesson_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_lesson_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `is_answered` tinyint(1) NOT NULL,
  `start_time` datetime NOT NULL,
  `finish_time` datetime DEFAULT NULL,
  `attempts` int(11) NOT NULL,
  `hint_is_used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_account_lesson_question_516e674a` (`account_lesson_id`),
  KEY `stats_account_lesson_question_1f92e550` (`question_id`),
  KEY `stats_account_lesson_question_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_account_lesson_question`
--

LOCK TABLES `stats_account_lesson_question` WRITE;
/*!40000 ALTER TABLE `stats_account_lesson_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_account_lesson_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_completedexercise`
--

DROP TABLE IF EXISTS `stats_completedexercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_completedexercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_lesson_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `completed_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_completedexercise_516e674a` (`account_lesson_id`),
  KEY `stats_completedexercise_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_completedexercise`
--

LOCK TABLES `stats_completedexercise` WRITE;
/*!40000 ALTER TABLE `stats_completedexercise` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_completedexercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudent`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudent_161e15c3` (`teacher_id`),
  KEY `teacher_student_core_teacherstudent_42ff452e` (`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudent`
--

LOCK TABLES `teacher_student_core_teacherstudent` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudent` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudentcontent`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudentcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudentcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudentcontent_161e15c3` (`teacher_id`),
  KEY `teacher_student_core_teacherstudentcontent_42ff452e` (`student_id`),
  KEY `teacher_student_core_teacherstudentcontent_cc8ff3c` (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudentcontent`
--

LOCK TABLES `teacher_student_core_teacherstudentcontent` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudentcontent` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudentcontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudenthistory`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudenthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudenthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `finished_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudenthistory_42e828ff` (`row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudenthistory`
--

LOCK TABLES `teacher_student_core_teacherstudenthistory` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudenthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudenthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translateable`
--

DROP TABLE IF EXISTS `translation_translateable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translateable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translateable`
--

LOCK TABLES `translation_translateable` WRITE;
/*!40000 ALTER TABLE `translation_translateable` DISABLE KEYS */;
INSERT INTO `translation_translateable` VALUES (1),(2),(3),(4),(5),(6),(7),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(31),(32),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(71),(72),(75),(76),(77),(78),(79),(80),(81),(82),(83),(84),(85),(86),(87),(88),(89),(90),(91),(92),(93),(94),(95),(96),(97),(98),(99),(102),(103),(104),(105),(106),(107),(108),(109),(110),(111),(112),(113),(114),(115);
/*!40000 ALTER TABLE `translation_translateable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translation`
--

DROP TABLE IF EXISTS `translation_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `field_name` varchar(30) NOT NULL,
  `text` varchar(12500) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translation_translation_7ab48146` (`language_id`),
  KEY `translation_translation_7d61c803` (`object_id`)
) ENGINE=MyISAM AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translation`
--

LOCK TABLES `translation_translation` WRITE;
/*!40000 ALTER TABLE `translation_translation` DISABLE KEYS */;
INSERT INTO `translation_translation` VALUES (1,1,'name','text answer',1),(2,1,'description','text answer',1),(3,1,'name','single choice',2),(4,1,'description','single choice',2),(5,1,'name','multiple choice',3),(6,1,'description','multiple choice',3),(7,1,'difficulty_text','easy',4),(8,1,'difficulty_text','moderate',5),(9,1,'difficulty_text','hard',6),(10,1,'name','Science',7),(21,1,'description','<p><span>Adding 2-digit numbers</span></p>',13),(20,1,'name','Addition 2',13),(15,1,'name','Mathmatics',10),(16,1,'name','Arithmatic',11),(17,1,'description','<p>Topics related to Arithmatic</p>',11),(18,1,'name','Basic Addition',12),(19,1,'description','<p><span>Introduction to addition. Multiple visual ways to represent addition.</span></p>',12),(22,1,'name','Level 2 Addition',14),(23,1,'description','<p><span>Adding a 2 digit number to a 1 digit number. Introduction to carrying.</span></p>',14),(24,1,'name','Addition 3',15),(25,1,'description','<p><span>Practice carrying digits to add multiple digit numbers</span></p>',15),(26,1,'name','Addition 4',16),(27,1,'description','<p><span>More practice carrying with multiple digit numbers</span></p>',16),(28,1,'name','Basic Subtraction',17),(29,1,'description','<p><span>Introduction to subtraction</span></p>',17),(30,1,'name','Subtraction 2',18),(31,1,'description','<p><span>Different ways to view subtraction</span></p>',18),(32,1,'name','Subtraction 3:  Introduction to Borrowing or Rgrp',19),(33,1,'description','<p><span>Introduction to borrowing and regrouping</span></p>',19),(34,1,'name','Alternate mental subtraction method',20),(35,1,'description','<p><span>How I subtract in my head</span></p>',20),(36,1,'name','Why borrowing works',21),(37,1,'description','<p><span>An explanation of why (not how) borrowing/regrouping works when subtracting numbers</span></p>',21),(38,1,'name','Level 4 Subtraction',22),(39,1,'description','<p><span>Subtraction of multi-digit numbers involving borrowing</span></p>',22),(40,1,'name','Adding Decimals',23),(41,1,'description','<p><span>Adding decimals</span></p>',23),(42,1,'name','Subtracting decimals',24),(43,1,'description','<p><span>Subtracting Decimals</span></p>',24),(44,1,'name','Basic Multiplication',25),(45,1,'description','<p><span>Introduction to multiplication</span></p>',25),(46,1,'name','Multiplication 2: The Multiplication Tables',26),(47,1,'description','<p><span>Introduction to the multiplication tables from 2-9.</span></p>',26),(48,1,'name','Multiplication 3: 10,11,12 times tables',27),(49,1,'description','<p><span>Multiplication 3: Learning to multiply 10, 11, and 12.</span></p>',27),(50,1,'name','Multiplication 4: 2-digit times 1-digit number',28),(51,1,'description','<p><span>Multiplying a 2-digit times a 1-digit number</span></p>',28),(52,1,'name','Multiplication 5:  2-digit times a 2-digit number',29),(53,1,'description','<p><span>Multiplying a 2-digit number times a 2-digit number</span></p>',29),(54,1,'name','Multiplication 6: Multiple Digit Numbers',30),(55,1,'description','<p><span>Working our way up to multiplying multiple-digit numbers with each other</span></p>',30),(56,1,'name','Multiplication 7: Old video giving more examples',31),(57,1,'description','<p><span>2 examples of multiplying a 3 digit number times a 2 digit number. 1 example of multiplying a 3 digit number times a 3 digit number.</span></p>',31),(58,1,'name','Multiplication 8: Multiplying decimals (Old video)',32),(59,1,'description','<p><span>Multiplying decimals</span></p>',32),(63,1,'description','<p><span>Introduction to lattice multiplication</span></p>',34),(62,1,'name','Lattice Multiplication',34),(64,1,'name','Why Lattice Multiplication Works',35),(65,1,'description','<p><span>Understanding why lattice multiplication works</span></p>',35),(66,1,'name','Division 1',36),(67,1,'description','<p><span>Introduction to division</span></p>',36),(68,1,'name','Division 2',37),(69,1,'description','<p><span>Dividing into larger numbers. Introduction to long division and remainders.</span></p>',37),(70,1,'name','Division 3: More long division and remainder exmpl',38),(71,1,'description','<div id=\"watch-description-clip\">\r\n<div id=\"watch-description-text\">\r\n<p id=\"eow-description\">More long division and remainder examples</p>\r\n</div>\r\n</div>',38),(72,1,'name','Level 4 division',39),(73,1,'description','<p><span>Dividing a two digit number into a larger number.</span></p>',39),(74,1,'name','Converting fractions to decimals',40),(75,1,'description','<p><span>How to express a fraction as a decimal</span></p>',40),(76,1,'name','Percent and decimals',41),(77,1,'description','<p><span>Expressing percentages as decimals. Expressing decimals as percentages.</span></p>',41),(78,1,'name','Dividing decimal',42),(79,1,'description','<p><span>Dividing decimal numbers</span></p>',42),(80,1,'name','Ordering numeric expressions',43),(81,1,'description','<p><span>Ordering numbers expressed as decimals, fractions, and percentages</span></p>',43),(82,1,'name','Greatest Common Divisor',44),(83,1,'description','<p><span>4 example problems of determining the greatest common factor of two numbers by factoring the 2 numbers first</span></p>',44),(84,1,'name','Least Common Multiple',45),(85,1,'description','<p><span>Example of figuring out the least common multiple of two nunmbers</span></p>',45),(86,1,'name','Equivalent fractions',46),(87,1,'description','<p><span>Introduces the concept of equivalent fractions</span></p>',46),(88,1,'name','Mixed numbers and improper fractions',47),(89,1,'description','<p><span>Converting mixed numbers to improper fractions and improper fractions to mixed numbers.</span></p>',47),(90,1,'name','Algebra',48),(91,1,'description','<p>Lessons on the topic of algebra.</p>',48),(92,1,'name','Simple Equations',49),(93,1,'description','<p><span>Introduction to basic algebraic equations of the form Ax=B.</span></p>',49),(94,1,'name','Equations 2',50),(95,1,'description','<p><span>Slightly more complicated equations.</span></p>',50),(96,1,'name','Equations 3',51),(97,1,'description','<p><span>Equations with the variable on both sides.</span></p>',51),(98,1,'name','Developmental Math',52),(99,1,'description','<p>Lessons on the topic of developmental math.</p>',52),(100,1,'name','Adding Decimals',53),(101,1,'description','<p><span>Adding Decimals</span></p>',53),(102,1,'name','Adding Fractions with Like Denominators',54),(103,1,'description','<p><span>Adding Fractions with Like Denominators.</span></p>',54),(104,1,'name','Adding Fractions with Unlike Denominators',55),(105,1,'description','<p><span>Adding Fractions with Unlike Denominators.</span></p>',55),(106,1,'name','Adding Whole Numbers and Applications 1',56),(107,1,'description','<p><span>Adding Whole Numbers and Applications 1.</span></p>',56),(140,1,'name','Algebraic Long Division',75),(146,1,'name','Place Values 2',78),(163,1,'description','<p>Decimal place value 2</p>',86),(162,1,'name','Decimal Place Value 2',86),(147,1,'description','<p>Place Values 2</p>',78),(144,1,'name','Convex Lenses',77),(141,1,'description','<p>Dividing one polynomial into another.</p>',75),(142,1,'name','Physics',76),(143,1,'description','<p>Topics on physics</p>',76),(148,1,'name','Place Values',79),(145,1,'description','<p>Convex Lenses</p>',77),(189,2,'description','<p>testttt</p>',11),(188,2,'name','testttt',11),(133,1,'question','323',71),(134,1,'hint','',71),(135,1,'name','Computers',72),(149,1,'description','<p>Place values</p>',79),(150,1,'name','Place Values 3',80),(151,1,'description','<p>Place Values 3</p>',80),(152,1,'name','Multiplying Whole Number and Application 5',81),(153,1,'description','<p>Multiplying whole number and application 5</p>',81),(154,1,'name','Multiplying Whole Number and Application 4',82),(155,1,'description','<p>multiplying whole number and application 4</p>',82),(156,1,'name','Multiplying Whole Number and Application 2',83),(157,1,'description','<p>multiplying whole number and application 2</p>',83),(158,1,'name','Multiplying Whole Number and Application 2',84),(159,1,'description','<p>Multiplying whole number and application 1</p>',84),(160,1,'name','Decimal Place Value',85),(161,1,'description','<p>Decimal place value</p>',85),(164,1,'name','Multiplying Decimals 3',87),(165,1,'description','<p>Multiplying decimals 3</p>',87),(166,1,'name','Rounding Whole Numbers',88),(167,1,'description','<p>Rounding whole numbers</p>',88),(168,1,'name','Rounding Whole Numbers 2',89),(169,1,'description','<p>Rounding whole numbers 2</p>',89),(170,1,'name','Dividing Mixed Numbers and Fractions',90),(171,1,'description','<p>Dividing mixed numbers and fractions.</p>',90),(172,1,'name','Rounding to Estimate Differences',91),(173,1,'description','<p>Rounding to estimate differences.</p>',91),(174,1,'name','Adding Whole Numbers and Applications 2',92),(175,1,'description','<p>Adding whole numbers and applications 2.</p>',92),(176,1,'name','Biology',93),(177,1,'description','<p>Topics on Biology</p>',93),(178,1,'name','Photosynthesis',94),(179,1,'description','<p>Overview of photosythesis.</p>',94),(180,1,'name','Osteology',95),(181,1,'description','<p>Topics on&nbsp;Osteology</p>',95),(182,1,'name','Bones of the Human Body',96),(183,1,'description','<p>List of the bones of the human body.</p>',96),(184,1,'name','Name the Bones',97),(185,1,'description','<p>See if you can identify the bones</p>',97),(186,1,'question','Choose the femur',98),(187,1,'hint','',98),(190,1,'name','Languages',99),(197,1,'name','English',103),(198,1,'description','<p>English videos</p>',103),(199,1,'name','Talking about being sick - health vocabulary',104),(200,1,'description','<p><br class=\"Apple-interchange-newline\" />When you\'re sick, it\'s important to know how to describe how you feel. This vocabulary lesson will help you to talk about sickness and bodily pain!</p>',104),(201,1,'name','English with Cooking: Making Blueberry Pancake',105),(202,1,'description','<p>Words and expressions about cooking can open up a world before you. Learning about the measurements, tools, and methods of cooking will expand your English vocabulary. And you&rsquo;ll learn how to make yummy blueberry pancakes, too! I explain the following words in this class:&nbsp;<code>flour</code>,&nbsp;<code>salt</code>,&nbsp;<code>milk</code>,&nbsp;<code>eggs</code>,&nbsp;<code>oil</code>,&nbsp;<code>sugar</code>,&nbsp;<code>blueberries</code>,&nbsp;<code>maple syrup</code>,&nbsp;<code>grams</code>,<code>cups</code>,&nbsp;<code>millilitre</code>,&nbsp;<code>tablespoon</code>,&nbsp;<code>teaspoon</code>,&nbsp;<code>whisk</code>,&nbsp;<code>blender</code>,&nbsp;<code>mix</code>,&nbsp;<code>pinch</code>,&nbsp;<code>flip</code>,&nbsp;<code>pour</code>,&nbsp;<code>batter</code>.</p>',105),(195,1,'name','Talking about being sick - health vocabulary',102),(196,1,'description','<p>When you\'re sick, it\'s important to know how to describe how you feel. This vocabulary lesson will help you to talk about sickness and bodily pain!</p>',102),(203,1,'name','Learn Pronunciation with Poetry',106),(204,1,'description','<p>English pronunciation can be tough, but immersing yourself in language and having fun with it can make a big difference. By learning to read poetry with tone and rhythm, you can make your spoken English come alive!&nbsp;</p>',106),(205,1,'name','Health',107),(206,1,'name','Dental Health',108),(207,1,'description','<p>This section talks about Dental health conditions and cures</p>',108),(208,1,'name','Vocabulary - Talking about Shopping',109),(209,1,'description','<p>You need to know some simple vocabulary and phrases in order to shop in an English-speaking country. In this lesson, I teach you some of this important vocabulary.</p>',109),(210,1,'name','English Grammar - Past Tense Questions',110),(211,1,'description','<p>Learn the one word many English students forget and which you must use to ask questions correctly in the Simple Past Tense.&nbsp;</p>',110),(212,1,'name','English Grammar - Present Simple & Present Progres',111),(213,1,'description','<p>Learn about the difference between the Present Simple and the Present Progressive in English. Go to engVid to take the free quiz on this lesson.</p>',111),(214,1,'name','Urinology',112),(215,1,'description','<p>Learn about urinary system</p>',112),(216,1,'name','Kidney Stones',113),(217,1,'description','<p>Kidney stones are stone-like lumps that can develop in one or both of the kidneys and may cause severe pain. This animation explains how kidneys function and how to prevent kidney stones. It also describes the procedures used to remove or break down larger kidney stones.</p>',113),(218,1,'name','Dental Abscess',114),(219,1,'description','<p>This animation explains in detail what a dental abscess is, why it occurs and how it can be treated.</p>',114),(220,1,'name','Liver and kidneys',115),(221,1,'description','<p>Learn more about&nbsp;Liver and kidneys</p>',115);
/*!40000 ALTER TABLE `translation_translation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-04 17:41:37
