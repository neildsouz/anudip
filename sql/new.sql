-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: openuniversity
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_403f60f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_message`
--

LOCK TABLES `auth_message` WRITE;
/*!40000 ALTER TABLE `auth_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add message',4,'add_message'),(11,'Can change message',4,'change_message'),(12,'Can delete message',4,'delete_message'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add account',8,'add_account'),(23,'Can change account',8,'change_account'),(24,'Can delete account',8,'delete_account'),(25,'Can add language',9,'add_language'),(26,'Can change language',9,'change_language'),(27,'Can delete language',9,'delete_language'),(28,'Can add age group',10,'add_agegroup'),(29,'Can change age group',10,'change_agegroup'),(30,'Can delete age group',10,'delete_agegroup'),(31,'Can add points',11,'add_points'),(32,'Can change points',11,'change_points'),(33,'Can delete points',11,'delete_points'),(34,'Can add content on off marker',12,'add_contentonoffmarker'),(35,'Can change content on off marker',12,'change_contentonoffmarker'),(36,'Can delete content on off marker',12,'delete_contentonoffmarker'),(37,'Can add translate able',13,'add_translateable'),(38,'Can change translate able',13,'change_translateable'),(39,'Can delete translate able',13,'delete_translateable'),(40,'Can add translation',14,'add_translation'),(41,'Can change translation',14,'change_translation'),(42,'Can delete translation',14,'delete_translation'),(43,'Can add badge',15,'add_badge'),(44,'Can change badge',15,'change_badge'),(45,'Can delete badge',15,'delete_badge'),(46,'Can add sub type',16,'add_subtype'),(47,'Can change sub type',16,'change_subtype'),(48,'Can delete sub type',16,'delete_subtype'),(49,'Can add difficulty',17,'add_difficulty'),(50,'Can change difficulty',17,'change_difficulty'),(51,'Can delete difficulty',17,'delete_difficulty'),(52,'Can add subject',18,'add_subject'),(53,'Can change subject',18,'change_subject'),(54,'Can delete subject',18,'delete_subject'),(55,'Can add topic',19,'add_topic'),(56,'Can change topic',19,'change_topic'),(57,'Can delete topic',19,'delete_topic'),(58,'Can add content alias',20,'add_contentalias'),(59,'Can change content alias',20,'change_contentalias'),(60,'Can delete content alias',20,'delete_contentalias'),(61,'Can add video',21,'add_video'),(62,'Can change video',21,'change_video'),(63,'Can delete video',21,'delete_video'),(64,'Can add pdf file',22,'add_pdffile'),(65,'Can change pdf file',22,'change_pdffile'),(66,'Can delete pdf file',22,'delete_pdffile'),(67,'Can add image',23,'add_image'),(68,'Can change image',23,'change_image'),(69,'Can delete image',23,'delete_image'),(70,'Can add lesson',24,'add_lesson'),(71,'Can change lesson',24,'change_lesson'),(72,'Can delete lesson',24,'delete_lesson'),(73,'Can add question',25,'add_question'),(74,'Can change question',25,'change_question'),(75,'Can delete question',25,'delete_question'),(76,'Can add exercise',26,'add_exercise'),(77,'Can change exercise',26,'change_exercise'),(78,'Can delete exercise',26,'delete_exercise'),(79,'Can add exercise_ question',27,'add_exercise_question'),(80,'Can change exercise_ question',27,'change_exercise_question'),(81,'Can delete exercise_ question',27,'delete_exercise_question'),(82,'Can add answer',28,'add_answer'),(83,'Can change answer',28,'change_answer'),(84,'Can delete answer',28,'delete_answer'),(85,'Can add course',29,'add_course'),(86,'Can change course',29,'change_course'),(87,'Can delete course',29,'delete_course'),(88,'Can add course contents',30,'add_coursecontents'),(89,'Can change course contents',30,'change_coursecontents'),(90,'Can delete course contents',30,'delete_coursecontents'),(91,'Can add account_ lesson',31,'add_account_lesson'),(92,'Can change account_ lesson',31,'change_account_lesson'),(93,'Can delete account_ lesson',31,'delete_account_lesson'),(94,'Can add account_ lesson_ question',32,'add_account_lesson_question'),(95,'Can change account_ lesson_ question',32,'change_account_lesson_question'),(96,'Can delete account_ lesson_ question',32,'delete_account_lesson_question'),(97,'Can add completed exercise',33,'add_completedexercise'),(98,'Can change completed exercise',33,'change_completedexercise'),(99,'Can delete completed exercise',33,'delete_completedexercise'),(100,'Can add registration profile',34,'add_registrationprofile'),(101,'Can change registration profile',34,'change_registrationprofile'),(102,'Can delete registration profile',34,'delete_registrationprofile'),(103,'Can add teacher student content',35,'add_teacherstudentcontent'),(104,'Can change teacher student content',35,'change_teacherstudentcontent'),(105,'Can delete teacher student content',35,'delete_teacherstudentcontent'),(106,'Can add teacher student',36,'add_teacherstudent'),(107,'Can change teacher student',36,'change_teacherstudent'),(108,'Can delete teacher student',36,'delete_teacherstudent'),(109,'Can add teacher student history',37,'add_teacherstudenthistory'),(110,'Can change teacher student history',37,'change_teacherstudenthistory'),(111,'Can delete teacher student history',37,'delete_teacherstudenthistory'),(112,'Can add log entry',38,'add_logentry'),(113,'Can change log entry',38,'change_logentry'),(114,'Can delete log entry',38,'delete_logentry');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_badge`
--

DROP TABLE IF EXISTS `badge_badge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_badge` (
  `translateable_ptr_id` int(11) NOT NULL,
  `points_num` int(11) NOT NULL,
  `exercise_id` int(11) DEFAULT NULL,
  `is_special` tinyint(1) NOT NULL,
  `file` varchar(300) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `badge_badge_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_badge`
--

LOCK TABLES `badge_badge` WRITE;
/*!40000 ALTER TABLE `badge_badge` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_badge_accounts`
--

DROP TABLE IF EXISTS `badge_badge_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_badge_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `badge_id` (`badge_id`,`account_id`),
  KEY `badge_badge_accounts_7f24a4dc` (`badge_id`),
  KEY `badge_badge_accounts_6f2fe10e` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_badge_accounts`
--

LOCK TABLES `badge_badge_accounts` WRITE;
/*!40000 ALTER TABLE `badge_badge_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_badge_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_difficulty`
--

DROP TABLE IF EXISTS `common_difficulty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_difficulty` (
  `translateable_ptr_id` int(11) NOT NULL,
  `difficulty_int` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_difficulty`
--

LOCK TABLES `common_difficulty` WRITE;
/*!40000 ALTER TABLE `common_difficulty` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_difficulty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_subtype`
--

DROP TABLE IF EXISTS `common_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_subtype` (
  `translateable_ptr_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `type_int` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `type_int` (`type_int`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_subtype`
--

LOCK TABLES `common_subtype` WRITE;
/*!40000 ALTER TABLE `common_subtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_contentalias`
--

DROP TABLE IF EXISTS `contents_core_contentalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_contentalias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `contents_core_contentalias_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_contentalias`
--

LOCK TABLES `contents_core_contentalias` WRITE;
/*!40000 ALTER TABLE `contents_core_contentalias` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_core_contentalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_subject`
--

DROP TABLE IF EXISTS `contents_core_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_subject` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_subject`
--

LOCK TABLES `contents_core_subject` WRITE;
/*!40000 ALTER TABLE `contents_core_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_core_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_core_topic`
--

DROP TABLE IF EXISTS `contents_core_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_core_topic` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `age_group_id` int(11) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `contents_core_topic_386a7d9c` (`age_group_id`),
  KEY `contents_core_topic_638462f1` (`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_core_topic`
--

LOCK TABLES `contents_core_topic` WRITE;
/*!40000 ALTER TABLE `contents_core_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_core_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_account`
--

DROP TABLE IF EXISTS `core_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_account`
--

LOCK TABLES `core_account` WRITE;
/*!40000 ALTER TABLE `core_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_agegroup`
--

DROP TABLE IF EXISTS `core_agegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_agegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_agegroup`
--

LOCK TABLES `core_agegroup` WRITE;
/*!40000 ALTER TABLE `core_agegroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_agegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_contentonoffmarker`
--

DROP TABLE IF EXISTS `core_contentonoffmarker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_contentonoffmarker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offline` tinyint(1) NOT NULL,
  `unique` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_contentonoffmarker`
--

LOCK TABLES `core_contentonoffmarker` WRITE;
/*!40000 ALTER TABLE `core_contentonoffmarker` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_contentonoffmarker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_language`
--

DROP TABLE IF EXISTS `core_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(50) NOT NULL,
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_language`
--

LOCK TABLES `core_language` WRITE;
/*!40000 ALTER TABLE `core_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_points`
--

DROP TABLE IF EXISTS `core_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_points`
--

LOCK TABLES `core_points` WRITE;
/*!40000 ALTER TABLE `core_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_course`
--

DROP TABLE IF EXISTS `courses_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_course` (
  `translateable_ptr_id` int(11) NOT NULL,
  `created_account_id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `edited_date` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `courses_course_418bb3` (`created_account_id`),
  KEY `courses_course_279e212f` (`course_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_course`
--

LOCK TABLES `courses_course` WRITE;
/*!40000 ALTER TABLE `courses_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_coursecontents`
--

DROP TABLE IF EXISTS `courses_coursecontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_coursecontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `added_account_id` int(11) DEFAULT NULL,
  `content_level_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_coursecontents_b7271b` (`course_id`),
  KEY `courses_coursecontents_cc8ff3c` (`content_id`),
  KEY `courses_coursecontents_370986a3` (`added_account_id`),
  KEY `courses_coursecontents_7aaf98d1` (`content_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_coursecontents`
--

LOCK TABLES `courses_coursecontents` WRITE;
/*!40000 ALTER TABLE `courses_coursecontents` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_coursecontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'message','auth','message'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'account','core','account'),(9,'language','core','language'),(10,'age group','core','agegroup'),(11,'points','core','points'),(12,'content on off marker','core','contentonoffmarker'),(13,'translate able','translation','translateable'),(14,'translation','translation','translation'),(15,'badge','badge','badge'),(16,'sub type','common','subtype'),(17,'difficulty','common','difficulty'),(18,'subject','contents_core','subject'),(19,'topic','contents_core','topic'),(20,'content alias','contents_core','contentalias'),(21,'video','files','video'),(22,'pdf file','files','pdffile'),(23,'image','files','image'),(24,'lesson','practical','lesson'),(25,'question','practical','question'),(26,'exercise','practical','exercise'),(27,'exercise_ question','practical','exercise_question'),(28,'answer','practical','answer'),(29,'course','courses','course'),(30,'course contents','courses','coursecontents'),(31,'account_ lesson','stats','account_lesson'),(32,'account_ lesson_ question','stats','account_lesson_question'),(33,'completed exercise','stats','completedexercise'),(34,'registration profile','registration','registrationprofile'),(35,'teacher student content','teacher_student_core','teacherstudentcontent'),(36,'teacher student','teacher_student_core','teacherstudent'),(37,'teacher student history','teacher_student_core','teacherstudenthistory'),(38,'log entry','admin','logentry');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_image`
--

DROP TABLE IF EXISTS `files_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file` varchar(100) DEFAULT NULL,
  `content_id` int(11) NOT NULL,
  `is_localized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `files_image_cc8ff3c` (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_image`
--

LOCK TABLES `files_image` WRITE;
/*!40000 ALTER TABLE `files_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_pdffile`
--

DROP TABLE IF EXISTS `files_pdffile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_pdffile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) DEFAULT NULL,
  `is_localized` tinyint(1) NOT NULL,
  `url_path` varchar(200) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_pdffile_cc8ff3c` (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_pdffile`
--

LOCK TABLES `files_pdffile` WRITE;
/*!40000 ALTER TABLE `files_pdffile` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_pdffile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_video`
--

DROP TABLE IF EXISTS `files_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_path` varchar(200) DEFAULT NULL,
  `url_path_embed` varchar(200) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `is_localized` tinyint(1) NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `points_id` int(11) DEFAULT NULL,
  `has_thumbnails` tinyint(1) NOT NULL,
  `thumbnail_path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_video_7ab48146` (`language_id`),
  KEY `files_video_cc8ff3c` (`content_id`),
  KEY `files_video_4b623205` (`points_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_video`
--

LOCK TABLES `files_video` WRITE;
/*!40000 ALTER TABLE `files_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_answer`
--

DROP TABLE IF EXISTS `practical_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(200) DEFAULT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `question_id` int(11) NOT NULL,
  `image_file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `practical_answer_1f92e550` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_answer`
--

LOCK TABLES `practical_answer` WRITE;
/*!40000 ALTER TABLE `practical_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_exercise`
--

DROP TABLE IF EXISTS `practical_exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_exercise` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `bonus_points_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `practical_exercise_6d25d6e2` (`lesson_id`),
  KEY `practical_exercise_1f37ef1b` (`bonus_points_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_exercise`
--

LOCK TABLES `practical_exercise` WRITE;
/*!40000 ALTER TABLE `practical_exercise` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_exercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_exercise_question`
--

DROP TABLE IF EXISTS `practical_exercise_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_exercise_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m2m_question_id` int(11) NOT NULL,
  `m2m_exercise_id` int(11) NOT NULL,
  `difficulty_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practical_exercise_question_97556c3` (`m2m_question_id`),
  KEY `practical_exercise_question_4c24863b` (`m2m_exercise_id`),
  KEY `practical_exercise_question_269a6dbd` (`difficulty_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_exercise_question`
--

LOCK TABLES `practical_exercise_question` WRITE;
/*!40000 ALTER TABLE `practical_exercise_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_exercise_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_lesson`
--

DROP TABLE IF EXISTS `practical_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_lesson` (
  `contentonoffmarker_ptr_id` int(11) NOT NULL,
  `translateable_ptr_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  UNIQUE KEY `contentonoffmarker_ptr_id` (`contentonoffmarker_ptr_id`),
  KEY `practical_lesson_57732028` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_lesson`
--

LOCK TABLES `practical_lesson` WRITE;
/*!40000 ALTER TABLE `practical_lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_question`
--

DROP TABLE IF EXISTS `practical_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_question` (
  `translateable_ptr_id` int(11) NOT NULL,
  `answer_type_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_required` tinyint(1) NOT NULL,
  `votes` int(11) NOT NULL,
  `points_id` int(11) NOT NULL,
  `submitted_by_id` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `suggest_status` int(11) NOT NULL,
  PRIMARY KEY (`translateable_ptr_id`),
  KEY `practical_question_6a0693de` (`answer_type_id`),
  KEY `practical_question_4b623205` (`points_id`),
  KEY `practical_question_3c12418b` (`submitted_by_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_question`
--

LOCK TABLES `practical_question` WRITE;
/*!40000 ALTER TABLE `practical_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical_question_voted_accounts`
--

DROP TABLE IF EXISTS `practical_question_voted_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practical_question_voted_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_id` (`question_id`,`account_id`),
  KEY `practical_question_voted_accounts_1f92e550` (`question_id`),
  KEY `practical_question_voted_accounts_6f2fe10e` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical_question_voted_accounts`
--

LOCK TABLES `practical_question_voted_accounts` WRITE;
/*!40000 ALTER TABLE `practical_question_voted_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `practical_question_voted_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration_registrationprofile`
--

DROP TABLE IF EXISTS `registration_registrationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_registrationprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activation_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration_registrationprofile`
--

LOCK TABLES `registration_registrationprofile` WRITE;
/*!40000 ALTER TABLE `registration_registrationprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration_registrationprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_account_lesson`
--

DROP TABLE IF EXISTS `stats_account_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_account_lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `video` tinyint(1) NOT NULL,
  `textbook` tinyint(1) NOT NULL,
  `submitted_questions_num` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `finished_date` datetime DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_account_lesson_6f2fe10e` (`account_id`),
  KEY `stats_account_lesson_6d25d6e2` (`lesson_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_account_lesson`
--

LOCK TABLES `stats_account_lesson` WRITE;
/*!40000 ALTER TABLE `stats_account_lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_account_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_account_lesson_question`
--

DROP TABLE IF EXISTS `stats_account_lesson_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_account_lesson_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_lesson_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `is_answered` tinyint(1) NOT NULL,
  `start_time` datetime NOT NULL,
  `finish_time` datetime DEFAULT NULL,
  `attempts` int(11) NOT NULL,
  `hint_is_used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_account_lesson_question_516e674a` (`account_lesson_id`),
  KEY `stats_account_lesson_question_1f92e550` (`question_id`),
  KEY `stats_account_lesson_question_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_account_lesson_question`
--

LOCK TABLES `stats_account_lesson_question` WRITE;
/*!40000 ALTER TABLE `stats_account_lesson_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_account_lesson_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_completedexercise`
--

DROP TABLE IF EXISTS `stats_completedexercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_completedexercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_lesson_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `completed_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stats_completedexercise_516e674a` (`account_lesson_id`),
  KEY `stats_completedexercise_2799bae2` (`exercise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_completedexercise`
--

LOCK TABLES `stats_completedexercise` WRITE;
/*!40000 ALTER TABLE `stats_completedexercise` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_completedexercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudent`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudent_161e15c3` (`teacher_id`),
  KEY `teacher_student_core_teacherstudent_42ff452e` (`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudent`
--

LOCK TABLES `teacher_student_core_teacherstudent` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudent` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudentcontent`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudentcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudentcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudentcontent_161e15c3` (`teacher_id`),
  KEY `teacher_student_core_teacherstudentcontent_42ff452e` (`student_id`),
  KEY `teacher_student_core_teacherstudentcontent_cc8ff3c` (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudentcontent`
--

LOCK TABLES `teacher_student_core_teacherstudentcontent` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudentcontent` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudentcontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_student_core_teacherstudenthistory`
--

DROP TABLE IF EXISTS `teacher_student_core_teacherstudenthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_student_core_teacherstudenthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `started_date` datetime NOT NULL,
  `finished_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_student_core_teacherstudenthistory_42e828ff` (`row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_student_core_teacherstudenthistory`
--

LOCK TABLES `teacher_student_core_teacherstudenthistory` WRITE;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudenthistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_student_core_teacherstudenthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translateable`
--

DROP TABLE IF EXISTS `translation_translateable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translateable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translateable`
--

LOCK TABLES `translation_translateable` WRITE;
/*!40000 ALTER TABLE `translation_translateable` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation_translateable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translation`
--

DROP TABLE IF EXISTS `translation_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `field_name` varchar(30) NOT NULL,
  `text` varchar(12500) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translation_translation_7ab48146` (`language_id`),
  KEY `translation_translation_7d61c803` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translation`
--

LOCK TABLES `translation_translation` WRITE;
/*!40000 ALTER TABLE `translation_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation_translation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-12 17:19:59
