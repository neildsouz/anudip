from django.conf.urls.defaults import patterns, include, url
from openuniversity import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic.simple import direct_to_template
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^nomadadmin/', include(admin.site.urls)),
    url(r'^', include('openuniversity.apps.home.urls')),
    url(r'^moderator/', include('openuniversity.apps.moderator.urls'), name="moderator"),
    url(r'^profiles/', include('openuniversity.apps.profiles.urls')),
    url(r'^accounts/', include('openuniversity.apps.registration.backends.default.urls')),
    url(r'^topics/practice/', include('openuniversity.apps.contents.practical.topic_practice_urls')),
    url(r'^topics/', include('openuniversity.apps.contents.topics.urls')),
    #url(r'^courses/practice/', include('openuniversity.apps.contents.practical.courses_practice_urls')),
    url(r'^courses/', include('openuniversity.apps.contents.courses.urls')),
    url(r'^common/', include('openuniversity.apps.common.urls')),
    url(r'^get-content/', include('openuniversity.apps.content_installer.server.urls')),
    url(r'^feedback/', include('openuniversity.apps.feedback.urls')),
    url(r'^evaluation/', include('openuniversity.apps.evaluation.urls')),
    url(r'^virtualclassroom/', include('openuniversity.apps.tokbox.urls')),
    url(r'^teacher/', include('openuniversity.apps.teacher_student.teacher.urls')),
    url(r'^report/', include('openuniversity.apps.contents.stats.urls')),
    url(r'^poll/', include('openuniversity.apps.poll.urls')),
#    url(r'^practice/', include('openuniversity.apps.practice.urls')),
#    url(r'^teachers/', include('openuniversity.apps.teachers.urls')),
#    url(r'^challenge/', include('openuniversity.apps.challenge.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^$', direct_to_template,
     #           { 'template': 'base.html' }, name='index'),
)

if settings.DEBUG:
    urlpatterns+= patterns('',
         (r'media/(?P<path>.*)$','django.views.static.serve',{"document_root":settings.MEDIA_ROOT})               
    )

urlpatterns += staticfiles_urlpatterns()