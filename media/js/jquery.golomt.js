﻿/******************

Author Sansarbayar .S

******************/

String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };
String.prototype.startsWith = function(prefix) { return this.indexOf(prefix) === 0; };
String.prototype.endsWith = function(suffix) { return this.match(suffix+"$") == suffix; };

var js =
{
    fixKeyCode: function (event) {
        if (!event.which && ((event.charCode || event.charCode === 0) ? event.charCode : event.keyCode)) {
            event.which = event.charCode || event.keyCode;
        }
    },

    tabCycle: function (lastTarget, firstTarget) {

        $(firstTarget).keydown(function (event) {
            js.fixKeyCode(event);

            if (event.which == 9 && event.shiftKey) {
                $(this).blur();
                $(lastTarget).focus();

                return false;
            }
        });

        $(lastTarget).keydown(function (event) {
            js.fixKeyCode(event);

            if (event.which == 9 && !event.shiftKey) {
                $(firstTarget).focus();

                return false;
            }
        });
    },

    mask: function (target, btn, chars, flag) {
        $(target).keypress(function (event) {
            js.fixKeyCode(event);

            switch (event.which) {
                case 13:
                    {
                        if (event.target.tagName.toLowerCase() == "textarea") {
                            return true;
                        }
                        else {
                            $(btn).click();
                            return false;
                        }
                    }
            }

            if (!flag && chars) {
                var ch = String.fromCharCode(event.which);

                if (chars.indexOf(ch) != -1) {
                    return false;
                }
            }
            else if (chars) {
                var ch = String.fromCharCode(event.which);

                if (chars.indexOf(ch) == -1) {
                    return false;
                }
            }
        });
    },
	
    enterAction: function (target, action) {
        $(target).keypress(function (event) {
            js.fixKeyCode(event);

            switch (event.which) {
                case 13:
                    {
                        action();                            						
						return false;
                    }
            }
        });
    },	

    growl: function (msg, type) {
        if (!type) {
            type = "success";
        }
        $.growlUI("", "<div class='growl " + type + "'>" + msg + "</div>", 3000);

        $("div.growl").click(function () {
            $.unblockUI({ fadeOut: 1000 });
        });
    },

    growlSuccess: function (msg) {
        js.growl(msg, "success");
    },

    growlError: function (msg) {
        js.growl(msg, "error");
    },

    showPopup: function (el, opacity) {
        $('#modalmask').css({ 'width': '100%', 'height': $(document).height(), 'opacity': opacity ? opacity : 0.2 }).show();

        var jqEl = $(el);
        
		jqEl.css({ 'left': $(document).width() / 2 - jqEl.width() / 2, 'top': $(window).height() / 2 - jqEl.height() / 2 }).show();

    	jqEl.draggable({ handle: el + " .panel_head", cancel: el + " .panel_content", start: function() {
			//jqEl.validationEngine("hide");
		} });
		
		jqEl.find('.close').click(function(){
	        js.hidePopup(el);
			
			if($(this).hasClass("clear")) {
				jqEl.html("");
			}
		});

        jqEl.keypress(function (event) {
            js.fixKeyCode(event);

            switch (event.which) {
                case 13:
                {
                    if (event.target.tagName.toLowerCase() == "textarea" || event.target.tagName.toLowerCase() == "button") {
                        return true;
                    }
                    else {
                    	event.stopImmediatePropagation();
                    	jqEl.submit();
                        return false;
                    }
                } break;
            }
        }).keydown(function (event) {
            js.fixKeyCode(event);

            if (event.which == 27) {
		        js.hidePopup(el);
				
				if(jqEl.find(".close").hasClass("clear")) {
					jqEl.html("");
				}
            	event.stopImmediatePropagation();
            	
               	return false;
            }
        });
        
		
		jqEl.find('.focus').focus();		
		
		$("#ajaxloader").hide();
		
		$.unblockUI({ fadeOut: 300 });
    },

    hidePopup: function (el) {
        var jqEl = $(el);
		
		//jqEl.validationEngine("hide");		
		
        jqEl.hide();		
        $('#modalmask').hide();		
    },

    hideAll: function () {
        $('.popup').hide();
        $('#modalmask').hide();
        $.unblockUI();
    },
	
	change_menubar: function(tab_id) {
		var tab = $(tab_id);
		
        if (tab && !tab.hasClass("active")) {
            $("#header a.main").removeClass("active");
            tab.addClass("active");

            $(".submenu").hide();

            var rel_submenu = tab.attr("rel");

            if (rel_submenu) {
                $("#" + rel_submenu).fadeIn("fast");
            }
        }								
	},
	
	getUrlVars:	function() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    
    getCookie: function(name, defaultValue) {
    	var value = $.cookie(name);
    	
        if (value == null) {
        	if (defaultValue) {
        		value = defaultValue;
        		$.cookie(name, value, {path: '/admin', expires: 1});        		        	
        	}
        	else {
        		value = "";
        	}        	        	
    	}
        
        return value;
    },
    
    setCookie: function(name, value) {
    	$.cookie(name, value, {path: '/admin', expires: 1});
    },    

    setLandingPage: function (formId, lpId) {
        if (formId && lpId) {
            $('#form_landing_page_id_'+formId).val(lpId);
        }
    }
};
