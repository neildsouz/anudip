function openDialog(divId){	
   $("#" + divId).dialog("open");
   $("#" + divId).dialog().show();
   return false;
}

function initDialog(divId,path){
	$("#" + divId).dialog({
		           autoOpen: false,
		           modal: false,
		           height: 300,
		           width: 500,
				   //hide: 'drop' ,
				   //show: 'drop',
				   position: ['center','center'],
				    close: function(event, ui) {
							//toggle()
					}
		       });
		   $("#" + divId).html('<iframe id="IframeId" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto" />');
		   $("#IframeId").attr("src",path);
		   return false;
}
