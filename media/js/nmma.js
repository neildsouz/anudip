// ajaxForm - g uurchilsun nmmaAjaxForm plugin
(function( $ ){
    $.fn.nmmaAjaxForm = function(options) {
        if (!options) optoins = {};
        options = $.extend({}, nmma.formSettings, options);
        if ($(this).attr('action')=="") {
            $(this).attr('action',location.pathname + location.hash.substring(1));
            act = $(this).attr('action');
            if (act.charAt(act.length - 1) != '/') $(this).attr('action', act + '/');
        }
        $(this).ajaxForm(options);
        //$(this).validationEngine();
		return $(this);
    };
})( jQuery );

var nmma =
{
    ajax_count: 0,
    queue: [],
    ajax_start: function() {
        nmma.ajax_count++;
        $('#ajax_loader').show();
    },

    ajax_finish: function () {
        nmma.ajax_count--;
        if (nmma.ajax_count<=0) $('#ajax_loader').hide();
    },

    unload: function() {},
    
    abort: function () {
        for(var i=0;i<nmma.queue.length;i++) {
        	if(nmma.queue[i].readyState != 4) {
        		nmma.queue[i].abort();
        	}
        }    	
    },

    init: function() {
        options = $.extend({}, nmma.formSettings, {});
        $('form:not([class~="noajax"])').nmmaAjaxForm(options);
    },

    doAjax: function (options) {
        nmma.ajax_start();
        options.onAjax();
        var xhr = $.ajax({
            url : options.path + options.url,
            dataType : options.dataType,
            cache: options.cache,
            type: options.type,
            data: options.data,
            error: function (x,e,t) {
                options.onFinish(e);
                nmma.ajax_finish();
                if (x.status != 0) {				 
					js.growlError(x.status + " " + e);
				}
            },
            success: function (data) {
                options.success(data);
                options.onFinish();
                nmma.ajax_finish();
            }
        });
        
        nmma.queue.push(xhr);
    },
    
    formSettings:{
        dataType:'json',
        type:'POST',
		beforeSubmit:function (formData, jqForm, options){
            //alert(jqForm.validationEngine('validate'));
            //return jqForm.validationEngine('validate');
            nmma.ajax_start();			
		},
        success:function (data, statusText, xhr, form) {
            if (data.msg) {
                form.html(data.msg);
                nmma.ajax_finish();
                return;
            }
            nmma.settings.success(data);
            nmma.ajax_finish();
        },
        error: function (x,e,t) {
            nmma.ajax_finish();
            js.growlError(x.status + " " + e);
        },
    },

    settings:{
        cache: false,
        data: '',
        path: '',
        url: '/',
        type: 'GET',
        dataType: 'json',
        onAjax: function () {
        },
        success: function (data) {
            if (data.notajax_redirect) {
                location = data.notajax_redirect;
                location.reload();
            }
            if (data.redirect) {
                nmma.ajax_finish();
                location.hash = data.redirect;
                if (!data.reload) return;
            }
            if (data.reload) {
                location.reload();
            }

            if (data.title) document.title = data.title;

            $.each(data.meta, function() {
                elm = $('meta[name="'+this.name+'"]');

                if (elm.length) {
                    elm.attr('content',this.content);
                } else {
                    $('head').append('<meta name="'+this.name+'" content="'+this.content+'"/>');
                }
            });

            $.each(data.res, function() {
                //$('body').html(nmma.body.html());
                elm = $(this.tag+'['+this.attr+'="'+this.attrValue+'"]');

                if (elm.length) {
                    pre_elm = nmma.body.find(this.tag+'['+this.attr+'="'+this.attrValue+'"]');
                    if (pre_elm.length) {
                        elm.html(pre_elm.html());
                    }
                    
                    /*
                    //attr-уудын хадгалж байна. replace хийсний дараа буцааж онооно.
                    var attr_list = []
                    $.each(elm.listAttributes(), function(i, attrib){
                        attr_list[attrib] = elm.attr(attrib);
                    });*/

                    if (this.type == 'replace') {
                        elm.replaceWith(this.res);
                    } else if (this.type == 'appbegin') {
                        elm.replaceWith($(this.res).append(elm.html()));
                    } else if (this.type == 'append') {
                        elm.append($(this.res).html());
                    } else if (this.type == 'remove') {
                        elm.hide();
                    }
                    elm = $(this.tag+'['+this.attr+'="'+this.attrValue+'"]');                  
                    elm.find('a').each(function(){
                        $(this).attr('href',decodeURI($(this).attr('href')));
                    });                    

/*
                    // дээр хадгалсан attr-уудыг элементэд буцааж оноож байна.
                    for (var key in attr_list) {
                        if (key != this.attr)
                            elm.attr(key, attr_list[key]);
                    }*/

                    elm.find('a').each(function(){
                        $(this).attr('href',decodeURI($(this).attr('href')));
                    });
                    
                    if (this.tab) js.change_menubar('#tab_'+this.tab);			
                }
            });
        },
        onFinish: function () {
        },
    },

    ajax: function (options) {
        if (!options) optoins = {};
        options = $.extend({}, nmma.settings, options);
        nmma.doAjax(options);
	},

    history: function (options) {
        if (!options) optoins = {};
        options = $.extend({}, nmma.settings, options);
        defUrl = options.url;

        load = function (url) {
            options.url = url;
            nmma.doAjax(options);
        }

        $.history.init(function (url)
        {
            nmma.unload(); // ajax huudas unload hiigdeh ved nmma.unload() eventiig /function/ - g duudna.
            if (url != "" && url != "/" && url != "./") {
                location.hash = decodeURI(url);
                load(encodeURI(url));
                if (typeof(_gaq) != "undefined") {
                    _gaq.push(['_trackPageview', "/#"+url]);
                }
            } else {
                load(defUrl);
            }
            nmma.unload=function() {}
        },
        { unescape: true}); 
        $('a').live('click', function (e) {
            $(this).blur();
        });
    },

    hide: function (elm) {
        nmma.body = elm.clone();
    },
};
