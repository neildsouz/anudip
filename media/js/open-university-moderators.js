function addFormFormSet(cnt_id, total_forms_name)
{
	var forms_total = parseInt($("#id_"+total_forms_name+"-TOTAL_FORMS").attr("value"));
	
	var new_ffs = $("#"+cnt_id+" > div:last").clone();
	
	$(new_ffs).find("#id_"+total_forms_name+"-0-id").removeAttr("value");
	
	var str = $(new_ffs).html();
	
	var patt=new RegExp(total_forms_name +"-\\d+-","gi");
	str2 = str.replace(patt, total_forms_name+"-"+forms_total+"-");
	$("#id_"+total_forms_name+"-TOTAL_FORMS").attr("value", forms_total+1);
	
	$("#"+cnt_id).append("<div class='form_form_set'>" + str2 + "</div>").end();
}

function confirm_delete(path, msg)
{
	if( confirm(msg) )
	{
        window.location = path;		
	}
	return false;
}