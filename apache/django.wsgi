import os
import sys

sys.path.append('/var/www/html')

os.environ['DJANGO_SETTINGS_MODULE'] = 'openuniversity.settings'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
