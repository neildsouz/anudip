# -*- coding: utf-8 -*-

# Django settings for openuniversity project.

import os
def rel(zam):
    """
    Төслийн одоо ажиллаж байгаа замыг буцаана
    """
    return os.path.join(os.path.abspath(os.path.dirname(__file__)),zam)

# False if the nomad edu is online
# True if the nomad edu is offline
OFFLINE_VERSION = False

LESSON_PATH = rel("media/lessons")

PATH = rel("")

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
     ('Brian Bernstein', 'brian@teachaclas.org'),
)

MANAGERS = ADMINS
AUTH_PROFILE_MODULE = "courses.Account"

DATABASES = {
    'default': {
        'ENGINE': 'mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '<Add database name>',                      # Or path to database file if using sqlite3.
        'USER': '<add user>',                      # Not used with sqlite3.
        'PASSWORD': '<add password>',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Ulaanbaatar'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html

LANGUAGE_CODE = 'en-us'

EMAIL_HOST = 'localhost'

DEFAULT_FROM_EMAIL = 'brian@teachaclass.org'

LOGIN_URL = "/accounts/login/"

LOGIN_REDIRECT_URL = "/topics"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

LOCALE_PATHS = (
    'locale'
)

gettext = lambda s: s
LANGUAGES = (
    ('mn', gettext('Mongolia')),
    ('en-us', gettext('English')),
    ('ru', gettext('Russia')),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = rel('media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/media/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '&$w3i#$=3kbz&%pk0#1u@ym6e(c33v9x!w#_o-2$d4g8c)9f26'

# List of callables that know how to import templates from various sources.

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

ROOT_URLCONF = 'openuniversity.urls'

TEMPLATE_DIRS = (
    rel("templates"),
)

AUTH_PROFILE_MODULE = 'core.Account'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'openuniversity.apps.core',
    'openuniversity.apps.translation',
    'openuniversity.apps.badge',
    'openuniversity.apps.common',
    'openuniversity.apps.contents.contents_core',
    'openuniversity.apps.contents.files',
    'openuniversity.apps.contents.practical',
    'openuniversity.apps.contents.courses',
    'openuniversity.apps.contents.stats',
    'openuniversity.apps.moderator',
    'openuniversity.apps.content_installer.core',
    'openuniversity.apps.feedback',
    'openuniversity.apps.content_installer.client',
    'openuniversity.apps.content_installer.server',
    'openuniversity.apps.home',
    'openuniversity.apps.registration',
    'openuniversity.apps.profiles',
    'openuniversity.apps.teacher_student.teacher_student_core',
    'openuniversity.apps.teacher_student.student',
    'openuniversity.apps.teacher_student.teacher',
    'pagination',
    # 'openuniversity.apps.challenge', # todo: to be added
    
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable the admin:
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

ACCOUNT_ACTIVATION_DAYS = 3

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

try:
    from settings_dev import *
except ImportError, exp:
    pass