#!/bin/bash

rm -rf initial_data/*
mkdir initial_data/auth
mkdir initial_data/translation
mkdir initial_data/badge
mkdir initial_data/core
mkdir initial_data/common
mkdir initial_data/contents
mkdir initial_data/contents/contents_core
mkdir initial_data/contents/practical
mkdir initial_data/contents/courses
mkdir initial_data/contents/stats
mkdir initial_data/contents/files

python manage.py dumpdata auth --format=json --indent=2 > initial_data/auth/initial_data.json
python manage.py dumpdata translation --format=json --indent=2 > initial_data/translation/initial_data.json
python manage.py dumpdata badge --format=json --indent=2 > initial_data/badge/initial_data.json
python manage.py dumpdata core --format=json --indent=2 > initial_data/core/initial_data.json
python manage.py dumpdata common --format=json --indent=2 > initial_data/common/initial_data.json
python manage.py dumpdata contents_core --format=json --indent=2 > initial_data/contents/contents_core/initial_data.json
python manage.py dumpdata practical --format=json --indent=2 > initial_data/contents/practical/initial_data.json
python manage.py dumpdata courses --format=json --indent=2 > initial_data/contents/courses/initial_data.json
python manage.py dumpdata stats --format=json --indent=2 > initial_data/contents/stats/initial_data.json
python manage.py dumpdata files --format=json --indent=2 > initial_data/contents/files/initial_data.json
